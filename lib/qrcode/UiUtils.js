function rpx2px(rpx) {
    var res = wx.getSystemInfoSync();
    return res.windowWidth / 750  * rpx;
}

function px2rpx(px) {
  var res = wx.getSystemInfoSync();
  return 750 * px / res.windowWidth;
}

module.exports = {
    rpx2px: rpx2px,
    px2rpx: px2rpx
}

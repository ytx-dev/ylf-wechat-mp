// app.js
const util = require('./utils/util.js');
const promisify = require('./utils/promisify.js');
const cart = require('./utils/cart.js');
const invite = require('./utils/invite.js');
const isProd = true;
const updateManager = wx.getUpdateManager();

App({
  /**
   * 微信登录获取用户信息
   */
  login: function (fn) {
    this.globalData.backFn = fn ? fn : null;
    //判断用户登录是否过期
    if (!this.globalData.rememberMe) {
      this.globalData.reset = 'authSetting';
      //未授权去获取授权
      util.navigation_to('/pages/authorize/authorize', 1);
    } else if (fn) {
      fn();
    }
  },
  /**
   * ytx 登录获取 ytx 信息
   */
  ytxLogin: function (callback) {
    var that = this;
    //判断用户登录是否过期
    if (!that.globalData.rememberMe) {
      wx.login({
        success: res => {
          // 发送 res.code 到后台换取 openId, sessionKey, unionId
          if (res.code) {
            util.$http({
              url: that.globalData.apiDomain + 'ylf/user/wxapp/login',
              type: 'post',
              header: {
                'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
              },
              dataType: 'json',
              data: {
                code: res.code,
                encryptedData: that.globalData.encryptedData,
                iv: that.globalData.iv
              },
              callback: function (obj) {
                if (obj.data.success == true) {
                  //判断用户需要注册么 true 为需要注册，false 为不需要
                  if (obj.data.unionId) {
                    that.globalData.unionId = obj.data.unionId;
                  }
                  if (obj.data['remember-me']) {
                    that.globalData.rememberMe = obj.data['remember-me'];
                    wx.setStorage({
                      key: 'rememberMe',
                      data: that.globalData.rememberMe
                    });
                    wx.setStorage({
                      key: 'lastLogin',
                      data: new Date().getTime()
                    });
                  }
                  if (obj.data.openId) {
                    that.globalData.openId = obj.data.openId;
                    wx.setStorage({
                      key: 'openId',
                      data: that.globalData.openId
                    });
                  }
                  if (obj.data.account) {
                    that.globalData.account = obj.data.account;
                    wx.setStorage({
                      key: 'account',
                      data: JSON.stringify(that.globalData.account)
                    });
                  }
                  if (obj.data.needRegister) {
                    obj.data.needRegister = obj.data.needRegister == true ? 1 : 0;
                  }
                  if (!that.globalData.rememberMe) {
                    //未能检测登录后执行方法的话，存储方法，在 onShow 的时候检测调用 backFn 方法，执行完后请清空
                    wx.redirectTo({
                      url: '/pages/login/login?needRegister=' + obj.data.needRegister,
                      success: function () {
                        that.globalData.reset = 'login';
                      }
                    });
                  } else {
                    that.globalData.reset = '';
                    if (that.globalData.backFn) {
                      that.globalData.backFn();
                      that.globalData.backFn = null;
                    }
                    if (callback) {
                      callback();
                    }
                  }
                }
              },
              fail() {
                wx.showToast({
                  title: '登陆失败，请退出重试',
                });
                if (callback) {
                  callback();
                }
              }
            })
          } else {
            if (callback) {
              callback();
            }
          }
        },
        fail: err => {
          if (callback) {
            callback();
          }
        }
      })
    } else {
      if (that.globalData.backFn) {
        that.globalData.backFn();
        that.globalData.backFn = null;
      }
      if (callback) {
        callback();
      }
    }
  },
  getCartNumber: function () {
    let count = cart.getCartSize();
    if (count > 0) {
      wx.setTabBarBadge({
        index: 2,
        text: count + ''
      })
    } else {
      wx.removeTabBarBadge({
        index: 2
      })
    }
  },
  checkUpdate() {
    const _this = this;
    updateManager.onCheckForUpdate(function (res) {
      if (res.hasUpdate) {
        updateManager.onUpdateReady(function () {
          wx.showModal({
            title: '更新提示',
            content: '新版本已经准备好，是否重启应用？',
            success(res) {
              if (res.confirm) {
                updateManager.applyUpdate()
              }
            }
          })
        })
      } else {
        setTimeout(_this.checkUpdate, 30000);
      }
    })
  },
  bezier: function (pots, amount) {
    var pot;
    var lines;
    var ret = [];
    var points;
    for (var i = 0; i <= amount; i++) {
      points = pots.slice(0);
      lines = [];
      while (pot = points.shift()) {
        if (points.length) {
          lines.push(pointLine([pot, points[0]], i / amount));
        } else if (lines.length > 1) {
          points = lines;
          lines = [];
        } else {
          break;
        }
      }
      ret.push(lines[0]);
    }

    function pointLine(points, rate) {
      var pointA, pointB, pointDistance, xDistance, yDistance, tan, radian, tmpPointDistance;
      pointA = points[0]; //点击
      pointB = points[1]; //中间
      xDistance = pointB.x - pointA.x;
      yDistance = pointB.y - pointA.y;
      pointDistance = Math.pow(Math.pow(xDistance, 2) + Math.pow(yDistance, 2), 1 / 2);
      tan = yDistance / xDistance;
      radian = Math.atan(tan);
      tmpPointDistance = pointDistance * rate;
      let ret = {
        x: pointA.x + tmpPointDistance * Math.cos(radian),
        y: pointA.y + tmpPointDistance * Math.sin(radian)
      };
      return ret;
    }
    return {
      'bezier_points': ret
    };
  },
  getShareCommonData() {
    let path = '/pages/index/index/index';
    if (this.globalData.spreadNumber) {
      path += '?spreadNumber=' + this.globalData.spreadNumber;
    }
    return {
      title: '迎米 囤货首选',
      path: path,
      imageUrl: 'https://g3.ytx.com/ylf_mp_2/bg-share.jpg'
    };
  },
  onLaunch() {
    this.checkUpdate();
    wx.getSystemInfo({
      success: e => {
        this.globalData.StatusBar = e.statusBarHeight;
        let capsule = wx.getMenuButtonBoundingClientRect();
        this.globalData.Custom = capsule;
        if (capsule) {
          this.globalData.CustomBar = capsule.bottom + capsule.top - e.statusBarHeight;
        } else {
          this.globalData.CustomBar = e.statusBarHeight + 50;
        }
      }
    });
    try {
      wx.async = promisify.toAsync(["getLocation", "request"]);
      let lastLogin = wx.getStorageSync('lastLogin');
      if (lastLogin) {
        lastLogin = Number(lastLogin) + 1000 * 60 * 60 * 24 * 15;
        let currTime = new Date().getTime();
        if (currTime < lastLogin) {
          let rememberMe = wx.getStorageSync('rememberMe')
          if (rememberMe) {
            this.globalData.rememberMe = rememberMe;
          }
        }
      }
      let openId = wx.getStorageSync('openId');
      if (openId) {
        this.globalData.openId = openId;
      }
      let account = wx.getStorageSync('account');
      if (account) {
        this.globalData.account = JSON.parse(account);
      }
      let userInfo = wx.getStorageSync('userInfo');
      if (userInfo) {
        this.globalData.userInfo = JSON.parse(userInfo);
      }

      // wx.setTabBarItem({
      //   index: 0,
      //   text: 'text',
      //   iconPath: 'https://ytx-g3.oss-cn-shanghai.aliyuncs.com/ylf_mp_2/icon-time.png',
      //   selectedIconPath: 'https://ytx-g3.oss-cn-shanghai.aliyuncs.com/ylf_mp_2/icon-shop.png'
      // })
    } catch (e) {
      console.error(e);
    }
    setTimeout(invite.getSpreadNumber, 1500);
  },
  globalData: {
    userInfo: null,
    /**
     * 迎天下账户信息
     */
    unionId: null,
    openId: null,
    rememberMe: null,
    backFn: null,
    account: null,
    iv: null,
    encryptedData: null,
    reset: '', //如果是进入授权页 'authSetting' ,如果是去注册页  'login'
    shareToken: '',
    isProd: isProd,
    imageDomain: isProd ? 'https://g2.ytx.com/' : 'http://g2.ytx5.com/',
    apiDomain: isProd ? 'https://api.ytx.com/' : 'http://api.ytx5.com/',
    liveApiDomain: isProd ? 'http://43.138.86.129' : 'http://api.ytx5.com/ylf/liveroom',
    sellerShop: {},
    sellerAccountId: 0,
    isInRange: false,
    spreadNumber: null,
    theme: '2',
    cartData: {},
    version: '2.9.1'
  }
})
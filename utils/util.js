const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('-')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatTimemdhms = date => {
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[month, day].map(formatNumber).join('-')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatTime2 = (date, tag) => {
  if(!tag) {
    tag = '-';
  }
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  return [year, month, day].map(formatNumber).join(tag)
}

Date.prototype.Format = function (fmt) {
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小时
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  }
  for (var k in o) {
    if (new RegExp("(" + k + ")").test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    }
  }
  return fmt;
};

const convertTime = (time) => {
  const day = time / (24 * 60 * 60 * 1000);
  let plus = time % (24 * 60 * 60 * 1000);
  const hour = plus / (60 * 60 * 1000);
  plus = plus % (60 * 60 * 1000);
  const min = plus / (60 * 1000);
  plus = plus % (60 * 1000);
  const sec = plus / (1000);
  let url = "";
  if (day > 1) {
    url += parseInt(day) + "天";
  }
  if (hour > 1) {
    url += parseInt(hour) + "小时";
  }
  url += parseInt(min) + "分" + parseInt(sec) + "秒";
  return url;
}

const convertTimeDian = (time) => {
  const day = time / (24 * 60 * 60 * 1000);
  let plus = time % (24 * 60 * 60 * 1000);
  const hour = plus / (60 * 60 * 1000);
  plus = plus % (60 * 60 * 1000);
  const min = plus / (60 * 1000);
  plus = plus % (60 * 1000);
  const sec = plus / (1000);
  let url = "";
  if (day > 1) {
    url += parseInt(day) + ":";
  }
  if (hour > 1) {
    let v = parseInt(hour) + '';
    if (v.length < 2) {
      v = '0' + v;
    }
    url += v + ":";
  }
  if (min > -1) {
    let v = parseInt(min) + '';
    if (v.length < 2) {
      v = '0' + v;
    }
    url += v + ":";
  }
  if (sec > -1) {
    let v = parseInt(sec) + '';
    if (v.length < 2) {
      v = '0' + v;
    }
    url += v;
  }
  return url;
}

const convertTimeDian3 = (time) => {
  const day = time / (24 * 60 * 60 * 1000);
  let plus = time % (24 * 60 * 60 * 1000);
  const hour = time / (60 * 60 * 1000);
  plus = plus % (60 * 60 * 1000);
  const min = plus / (60 * 1000);
  plus = plus % (60 * 1000);
  const sec = plus / (1000);
  let url = "";
  if (hour > 1) {
    let v = parseInt(hour) + '';
    if (v.length < 2) {
      v = '0' + v;
    }
    url += v + ":";
  } else {
    url += "00:"
  }
  if (min > -1) {
    let v = parseInt(min) + '';
    if (v.length < 2) {
      v = '0' + v;
    }
    url += v + ":";
  } else {
    url += "00:"
  }
  if (sec > -1) {
    let v = parseInt(sec) + '';
    if (v.length < 2) {
      v = '0' + v;
    }
    url += v;
  } else {
    url += "00"
  }
  return url;
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}

const $http = options => {
  wx.getNetworkType({
    success: function (res) {
      if (res.networkType === 'none' || res.networkType === 'unKnown') {
        wx.showToast({
          title: '网络异常',
          image: '/lib/images/exclamation.png'
        })
        return;
      }
      wx.request({
        url: options.url,
        method: options.type ? options.type.toUpperCase() : 'GET',
        data: options.data || {},
        header: options.header || {},
        success: data => {
          options.callback(data);
        },
        fail: data => {
          let currentPage = getCurrentPages();
        },
        complete: function (data) {
          options.complete && options.complete(data);
        }
      });
    },
  })

}

const slider_layer_show = (vm, fn) => {
  var animation = wx.createAnimation({
    duration: 400,
    timingFunction: "easeOut"
  })
  let animationShowHeight = 300;
  wx.getSystemInfo({
    success: function (res) {
      animationShowHeight = res.windowHeight;
    }
  })
  vm.animation = animation
  animation.translateY(animationShowHeight).step()
  vm.setData({
    animationData: animation.export(),
  })
  fn();
  setTimeout(function () {
    animation.translateY(0).step()
    vm.setData({
      animationData: animation.export()
    })
  }.bind(vm), 10)
}

const slider_layer_hide = (vm, fn) => {
  fn();
}

const show_toast_layer = (vm, fn) => {
  vm.setData({
    isShowToast: true
  });
  fn();
  setTimeout(function () {
    vm.setData({
      isShowToast: false,
      toastText: ''
    });
  }, 1000)
}

const navigation_to = (url, type) => {
  wx.getNetworkType({
    success: function (res) {
      if (res.networkType === 'none' || res.networkType === 'unKnown') {
        wx.navigateTo({
          url: '/pages/error/error',
        })
      } else {
        switch (type) {
          case 1:
            wx.navigateTo({
              url: url,
            });
            break;
          case 2:
            wx.redirectTo({
              url: url,
            });
            break;
        }
      }
    },
  });
}

module.exports = {
  formatTime,
  formatTimemdhms: formatTimemdhms,
  formatTime2: formatTime2,
  convertTime: convertTime,
  convertTimeDian: convertTimeDian,
  convertTimeDian3: convertTimeDian3,
  $http: $http,
  slider_layer_show: slider_layer_show,
  slider_layer_hide: slider_layer_hide,
  show_toast_layer: show_toast_layer,
  navigation_to: navigation_to
}
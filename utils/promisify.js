function promisify(fn) {
  // promisify() 返回的是一个函数，
  // 这个函数跟传入的 fn（即 wx.abcd） 签名相同（或兼容）
  return async function (args) {
    return new Promise((resolve, reject) => {
      fn({
        ...(args || {}),
        success: res => resolve(res),
        fail: err => reject(err)
      });
    });
  };
}

function toAsync(names) {
  return (names || [])
    .map(name => ({
      name,
      member: wx[name]
    }))
    .filter(t => typeof t.member === "function")
    .reduce((r, t) => {
      r[t.name] = promisify(wx[t.name]);
      return r;
    }, {});
}

module.exports = {
  toAsync
}

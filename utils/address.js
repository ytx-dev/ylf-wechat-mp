const amap = require('./amap.js');
const request = require('./request.js');

async function getCurrentLocation() {
  let result = {};
  const points = await getCoordinate();
  if (points.errMsg === 'getLocation:ok') {
    const location = points.longitude + ',' + points.latitude;
    const res = await getPoisByAmap(location);
    if (res && res.cityCode) {
      const checkres = await checkAddr(res.cityCode, points.latitude, points.longitude);
      result = {
        country: '中国',
        countryCode: '86',
        province: res ? res.regeocode.addressComponent.province : '',
        provinceCode: res ? res.regeocode.addressComponent.adcode : '',
        city: res ? res.regeocode.addressComponent.city : '',
        cityCode: res ? res.cityCode : '',
        area: res ? res.regeocode.addressComponent.district : '',
        areaCode: res ? res.regeocode.addressComponent.adcode : '',
        sellerAccountId: checkres ? checkres.sellerAccountId : 0,
        sellerName: checkres ? checkres.sellerName : '',
        isInRange: checkres ? checkres.isCreateAddress : false,
        latitude: points.latitude,
        longitude: points.longitude,
        address: res?.regeocode?.pois ?? [] ? res?.regeocode?.pois[0].address : '',
        poiName: res?.regeocode?.pois ?? [] ? res?.regeocode?.pois[0].name : ''
      };
    }
  }
  return result;
}

async function getCoordinate() {
  try {
    const res = await wx.async.getLocation({
      type: 'gcj02'
    });
    return res;
  } catch (e) {
    return {}
  }
}

async function getPoisByAmap(location) {
  try {
    const res = await amap.regeo(location);
    return res;
  } catch (err) {
    return {};
  }
}

async function checkAddr(cityCode, latitude, longitude) {
  const res = await request.getWithOutToken('/ylf/buyer/buyer/address/check', {
    // cityCode,
    latitude,
    longitude
  });
  return res.data;
}

module.exports = {
  getCurrentLocation,
  getCoordinate,
  getPoisByAmap,
  checkAddr
}
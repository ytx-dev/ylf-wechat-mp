async function regeo(location) {
  const url = 'https://restapi.amap.com/v3/geocode/regeo';
  return new Promise((resolve, reject) => {
    wx.request({
      url: url, //仅为示例，并非真实的接口地址
      data: {
        key: '63a1903e9be6485b8a4aeafa22a3be75',
        location,
        roadlevel: 0,
        extensions: 'all'
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success(res) {
        const result = {};
        const data = res.data;
        if (data && data.status == 1) {
          const cityCode = data.regeocode.addressComponent.citycode;
          result.cityCode = cityCode;
          result.pois = data.regeocode.pois;
          result.regeocode = data.regeocode;
          if (data.regeocode.aois && data.regeocode.aois.length > 0) {
            const aoi = data.regeocode.aois[0];
            result.location = aoi.location;
            result.name = aoi.name;
          }
        }
        resolve(result);
      },
      fail() {
        reject({
          msg: '请求失败',
          url: url,
          method: 'get'
        });
      }
    })
  });
}

async function inputtips(keywords, city) {
  const url = 'https://restapi.amap.com/v3/assistant/inputtips';
  return new Promise((resolve, reject) => {
    wx.request({
      url: url, //仅为示例，并非真实的接口地址
      data: {
        key: '63a1903e9be6485b8a4aeafa22a3be75',
        keywords,
        city,
        datatype: 'poi'
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success(res) {
        let result = [];
        const data = res.data;
        if (data && data.status == 1) {
          for (const i in data.tips) {
            let item = data.tips[i];
            if (item && item.location && item.location.length > 0) {
              result.push(item);
            }
          }
        }
        resolve(result);
      },
      fail() {
        reject({
          msg: '请求失败',
          url: url,
          method: 'get'
        });
      }
    })
  });
}

module.exports = {
  regeo,
  inputtips
}
async function initCartData(callback) {
  const app = getApp();
  if (app && app.globalData.sellerShop && app.globalData.sellerShop.accountId && app.globalData.rememberMe) {
    const request = require('./request.js');
    const res = await request.get('ylf/trade/cart', {
      shop_id: app.globalData.sellerShop.accountId
    });
    setCartData(res.data);
  }
  if (callback) {
    callback();
  }
}

function setCartData(data) {
  const app = getApp();
  if (app) {
    let cardData = {};
    if (data) {
      if (data.valid && data.valid.length > 0) {
        for (const i in data.valid) {
          let item = data.valid[i];
          cardData[item.itemId] = item.number;
        }
      }
      if (data.disabled && data.disabled.length > 0) {
        for (const i in data.disabled) {
          let item = data.disabled[i];
          cardData[item.itemId] = item.number;
        }
      }
    }
    app.globalData.cartData = cardData;
  }
}

function setCartDataItemAdd(itemId) {
  const app = getApp();
  if (app) {
    let cardData = app.globalData.cartData;
    let num = cardData[itemId];
    if (!num) {
      num = 0;
    }
    cardData[itemId] = (num + 1);
    app.globalData.cartData = cardData;
  }
}

function getCartSize() {
  let count = 0;
  const app = getApp();
  if (app && app.globalData.cartData) {
    for (const key in app.globalData.cartData) {
      let item = app.globalData.cartData[key];
      if (item) {
        count += Number(item);
      }
    }
  }
  return count;
}

module.exports = {
  initCartData,
  setCartData,
  setCartDataItemAdd,
  getCartSize
}
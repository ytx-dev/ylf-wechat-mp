const app = getApp();
const baseURL = app.globalData.liveApiDomain;

async function get(url, data) {
  return await request('GET', url, data);
}

async function post(url, data) {
  return await request('POST', url, data);
}

async function put(url, data) {
  return await request('PUT', url, data);
}

async function del(url, data) {
  return await request('DELETE', url, data);
}

async function request(method, url, data) {
  return new Promise((resolve, reject) => {
    wx.request({
      url: baseURL + url,
      header: {
        'content-type': 'application/json'
      },
      data,
      method,
      success(res, statusCode, header, cookies) {
        resolve(res.data);
      },
      fail() {
        reject({
          msg: '请求失败',
          url: baseURL + url,
          method,
          data
        });
      }
    });
  });
};

module.exports = {
  get,
  post,
  put,
  del
}
async function getSpreadNumber() {
  const app = getApp();
  if (app && app.globalData.sellerShop && app.globalData.sellerShop.accountId && app.globalData.rememberMe) {
    const request = require('./request.js');
    const res = await request.get('ylf/buyer/spread/getSpreadNumber');
    if (res && res.data && res.data.spreadNumber) {
      app.globalData.spreadNumber = res.data.spreadNumber;
    } else {
      app.globalData.spreadNumber = null;
    }
  }
}

async function getSpreadNumberFromUrl(url) {
  let value = null;
  if (url && url.indexOf('spreadNumber=') > 0) {
    let params = url.split('?')[1];
    params = params.split('&');
    for (const i in params) {
      //spreadNumber=123456
      let param = params[i];
      if (param.indexOf('spreadNumber=') > -1) {
        value = param.split('=')[1];
        break;
      }
    }
  }
  return value;
}

async function bindFormLocal() {
  let spreadNumber = wx.getStorageSync('spreadNumber');
  if (spreadNumber) {
    let res = await bind(spreadNumber);
    return res;
  } else {
    return false;
  }
}

async function bind(spreadNumber) {
  const app = getApp();
  if (app && app.globalData.rememberMe && spreadNumber) {
    //&& spreadNumber != app.globalData.spreadNumber
    const request = require('./request.js');
    const res = await request.post('ylf/buyer/spread/bind?spreadNumber=' + spreadNumber, {});
    wx.removeStorageSync('spreadNumber');
    return {
      succeed: res.succeed,
      message: res.message
    };
  }
}

module.exports = {
  getSpreadNumber,
  getSpreadNumberFromUrl,
  bindFormLocal,
  bind
}
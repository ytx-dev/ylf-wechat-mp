const app = getApp();
const request = require('./request.js');
const payType = 4;

async function payment(orderNo, paymentSn, callback) {
  if(!app.globalData.isProd) {
    check(paymentSn, callback);
  } else {
    paymentOrder(orderNo, paymentSn, callback)
  }
}

async function paymentOrder(orderNo, paymentSn, callback) {
  const res = await request.get('ylf/trade/pay', {
    orderNo,
    payType,
    openId: app.globalData.openId
  });
  if (res.succeed) {
    const data = res.data.unifiedOrderMap;
    if (data.result_code === 'FAIL') {
      wx.showToast({
        title: data.err_code_des,
        icon: 'none',
        duration: 2000,
        success: () => {
          check(paymentSn, callback);
          // setTimeout(callback, 2000, true);
        }
      });
    } else {
      wx.requestPayment({
        timeStamp: data.timeStamp,
        nonceStr: data.nonce_str,
        package: 'prepay_id=' + data.prepay_id,
        signType: 'MD5',
        paySign: data.paySign,
        success(res) {
          if (res.errMsg == "requestPayment:ok") {
            check(paymentSn, callback);
          } else {
            callback(false);
          }
        },
        fail(res) {
          callback(false);
        }
      });
    }
  }
  return res;
}

async function check(paymentSn, callback) {
  let params = {};
  if(!app.globalData.isProd) {
    params.pretend = true;
  }
  const res = await request.get('ylf/trade/pay/' + paymentSn + '/' + payType, params);
  callback(res.succeed);
}

async function checkPay(paymentSn, callback) {
  const res = await request.get('ylf/trade/pay/' + paymentSn + '/5');
  callback(res);
}

module.exports = {
  payment,
  checkPay
}
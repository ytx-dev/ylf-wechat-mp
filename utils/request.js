const app = getApp();
const baseURL = app.globalData.apiDomain;

async function get(url, data) {
  return await request('GET', url, data, true);
}

async function getWithOutToken(url, data) {
  return await request('GET', url, data, false);
}

async function post(url, data) {
  return await request('POST', url, data, true);
}

async function postWithOutToken(url, data) {
  return await request('POST', url, data, false);
}

async function put(url, data) {
  return await request('PUT', url, data, true);
}

async function del(url, data) {
  return await request('DELETE', url, data, true);
}

async function request(method, url, data, isToken) {
  return new Promise((resolve, reject) => {
    wx.getNetworkType({
      success: function (typeRes) {
        if (typeRes.networkType === 'none' || typeRes.networkType === 'unKnown') {
          wx.showToast({
            title: '网络异常',
            icon: 'error'
          })
          reject({
            message: '请求失败',
            url: baseURL + url,
            method,
            data
          });
        } else {
          wx.request({
            url: baseURL + url,
            header: {
              'content-type': 'application/json',
              'cookie': isToken ? 'remember-me=' + app.globalData.rememberMe : ''
            },
            data,
            method,
            success(res, statusCode, header, cookies) {
              if(statusCode == 401 || statusCode == 403 || res.statusCode == 401 || res.statusCode == 403) {
                app.globalData.rememberMe = null;
                wx.removeStorageSync('rememberMe');
                wx.removeStorageSync('lastLogin');
                wx.hideLoading();
                reject({
                  message: '请重试',
                  url: baseURL + url,
                  method,
                  data,
                  isReLogin: true
                });
              } else {
                resolve(res.data);
              }
            },
            fail(res, statusCode, header, cookies) {
              reject({
                message: '请求失败',
                url: baseURL + url,
                method,
                data
              });
            }
          });
        }
      }
    })
  });
};

module.exports = {
  get,
  getWithOutToken,
  post,
  postWithOutToken,
  put,
  del
}
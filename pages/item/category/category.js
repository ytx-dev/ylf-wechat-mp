// pages/item/category/category.js
var app = getApp();
var unload = true;
var util = require('../../../utils/util.js');
const request = require('../../../utils/request.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar < 21 ? 26 : app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    categoryList: [],
    selectCatgory: null,
    categoryError: true
  },

  gotoHome(){
    wx.switchTab({
      url: '/pages/index/index/index',
    })
  },

  onSearchTap(e) {
    wx.navigateTo({
      url: '/pages/item/search/search',
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (!app.globalData.sellerShop.accountId) {
      wx.showModal({
        title: '提示',
        content: '请选择地址',
        showCancel: false,
        success(res) {
          wx.switchTab({
            url: '/pages/index/index/index',
          });
        }
      });
    } else {
      unload = true;
      if (this.data.categoryList.length == 0) {
        this.sqlGetCategoryList();
      }
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  },
  /**
   * 获取分类
   */
  async sqlGetCategoryList() {
    const that = this;
    let url = 'ylf/item/classification/list?accountId=' + app.globalData.sellerShop.accountId + '&classificationId=0';
    if (!app.globalData.isInRange) {
      url += '&type=2';
    }
    const res = await request.get(url);
    if (res.data.list) {
      that.setData({
        categoryError: true
      })
      var categoryList = res.data.list;
      //压缩图片
      for (var i = 0, len = categoryList.length; i < len; i++) {
        if (categoryList[i].subList) {
          for (var j = 0, jLen = categoryList[i].subList.length; j < jLen; j++) {
            if (categoryList[i].subList[j].icon) {
              categoryList[i].subList[j].icon = app.globalData.imageDomain + categoryList[i].subList[j].icon + "?x-oss-process=image/resize,m_pad,h_122,w_122,color_FFFFFF";
            }
          }
        }
      }
      that.setData({
        categoryList: categoryList,
        selectCatgory: categoryList && categoryList.length ? categoryList[0] : {}
      });
    } else {
      that.setData({
        categoryError: false
      })
    }
  },
  /**
   * 页面改变类目
   */
  changeCategory: function (event) {
    this.setCategory(event.currentTarget.dataset.id);
  },
  /**
   * 选中类目
   */
  setCategory: function (id) {
    var categoryList = this.data.categoryList;
    let selectCatgory = {};
    for (var i = 0, len = categoryList.length; i < len; i++) {
      if (id === categoryList[i].id) {
        selectCatgory = categoryList[i];
        break;
      }
    }
    this.setData({
      selectCatgory: selectCatgory
    });
  },
  /**
   * 跳转列表页
   */
  gotoList: function (e) {
    let id = e.currentTarget.dataset.categoryId;
    let ids = e.currentTarget.dataset.categoryIds;
    let navigationBarTitleText = e.currentTarget.dataset.categoryName;
    if (unload) {
      unload = false;
      util.navigation_to('/pages/item/list/list2?categoryId=' + id + '&categoryIds=' + ids + '&navigationBarTitleText=' + navigationBarTitleText, 1);
    }
  }
})
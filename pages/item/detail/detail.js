// pages/goods/detail.js
const app = getApp();
const request = require('../../../utils/request.js');
const UiUtils = require("../../../lib/qrcode/UiUtils.js");
const util = require('../../../utils/util.js');
const cart = require('../../../utils/cart.js');

Page({
  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    cartNumData: app.globalData.cartData,
    isScroll: true,
    itemId: null,
    itemData: null,
    distributionTime: '',
    cartNum: 0,
    imgUrls: [],
    current: 0,
    indicatorDots: true,
    autoplay: false,
    interval: 5000,
    duration: 300,
    isCollectiveItem: false,
    animationData: {},
    status: 1,
    itemOff: false,
    stockEmpty: false,
    skuData: {},
    selectedSku: {},
    selectSkuIds: [],
    skuList: [],
    showSkuLayer: false,
    seckillData: {},
    seckillTime: 0,
    seckillTimer: null,
    isSharePintuan: false,
    type: undefined, // 1:直播
    activityId: undefined,

    animationX: null,
    animationY: null,
    ballX: 0,
    ballY: 0,
    showBall: false,
    isLoading: false,
    guessList: [],
    orderBy: '',
    pageNumber: 1,
    hasNextPage: true,
  },

  async getGuessList() {
    let params = {
      pageNum: this.data.pageNumber,
      sellerAccountId: app.globalData.sellerShop.accountId,
      itemId: this.data.itemId
    }
    if (!app.globalData.isInRange) {
      params.type = 2;
    }
    if (this.data.orderBy) {
      params.sort = this.data.orderBy;
    }
    let res = await request.get('ylf/search/guess', params);
    if (res && res.data) {
      let page = res.data.page;
      this.setData({
        guessList: this.data.guessList.concat(page.list),
        hasNextPage: page.pageNum < page.pages,
        orderBy: page.orderBy
      });
    }
  },

  initGuessList() {
    this.setData({
      guessList: [],
      pageNumber: 1,
      orderBy: '',
      hasNextPage: true
    }, () => {
      this.getGuessList();
    });
  },

  onScrolltolower() {
    if (this.data.hasNextPage) {
      this.setData({
        pageNumber: this.data.pageNumber + 1,
      }, () => {
        this.getGuessList();
      })
    }
  },

  setCartDataItemAdd(itemId) {
    cart.setCartDataItemAdd(itemId);
    this.setData({
      cartNumData: app.globalData.cartData
    })
  },

  async onItemAddCartTap(e) {
    if (this.data.isLoading) {
      return;
    }
    app.login(() => {
      let itemId = e.currentTarget.dataset['itemId'];
      this.setCartDataItemAdd(itemId);
      this.addCart(itemId, e);
    });
  },

  async addCart(itemId, e) {
    this.setData({
      isLoading: true
    });
    const res = await request.post('ylf/trade/cart/' + app.globalData.sellerShop.accountId + '/' + itemId);
    if (res.succeed) {
      let ballX = e.touches[0].clientX;
      let ballY = e.touches[0].clientY;
      this.createAnimation(ballX, ballY);
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      });
      this.setData({
        isLoading: false
      });
    }
  },

  setDelayTime(sec) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, sec)
    });
  },

  // 创建动画
  createAnimation(ballX, ballY) {
    var sysinfo = wx.getSystemInfoSync();
    let _this = this;
    let bottomX = UiUtils.rpx2px(40);
    let bottomY = sysinfo.windowHeight;
    let animationX = _this.flyX(bottomX, ballX); // 创建小球水平动画
    let animationY = _this.flyY(bottomY, ballY); // 创建小球垂直动画
    _this.setData({
      ballX: ballX,
      ballY: ballY,
      showBall: true
    });
    _this.setDelayTime(100).then(() => {
      // 100ms延时,  确保小球已经显示
      _this.setData({
        animationX: animationX.export(),
        animationY: animationY.export()
      })
      // 400ms延时, 即小球的抛物线时长
      return _this.setDelayTime(400);
    }).then(() => {
      _this.setData({
        animationX: _this.flyX(0, 0, 0).export(),
        animationY: _this.flyY(0, 0, 0).export(),
        showBall: false,
        isLoading: false
      });
      _this.get_cart_num();
    })
  },

  // 水平动画
  flyX(bottomX, ballX, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'linear',
    })
    animation.translateX(bottomX - ballX).step();
    return animation;
  },
  // 垂直动画
  flyY(bottomY, ballY, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'ease-in',
    })
    animation.translateY(bottomY - ballY).step();
    return animation;
  },

  buyFromLive() {
    let selfMention = this.data.itemData.itemOtherPropertyVo && this.data.itemData.itemOtherPropertyVo.selfMention == 1 ? 1 : 0;
    wx.navigateTo({
      url: '/pages/order/confirm/confirm?isLive=1&itemId=' + this.data.itemId + '&activityId=' + this.data.activityId + '&selfMention=' + selfMention,
    });
  },

  onBtnSharePintuan() {
    this.setData({
      isSharePintuan: true
    })
  },

  gotoCart() {
    wx.switchTab({
      url: '/pages/cart/cart',
    })
  },

  onBtnPintuanTap(e) {
    let item = e.currentTarget.dataset['data'];
    app.login(() => {
      setTimeout(this.gotoPintuan, 300, item);
    });
  },

  gotoPintuan(item) {
    let selfMention = this.data.itemData.itemOtherPropertyVo && this.data.itemData.itemOtherPropertyVo.selfMention == 1 ? 1 : 0;
    let itemId = this.data.itemId;
    wx.navigateTo({
      url: '/pages/order/confirm/confirm?isPintuan=1&itemId=' + itemId + '&combinationId=' + this.data.itemData.combinationVo.combinationId + '&groupId=' + item.groupId + '&selfMention=' + selfMention,
    });
  },

  onBtnKaituanTap() {
    app.login(() => {
      setTimeout(this.gotoKaituan, 300);
    });
  },

  async gotoKaituan() {
    let isCanPintuan = await this.checkIsCanPintuan();
    if (isCanPintuan) {
      let selfMention = this.data.itemData.itemOtherPropertyVo && this.data.itemData.itemOtherPropertyVo.selfMention == 1 ? 1 : 0;
      wx.navigateTo({
        url: '/pages/order/confirm/confirm?isPintuan=1&itemId=' + this.data.itemId + '&combinationId=' + this.data.itemData.combinationVo.combinationId + '&selfMention=' + selfMention,
      });
    }
  },

  onSelectSkuTap(e) {
    this.show_sku_layer();
  },

  onSkuBtnTap(e) {
    let index = e.currentTarget.dataset['skuIndex'];
    let skuId = e.currentTarget.dataset['skuId'];
    let ids = this.data.selectSkuIds;
    ids[index] = skuId;
    let mapKey = '';
    for (const i in ids) {
      if (mapKey) {
        mapKey += ';';
      }
      mapKey += ids[i];
    }
    const select = this.data.skuData[mapKey];
    this.setData({
      selectSkuIds: ids,
      selectedSku: select ? select : {
        stockNum: 0
      },
      stockNum: select ? select.stockNum : 0
    });
  },

  show_sku_layer: function () {
    let vm = this;
    util.slider_layer_show(this, function () {
      vm.setData({
        showSkuLayer: true,
        isScroll: false
      })
    });
  },

  hide_sku_layer: function () {
    let vm = this;
    util.slider_layer_hide(this, function () {
      vm.setData({
        showSkuLayer: false,
        isScroll: true
      })
    });
  },

  shou_sku_image() {
    wx.previewImage({
      urls: [this.data.imgUrls[0]]
    });
  },

  set_swiper_current: function (e) {
    this.setData({
      current: e.detail.current
    })
  },
  show_swiper_images: function () {
    let vm = this;
    wx.previewImage({
      urls: vm.data.imgUrls,
      current: vm.data.imgUrls[vm.data.current]
    })
  },

  add_cart_check: function () {
    if (this.data.skuList && this.data.skuList.length > 0) {
      this.show_sku_layer();
    } else {
      this.add_cart_by_sku();
    }
  },
  add_cart_by_sku() {
    let vm = this;
    if (vm.data.cartNum >= 99) {
      wx.showToast({
        title: '添加失败,购物车数量已达上限',
        icon: 'none',
        duration: 1500
      });
      return;
    }
    if (vm.data.selectedSku.stockNum < 1) {
      return;
    }
    vm.add_cart();
    vm.hide_sku_layer();
  },
  async add_cart_commit() {
    try {
      const data = await request.post('ylf/trade/cart?seller_item_sku_id=' + this.data.selectedSku.id + '&number=1');
      if (data.succeed) {
        wx.showToast({
          title: '商品已成功添加到购物车',
          icon: 'none',
          duration: 1500
        });
        this.get_cart_num();
      } else {
        wx.showToast({
          title: data.message,
          icon: 'none'
        });
      }
    } catch (err) {
      if (err.isReLogin) {
        app.login(() => {
          this.add_cart_commit();
        });
      }
    }
  },
  add_cart() {
    app.login(() => {
      this.add_cart_commit();
    });
  },

  get_cart_num() {
    cart.initCartData(() => {
      let count = cart.getCartSize();
      this.setData({
        cartData: app.globalData.cartData,
        cartNum: count
      })
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    if (options.spreadNumber) {
      wx.setStorageSync('spreadNumber', options.spreadNumber);
      const invite = require('../../../utils/invite.js');
      invite.bindFormLocal();
    }
    if (!options.itemId) {
      wx.navigateBack();
    } else {
      this.setData({
        type: options.type,
        itemId: options.itemId,
        activityId: options.activityId
      }, () => {
        this.checkShopData(options.shopId);
      });
    }
  },

  async checkShopData(shopId) {
    if (app.globalData.sellerShop && app.globalData.sellerShop.accountId) {
      this.initItemData();
    } else if (shopId) {
      const shopIndex = await this.getShopIndexPage(shopId);
      app.globalData.sellerShop = shopIndex.sellerShop;
      wx.setStorage({
        key: "sellerShop",
        data: JSON.stringify(shopIndex.sellerShop)
      });
      this.initItemData();
    } else {
      wx.showModal({
        title: '提示',
        content: '请选择地址',
        showCancel: false,
        success(res) {
          wx.switchTab({
            url: '/pages/index/index/index',
          });
        }
      });
    }
  },

  async getShopIndexPage(id) {
    const res = await request.get('ylf/store/index/' + id);
    return res.data;
  },

  async initItemData() {
    const data = await this.getItemData();
    const imageList = [];
    for (const i in data.itemDetail.itemImageList) {
      const item = data.itemDetail.itemImageList[i];
      imageList.push(app.globalData.imageDomain + item.imageKey);
    }
    if (data.itemDetail.skuPropertyVOList && data.itemDetail.skuPropertyVOList.length > 0) {
      const skuData = data.itemDetail.sellerItemSkuMap;
      let select = null;
      let selectSkuIds = null;
      for (const key in skuData) {
        let sku = skuData[key];
        if (!select) {
          select = sku;
          let ids = key.split(';');
          if (ids[0] === '') {
            ids = []
          }
          selectSkuIds = ids.map(Number);
        }
        const descs = sku.skuDesc.split(';');
        let desc = '';
        for (const i in descs) {
          const items = descs[i].split(':');
          if (desc) {
            desc += ', ';
          }
          desc += items[1];
        }
        sku.desc = desc;
      }
      this.setData({
        itemData: data,
        distributionTime: data.distributionTime,
        imgUrls: imageList,
        skuData: skuData,
        status: data.itemDetail.sellerItem.status,
        stockNum: select.stockNum,
        selectedSku: select,
        selectSkuIds: selectSkuIds,
        skuList: data.itemDetail.skuPropertyVOList
      });
    } else {
      const skuData = data.itemDetail.sellerItemSkuMap[this.data.itemId];
      this.setData({
        itemData: data,
        distributionTime: data.distributionTime,
        imgUrls: imageList,
        status: data.itemDetail.sellerItem.status,
        stockNum: skuData ? skuData.stockNum : 0,
        skuData: skuData,
        selectedSku: skuData
      });
    }
    if (data.itemDetail.itemOtherPropertyVo && data.itemDetail.itemOtherPropertyVo.itemType == 2) {
      if (this.data.seckillTimer) {
        clearTimeout(this.data.seckillTimer);
      }
      let endDate = new Date(data.itemDetail.sellerItem.saleEnd);
      if (!endDate || !endDate.getTime()) {
        let endStr = data.itemDetail.sellerItem.saleEnd;
        endStr = endStr.replace(/-/g, "/");
        endDate = new Date(endStr);
      }
      let seckillTime = endDate.getTime() - (data.itemDetail.itemOtherPropertyVo.currentTime * 1000);
      this.setData({
        seckillData: {
          hh: '00',
          mm: '00',
          ss: '00'
        },
        seckillTime: seckillTime,
        seckillTimer: null
      }, () => {
        this.countdownSeckill();
      });
    } else if (data.combinationVo && data.combinationVo.currentTime < data.combinationVo.stopTime) {
      if (this.data.seckillTimer) {
        clearTimeout(this.data.seckillTimer);
      }
      let seckillTime = data.combinationVo.stopTime - data.combinationVo.currentTime;
      this.setData({
        seckillData: {
          hh: '00',
          mm: '00',
          ss: '00'
        },
        seckillTime: seckillTime,
        seckillTimer: null
      }, () => {
        this.countdownSeckill();
      });
    }
    this.get_cart_num();
    this.initGuessList();
  },

  countdownSeckill() {
    let time = this.data.seckillTime - 1000;
    if (time > 0) {
      let timer = setTimeout(() => {
        this.countdownSeckill();
      }, 1000);
      let seckillData = this.data.seckillData;
      let timeStr = util.convertTimeDian3(time);
      timeStr = timeStr.split(':');
      seckillData.hh = timeStr[0];
      seckillData.mm = timeStr[1];
      seckillData.ss = timeStr[2];
      this.setData({
        seckillData: seckillData,
        seckillTime: time,
        seckillTimer: timer
      });
    } else {
      this.setData({
        seckillData: {
          hh: '00',
          mm: '00',
          ss: '00'
        },
        seckillTime: null,
        seckillTimer: null
      });
      // this.initItemData();
    }
  },

  async checkIsCanPintuan() {
    let data = this.data.itemData;
    if (data.combinationVo && data.combinationVo.combinationId) {
      //检查是否能拼团
      const result = await request.get('ylf/trade/toCombination', {
        combinationId: data.combinationVo.combinationId,
        itemId: data.combinationVo.itemId,
        sellerAccountId: app.globalData.sellerShop.accountId
      });
      if (result.data.flag == 1) {
        return true;
      } else {
        wx.showToast({
          title: result.message,
          icon: 'none'
        })
        return false;
      }
    }
    return true;
  },

  async getItemData() {
    let url = 'ylf/item/item/' + app.globalData.sellerShop.accountId + '/' + this.data.itemId;
    if (this.data.type) {
      url += '?type=' + this.data.type + '&activityId=' + this.data.activityId;
    }
    const res = await request.get(url);
    if (res && res.data) {
      const data = res.data;
      if (data.itemDetail && data.itemDetail.itemDesc && data.itemDetail.itemDesc.desc) {
        data.itemDetail.itemDesc.desc = JSON.parse(data.itemDetail.itemDesc.desc);
      }
      return data;
    }
    return null;
  },

  onSwiperItemTap(e) {
    const url = e.currentTarget.dataset['url'];
    const urls = [this.data.imgServer + url];
    for (const i in this.data.itemData.itemDetail.itemImageList) {
      const item = this.data.itemData.itemDetail.itemImageList[i];
      if (item.imageKey != url) {
        urls.push(this.data.imgServer + this.data.itemData.itemDetail.itemImageList[i].imageKey);
      }
    }
    wx.previewImage({
      current: url,
      urls: urls
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    if (this.data.isSharePintuan && this.data.itemData.combinationVo.groupId) {
      this.setData({
        isSharePintuan: false
      });
      const promise = new Promise(resolve => {
        request.get('ylf/trade/imageCombiner/combination', {
          groupId: this.data.itemData.combinationVo.groupId
        }).then((res) => {
          if (res.data && res.data.shareUrl) {
            let path = '/pages/pintuan/share/index?groupId=' + this.data.itemData.combinationVo.groupId + '&shopId=' + app.globalData.sellerShop.accountId;
            if (app.globalData.spreadNumber) {
              path += '&spreadNumber=' + app.globalData.spreadNumber;
            }
            resolve({
              title: res.data.shareTitle,
              path: path,
              imageUrl: res.data.shareUrl
            });
          }
        });
      });
      let dp = '/pages/item/detail/detail?itemId=' + this.data.itemData.itemDetail.sellerItem.itemId + '&shopId=' + app.globalData.sellerShop.accountId;
      if (app.globalData.spreadNumber) {
        dp += '&spreadNumber=' + app.globalData.spreadNumber;
      }
      return {
        title: this.data.itemData.itemDetail.sellerItem.name,
        path: dp,
        imageUrl: this.data.imageDomain + this.data.itemData.itemDetail.itemImageList[0].imageKey,
        promise
      }
    } else {
      let sellerItem = this.data.itemData.itemDetail.sellerItem;
      const promise = new Promise(resolve => {
        request.getWithOutToken('ylf/trade/imageCombiner/generate?sellerItemId=' + sellerItem.id, {}).then((res) => {
          if (res.data && res.data.shareUrl) {
            let path = '/pages/item/detail/detail?itemId=' + sellerItem.itemId + '&shopId=' + app.globalData.sellerShop.accountId;
            if (app.globalData.spreadNumber) {
              path += '&spreadNumber=' + app.globalData.spreadNumber;
            }
            resolve({
              title: sellerItem.name,
              path: path,
              imageUrl: res.data.shareUrl
            });
          }
        });
      });
      let dp = '/pages/item/detail/detail?itemId=' + this.data.itemData.itemDetail.sellerItem.itemId + '&shopId=' + app.globalData.sellerShop.accountId;
      if (app.globalData.spreadNumber) {
        dp += '&spreadNumber=' + app.globalData.spreadNumber;
      }
      return {
        title: this.data.itemData.itemDetail.sellerItem.name,
        path: dp,
        imageUrl: this.data.imageDomain + this.data.itemData.itemDetail.itemImageList[0].imageKey,
        promise
      }
    }
  }
})
// pages/item/redbag/list.js
const app = getApp();
const request = require('../../../utils/request.js');
const cart = require('../../../utils/cart.js');
const UiUtils = require("../../../lib/qrcode/UiUtils.js");

Page({

  /**
   * Page initial data
   */
  data: {
    isShow: false,
    StatusBar: app.globalData.StatusBar < 21 ? 26 : app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    imageDomain: app.globalData.imageDomain,
    isReload: false,
    searchVal: '',
    redbagData: {},
    pageNum: 1,
    itemList: [],
    isLastPage: false,
    animationX: null,
    animationY: null,
    ballX: 0,
    ballY: 0,
    showBall: false,
    isLoading: false,
    cartData: app.globalData.cartData,
    isCanUse: false,
    cartAmount: 0,
    needAmount: 0
  },

  onSearchTap() {
    wx.navigateTo({
      url: '/pages/item/search/search?redbag=1&keyword=' + this.data.searchVal
    });
  },

  gotoItem(e) {
    let item = e.currentTarget.dataset['item'];
    if (item) {
      wx.navigateTo({
        url: '/pages/item/detail/detail?itemId=' + item.itemId
      })
    }
  },

  gotoCart() {
    wx.switchTab({
      url: '/pages/cart/cart',
    })
  },

  onBtnBackTap() {
    wx.navigateBack();
  },

  async onItemAddCartTap(e) {
    if (this.data.isLoading) {
      return;
    }
    app.login(() => {
      let itemId = e.currentTarget.dataset['itemId'];
      this.setCartDataItemAdd(itemId);
      this.addCart(itemId, e);
    });
  },

  async addCart(itemId, e) {
    this.setData({
      isLoading: true
    });
    const res = await request.post('ylf/trade/cart/' + app.globalData.sellerShop.accountId + '/' + itemId);
    if (res.succeed) {
      let ballX = e.touches[0].clientX;
      let ballY = e.touches[0].clientY;
      this.createAnimation(ballX, ballY);
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      });
      this.setData({
        isLoading: false
      });
    }
  },

  initCartData() {
    cart.initCartData(() => {
      this.setData({
        cartData: app.globalData.cartData
      });
      this.updateCanUseStatus();
    });
  },

  async updateCanUseStatus() {
    let isCanUse = false;
    let cartAmount = 0;
    let needAmount = 0;

    const params = {
      accountId: app.globalData.sellerShop.accountId,
      pageNumber: 1,
      batchCode: this.data.redbagData.batchCode,
      keyword: this.data.searchVal
    };
    if (!app.globalData.isInRange) {
      params.scope = 2;
    } else {
      params.scope = 1;
    }
    const res = await request.get('ylf/item/redbag/item', params);
    if (res.data && res.data.page) {
      this.setData({
        isCanUse: res.data.status == 2 ? true : false,
        cartAmount: res.data.cartPrice,
        needAmount: res.data.needPrice
      })
    }
  },

  setCartDataItemAdd(itemId) {
    cart.setCartDataItemAdd(itemId);
    this.setData({
      cartData: app.globalData.cartData
    })
  },

  setDelayTime(sec) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, sec)
    });
  },

  // 创建动画
  createAnimation(ballX, ballY) {
    var sysinfo = wx.getSystemInfoSync();
    let _this = this;
    let bottomX = UiUtils.rpx2px(480);
    let bottomY = sysinfo.windowHeight;
    let animationX = _this.flyX(bottomX, ballX); // 创建小球水平动画
    let animationY = _this.flyY(bottomY, ballY); // 创建小球垂直动画
    _this.setData({
      ballX: ballX,
      ballY: ballY,
      showBall: true
    });
    _this.setDelayTime(100).then(() => {
      // 100ms延时,  确保小球已经显示
      _this.setData({
        animationX: animationX.export(),
        animationY: animationY.export()
      })
      // 400ms延时, 即小球的抛物线时长
      return _this.setDelayTime(400);
    }).then(() => {
      _this.setData({
        animationX: _this.flyX(0, 0, 0).export(),
        animationY: _this.flyY(0, 0, 0).export(),
        showBall: false,
        isLoading: false
      });
      _this.initCartData();
    })
  },

  // 水平动画
  flyX(bottomX, ballX, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'linear',
    })
    animation.translateX(bottomX - ballX).step();
    return animation;
  },
  // 垂直动画
  flyY(bottomY, ballY, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'ease-in',
    })
    animation.translateY(bottomY - ballY).step();
    return animation;
  },

  onScrolltolower(e) {
    if (this.data.hasNextPage) {
      this.setData({
        pageNum: this.data.pageNum + 1,
      }, () => {
        this.getList();
      })
    }
  },

  async getList() {
    const params = {
      accountId: app.globalData.sellerShop.accountId,
      pageNumber: this.data.pageNum,
      batchCode: this.data.redbagData.batchCode,
      keywords: this.data.searchVal
    };
    if (!app.globalData.isInRange) {
      params.scope = 2;
    } else {
      params.scope = 1;
    }
    const res = await request.get('ylf/item/redbag/item', params);
    if (res.data && res.data.page) {
      let array = this.data.itemList.concat(res.data.page.list);
      this.setData({
        itemList: array,
        hasNextPage: res.data.page.hasNextPage,
        isShow: true,
        isCanUse: res.data.status == 2 ? true : false,
        cartAmount: res.data.cartPrice,
        needAmount: res.data.needPrice
      })
    } else {
      this.setData({
        isShow: true
      })
    }
  },

  initData() {
    this.setData({
      pageNum: 1,
      hasNextPage: false,
      itemList: [],
      isCanUse: false,
      cartAmount: 0,
      needAmount: 0
    }, () => {
      this.getList();
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    if (options.redbag) {
      this.setData({
        redbagData: JSON.parse(options.redbag)
      }, () => {
        this.initData();
      });
    } else {
      wx.navigateBack();
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {
    if (this.data.isReload) {
      this.setData({
        isReload: false
      }, () => {
        this.initData();
      })
    }
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
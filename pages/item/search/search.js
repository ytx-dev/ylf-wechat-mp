// pages/item/search/search.js
const util = require('../../../utils/util.js');
const request = require('../../../utils/request.js');
const app = getApp();

Page({

  /**
   * Page initial data
   */
  data: {
    isFromRedbag: false,
    searchValue: '',
    searchTermList: [],
    searchUserTermList: []
  },

  onChange(e) {
    this.setData({
      searchValue: e.detail.value
    });
  },

  clearInput() {
    this.setData({
      searchValue: ''
    });
  },

  onSerach() {
    this.onResult(this.data.searchValue);
  },

  onTagTap(e) {
    let value = e.currentTarget.dataset['value'];
    this.onResult(value);
  },

  onResult(value) {
    if(this.data.isFromRedbag) {
      this.gotoRedbag(value);
    } else {
      this.gotoListPage(value);
    }
  },

  gotoRedbag(value) {
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    prevPages.setData({
      searchVal: value,
      isReload: true
    }, () => {
      wx.navigateBack();
    });
  },

  gotoListPage(value) {
    wx.redirectTo({
      url: '/pages/item/list/list?keyword=' + value
    })
  },

  goBack: function () {
    wx.navigateBack();
  },

  async onCleanHistoryTap() {
    const res = await request.post('ylf/search/search/terms/clear');
    if (res.succeed) {
      wx.showToast({
        title: '清空搜索历史成功',
        icon: 'success',
        duration: 2000
      })
      this.initData();
    }
  },

  async initData() {
    // 查询搜索记录
    const res = await this.getSearchTerms();
    this.setData({
      searchTermList: res.searchTermList,
      searchUserTermList: res.searchUserTermList
    });
  },

  async getSearchTerms() {
    const res = await request.get('ylf/search/search/terms');
    return res.data;
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: options.navigationBarTitleText ? options.navigationBarTitleText : '搜索',
    });
    let keyword = '';
    let isFromRedbag = false;
    if (options.keyword) {
      keyword = options.keyword;
    }
    if(options.redbag) {
      isFromRedbag = true;
    }
    this.setData({
      searchValue: keyword,
      isFromRedbag: isFromRedbag
    }, () => {
      this.initData();
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
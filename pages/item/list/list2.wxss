/* pages/item/list/list2.wxss */
@import "../../../lib/style/font.wxss";
@import "../../../lib/colorui/icon.wxss";
@import "../../../lib/colorui/animation.wxss";

.fs18 {
  font-size: 18rpx;
}

.ce00B861 {
  color: #00B861 !important;
}

.page-list-2 {
  width: 100vw;
  height: 100vh;
}

.page-list-header {
  background: linear-gradient(135deg, #04D5A7 0%, #FCED2C 100%);
  height: 310rpx;
  position: relative;
}

.page-list-header-navbar {
  display: flex;
  align-items: center;
}

.btn-back {
  font-size: 40rpx;
  padding-left: 30rpx;
}

.navbar-search {
  width: 450rpx;
  height: 60rpx;
  background: rgb(255, 255, 255, 0.7);
  border-radius: 30rpx;
  display: flex;
  align-items: center;
  font-size: 26rpx;
  color: #00B861;
  padding-left: 20rpx;
  margin-left: 14rpx;
}

.search-icon {
  margin-right: 10rpx;
}

.page-list-header-category {
  display: flex;
  margin: 20rpx 0;
}

.page-list-header-category-scroll {
  width: 678rpx;
}

.page-list-header-category-list {
  display: flex;
  align-items: center;
}

.page-list-header-category-item {
  width: 140rpx;
  height: 164rpx;
  text-align: center;
  margin-left: 20rpx;
}

.page-list-header-category-item-image {
  width: 102rpx;
  height: 102rpx;
  border-radius: 50%;
}

.page-list-header-category-item-name {
  width: 100%;
  height: 40rpx;
  line-height: 40rpx;
  text-align: center;
  color: white;
  margin-top: 10rpx;
}

.active .page-list-header-category-item-image {
  border: solid 3rpx #00B861;
}

.active .page-list-header-category-item-name {
  background: #FFFFFF;
  border-radius: 24rpx;
  color: #00B861;
}

.page-list-header-category-all {
  width: 72rpx;
  text-align: center;
  padding-top: 24rpx;
  position: relative;
  box-shadow: -3rpx 0px 60rpx -30rpx rgba(0, 0, 0, 0.89);
}

.page-list-header-category-all-text {
  font-size: 26rpx;
  font-weight: 500;
  color: #00B861;
  text-align: center;
}

.page-list-header-category-all-img {
  width: 30rpx;
  height: 30rpx;
  margin-top: 4rpx;
}

.page-list-category {
  background-color: white;
  margin-top: -24rpx;
  border-top-left-radius: 24rpx;
  border-top-right-radius: 24rpx;
  position: relative;
  display: flex;
}

.page-list-category-left {
  width: 224rpx;
  height: 100%;
  box-shadow: 0px 0px 9px 0px rgba(225, 225, 225, 0.5);
}

.page-list-category-left-scroll {
  height: 100%;
}

.page-list-category-left-list {
  padding-top: 10rpx;
  padding-bottom: 30rpx;
}

.page-list-category-left-item {
  text-align: center;
  position: relative;
  padding-top: 20rpx;
  padding-bottom: 20rpx;
}

.page-list-category-left-item image {
  width: 184rpx;
  height: 48rpx;
}

.page-list-category-left-item-title {
  font-size: 26rpx;
  color: #6A6A6C;
  width: 224rpx;
  line-height: 48rpx;
}

.page-list-category-left-list .active .page-list-category-left-item-title {
  font-size: 28rpx;
  color: #FFFFFF;
  position: absolute;
  top: 20rpx;
}

.page-list-category-right {
  width: 526rpx;
  height: 100%;
}

.tabs {
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin-top: 30rpx;
  margin-bottom: 20rpx;
}

.tab-item-title {
  font-size: 24rpx;
  font-weight: 500;
  color: #27292A;
  display: flex;
  align-items: center;
  justify-content: center;
}

.tabs .active .tab-item-title {
  color: #00B861;

}

.tabs .tab-item-title .tab-item-icon {
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 30rpx;
  line-height: 20rpx;
  margin-left: 5rpx;
}

.tabs .tab-item-title .tab-item-icon .iconfont {
  display: block;
  font-size: 14rpx;
}

.tabs .tab-item-title .tab-item-icon .icon-sortUp {
  margin-bottom: -4rpx;
  margin-top: 1rpx;
  color: #27292A;
}

.tabs .tab-item-title .tab-item-icon .icon-sortDown {
  margin-top: -4rpx;
  color: #27292A;
}

.page-list-category-right-scroll {
  height: calc(100% - 88rpx);
}

.page-list-category-right-item-list {
  padding: 10rpx 20rpx 20rpx 20rpx;
}

.page-list-category-right-item {
  display: flex;
  margin-bottom: 44rpx;
  position: relative;
}

.page-list-category-right-item-image {
  width: 140rpx;
  min-width: 140rpx;
  height: 140rpx;
  margin-right: 20rpx;
  border-radius: 20rpx;
}

.page-list-category-right-item-body {
  width: 100%;
}

.page-list-category-right-item-body-name {
  font-size: 26rpx;
  font-weight: 500;
  color: #27292A;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
  display: -webkit-box;
  word-break: break-all;
  min-height: 50rpx;
}

.page-list-category-right-item-body-sku {
  font-size: 22rpx;
  color: #7E7E7E;
  margin-bottom: 10rpx;
}

.page-list-category-right-item-body-row {
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
}

.page-list-category-right-item-body-price {
  font-size: 20rpx;
  color: #FA2B22;
}

.price-value {
  font-size: 36rpx;
  font-weight: 500;
}

.page-list-category-right-item-body-sold-num {
  display: flex;
  align-items: center;
  font-size: 24rpx;
  font-weight: 400;
  color: #999999;
}

.page-list-category-right-item-body-sold-num image {
  width: 24rpx;
  height: 24rpx;
  margin-right: 10rpx;
}

.page-list-category-right-item-body-addcart {
  width: 46rpx;
  height: 46rpx;
}

.item-tip {
  position: absolute;
  top: 106rpx;
  left: 0;
  width: 140rpx;
  text-align: center;
  background-color: rgba(0, 0, 0, 0.6);
  color: white;
  border-radius: 20rpx;
}

.page-list-category-right-empty {
  font-size: 28rpx;
  font-weight: 500;
  color: #27292A;
  text-align: center;
}

.page-list-category-right-empty image {
  width: 250rpx;
  height: 150rpx;
  margin-bottom: 30rpx;
  margin-top: 10vh;
}

.page-list-category-all-pop {
  background-color: rgba(0, 0, 0, 0.7);
  position: fixed;
  width: 100vw;
}

.page-list-category-all-pop-header {
  background-color: white;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 34rpx 20rpx;
}

.page-list-category-all-pop-header-title {
  display: flex;
  align-items: center;
}

.page-list-category-all-pop-header-title-prefix {
  width: 8rpx;
  height: 28rpx;
  background: #00B861;
  border-radius: 2rpx;
}

.page-list-category-all-pop-header-title-text {
  font-size: 30rpx;
  font-weight: 500;
  color: #27292A;
  margin-left: 20rpx;
}

.page-list-category-all-pop-header-close {
  width: 30rpx;
  height: 30rpx;
}

.page-list-category-all-pop-list {
  background-color: white;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  padding: 10rpx 20rpx 20rpx 20rpx;
}

.page-list-category-all-pop-item {
  width: 140rpx;
  text-align: center;
  margin-bottom: 36rpx;
}

.page-list-category-all-pop-item-image {
  width: 102rpx;
  height: 102rpx;
  border-radius: 50%;
}

.page-list-category-all-pop-item-name {
  font-size: 24rpx;
  font-weight: 500;
  color: #27292A;
  width: 134rpx;
  height: 41rpx;
  line-height: 41rpx;
}

.active .page-list-category-all-pop-item-image {
  border: 4rpx solid #00B861;
}

.active .page-list-category-all-pop-item-name {
  background-color: #00B861;
  border-radius: 24rpx;
  color: white;
}

.refresh-container {
  width: 100%;
  height: 45px;
  display: flex;
  align-items: center;
  justify-content: center;
}

.btn-cart-float {
  position: fixed;
}

.btn-cart-float-body {
  position: relative;
  width: 100rpx;
  height: 100rpx;
  background-color: #00B861;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
}

.btn-cart-float-body image {
  width: 60rpx;
  height: 60rpx;
}

.cart-num-float {
  position: absolute;
  top: 10rpx;
  right: 20rpx;
  width: 32rpx;
  height: 32rpx;
  border-radius: 50%;
  background-color: #FE3B30;
  font-size: 20rpx;
  color: #FFFFFF;
  line-height: 32rpx;
  text-align: center;
}

.ball {
  width: 18px;
  height: 18px;
  border-radius: 50%;
  overflow: hidden;
  z-index: +99;
  background-color: #00B861;
}

.page-list-category-right-scroll-next-category-text {
  position: fixed;
  bottom: 40rpx;
  width: 526rpx;
  text-align: center;
}

.add-cart-group {
  position: relative;
  width: 46rpx;
  height: 46rpx;
}

.add-cart-num {
  position: absolute;
  right: -10rpx;
  top: -13rpx;
  background-color: #FA2B22;
  font-size: 20rpx;
  color: #FFFFFF;
  text-align: center;
  border-top-left-radius: 16rpx;
  border-top-right-radius: 16rpx;
  border-bottom-right-radius: 16rpx;
  padding: 2rpx 6rpx;
}

.item-activity {
  color: white;
  background-color: #FA2B22;
  font-size: 22rpx;
  padding-right: 10rpx;
  padding-left: 10rpx;
  width: max-content;
  height: 34rpx;
  line-height: 34rpx;
  text-align: center;
  border-radius: 8rpx;
  margin-top: 14rpx;
}

.item-original-price {
  font-size: 20rpx;
  color: #7E7E7E;
  text-decoration: line-through;
}

.self-mention-tip {
  width: 90rpx;
  padding-right: 8rpx;
  padding-left: 8rpx;
  border-radius: 8rpx;
  border: 2rpx solid #F52D2E;
  font-size: 22rpx;
  font-weight: 400;
  color: #F52D2E;
  margin-right: 8rpx;
}
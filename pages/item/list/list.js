// pages/item/list/list.js
const app = getApp();
const request = require('../../../utils/request.js');
const UiUtils = require("../../../lib/qrcode/UiUtils.js");
const cart = require('../../../utils/cart.js');

Page({

  /**
   * Page initial data
   */
  data: {
    scrollHeight: 550,
    imageDomain: app.globalData.imageDomain,
    searchVal: '',
    needSearch: true,
    categoryId: '',
    currCategoryId: null,
    selectedTag: null,
    categoryList: [],
    sortType: 'desc',
    sort: 'default',
    pageNum: 1,
    itemList: [],
    recommendResult: false,
    term: '',
    isLastPage: false,
    scrollTop: {
      scroll_top: 0,
      goTop_show: false
    },
    cartNum: 0,
    animationX: null,
    animationY: null,
    ballX: 0,
    ballY: 0,
    showBall: false,
    isLoading: false,
    cartData: app.globalData.cartData
  },

  setDelayTime(sec) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, sec)
    });
  },

  // 创建动画
  createAnimation(ballX, ballY) {
    let _this = this;
    let bottomX = UiUtils.rpx2px(680);
    let bottomY = UiUtils.rpx2px(80);
    let animationX = _this.flyX(bottomX, ballX); // 创建小球水平动画
    let animationY = _this.flyY(bottomY, ballY); // 创建小球垂直动画
    _this.setData({
      ballX: ballX,
      ballY: ballY,
      showBall: true
    });
    _this.setDelayTime(100).then(() => {
      // 100ms延时,  确保小球已经显示
      _this.setData({
        animationX: animationX.export(),
        animationY: animationY.export()
      })
      // 400ms延时, 即小球的抛物线时长
      return _this.setDelayTime(400);
    }).then(() => {
      _this.setData({
        animationX: _this.flyX(0, 0, 0).export(),
        animationY: _this.flyY(0, 0, 0).export(),
        showBall: false,
        isLoading: false
      });
      _this.initCartData();
    })
  },

  // 水平动画
  flyX(bottomX, ballX, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'linear',
    })
    animation.translateX(bottomX - ballX).step();
    return animation;
  },
  // 垂直动画
  flyY(bottomY, ballY, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'ease-in',
    })
    animation.translateY(bottomY - ballY).step();
    return animation;
  },

  gotoCart() {
    wx.switchTab({
      url: '/pages/cart/cart',
    })
  },

  gotoItem(e) {
    let item = e.currentTarget.dataset['item'];
    if (item) {
      wx.navigateTo({
        url: '/pages/item/detail/detail?itemId=' + item.itemId
      })
    }
  },

  onSearchTap() {
    wx.redirectTo({
      url: '/pages/item/search/search?keyword=' + this.data.searchVal
    });
  },

  onTabItemTap(e) {
    this.setData({
      pageNum: 1,
      itemList: [],
      sort: e.currentTarget.dataset.sort,
      sortType: e.currentTarget.dataset.sort === 'price' ? this.data.sortType === 'desc' ? 'asc' : 'desc' : 'desc'
    }, () => {
      this.getList();
    });
  },

  onCategoryItemTap(e) {
    const id = e.currentTarget.dataset['id'] ? e.currentTarget.dataset['id'] : null;
    this.setData({
      pageNum: 1,
      itemList: [],
      currCategoryId: id,
      selectedTag: id
    }, () => {
      this.getList();
    });
  },

  async onItemAddCartTap(e) {
    if(this.data.isLoading) {
      return;
    }
    app.login(() => {
      let itemId = e.currentTarget.dataset['itemId'];
      this.setCartDataItemAdd(itemId);
      this.addCart(itemId, e);
    });
  },

  async addCart(itemId, e) {
    this.setData({
      isLoading: true
    });
    const res = await request.post('ylf/trade/cart/' + app.globalData.sellerShop.accountId + '/' + itemId);
    if (res.succeed) {
      let ballX = e.touches[0].clientX;
      let ballY = e.touches[0].clientY;
      this.createAnimation(ballX, ballY);
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      });
      this.setData({
        isLoading: false
      });
    }
  },

  initCartData() {
    cart.initCartData(() => {
      let count = cart.getCartSize();
      this.setData({
        cartData: app.globalData.cartData,
        cartNum: count
      })
    });
  },

  setCartDataItemAdd(itemId) {
    cart.setCartDataItemAdd(itemId);
    let count = cart.getCartSize();
    this.setData({
      cartData: app.globalData.cartData,
      cartNum: count
    })
  },

  goTopFun() {
    var _top = this.data.scrollTop.scroll_top;
    if (_top == 1) {
      _top = 0;
    } else {
      _top = 1;
    }
    this.setData({
      'scrollTop.scroll_top': _top
    });
  },

  onScrolltolower(e) {
    if (this.data.hasNextPage) {
      this.setData({
        pageNum: this.data.pageNum + 1,
      }, () => {
        this.getList();
      })
    }
  },

  initData() {
    this.initCartData();
    this.getList();
    this.getClassificationList();
  },

  async getList() {
    // wx.showLoading({
    //   title: "加载中..."
    // });
    const params = {
      sellerAccountId: app.globalData.sellerShop.accountId,
      sort: this.data.sort,
      sortType: this.data.sortType,
      pageNum: this.data.pageNum,
      categoryId: this.data.currCategoryId ? this.data.currCategoryId : this.data.categoryId,
      keyword: this.data.searchVal
    };
    if (!app.globalData.isInRange) {
      params.type = 2;
    }
    const res = await request.get('ylf/search/search', params);
    this.setData({
      recommendResult: res.data.itemSearchResult.recommendResult,
      term: res.data.itemSearchResult.term,
      itemList: this.data.itemList.concat(res.data.itemSearchResult.itemPage.list),
      hasNextPage: res.data.itemSearchResult.itemPage.pageNum < res.data.itemSearchResult.itemPage.pages
    })
    // wx.hideLoading();
  },

  async getClassificationList() {
    if (this.data.categoryId) {
      let url = 'ylf/item/classification/list?accountId=' + app.globalData.sellerShop.accountId + '&classificationId=' + this.data.categoryId;
      if (!app.globalData.isInRange) {
        url += '&type=2';
      }
      const res = await request.get(url);
      this.setData({
        categoryList: res.data.list
      });
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: options.navigationBarTitleText ? decodeURIComponent(options.navigationBarTitleText) : '搜索',
    });
    let currCategoryId = '0';
    let selectedTag = '0';
    if (options.categoryId === options.categoryIds) {
      currCategoryId = options.categoryId;
    } else {
      let array = options.categoryIds.split(',');
      currCategoryId = array[array.length - 1];
      selectedTag = currCategoryId;
    }
    this.setData({
      categoryId: options.categoryId ? options.categoryId : '',
      currCategoryId: Number(currCategoryId),
      selectedTag: Number(selectedTag),
      searchVal: options.keyword ? options.keyword : ''
    }, () => {
      this.initData();
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {
    let screenHeight = wx.getSystemInfoSync().windowHeight;
    const query = wx.createSelectorQuery();
    query.select('.header').boundingClientRect();
    query.selectViewport().scrollOffset();
    query.exec((res) => {
      this.setData({
        scrollHeight: (screenHeight - res[0].height)
      });
    })
  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    this.setData({
      cartData: app.globalData.cartData
    })
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
// pages/item/list/list2.js
const app = getApp();
const request = require('../../../utils/request.js');
const cart = require('../../../utils/cart.js');

Page({

  /**
   * Page initial data
   */
  data: {
    StatusBar: app.globalData.StatusBar < 21 ? 26 : app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    imageDomain: app.globalData.imageDomain,
    categoryList: [],
    currCategoryId: 0,
    subCategoryList: [],
    currSubCategoryId: 0,
    options: {},
    itemList: [],
    sortType: 'desc',
    sort: 'default',
    pageNum: 1,
    isLastPage: false,
    isShowAllCategory: false,
    triggered: false,
    cartNum: 0,
    floatBtn: {
      x: 300,
      y: 750
    },
    setFloatBtnPointTimeoutId: -1,
    animationX: null,
    animationY: null,
    ballX: 0,
    ballY: 0,
    showBall: false,
    isLoading: false,
    scrollTopStart: 0,
    isShowGotoNextCategory: false,
    cartData: app.globalData.cartData,
    isLower: false,
    scrollTop: 0
  },

  setDelayTime(sec) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, sec)
    });
  },

  // 创建动画
  createAnimation(ballX, ballY) {
    let _this = this;
    let bottomX = this.data.floatBtn['x'];
    let bottomY = this.data.floatBtn['y'];
    let animationX = _this.flyX(bottomX, ballX); // 创建小球水平动画
    let animationY = _this.flyY(bottomY, ballY); // 创建小球垂直动画
    _this.setData({
      ballX: ballX,
      ballY: ballY,
      showBall: true
    });
    _this.setDelayTime(100).then(() => {
      // 100ms延时,  确保小球已经显示
      _this.setData({
        animationX: animationX.export(),
        animationY: animationY.export()
      })
      // 400ms延时, 即小球的抛物线时长
      return _this.setDelayTime(400);
    }).then(() => {
      _this.setData({
        animationX: _this.flyX(0, 0, 0).export(),
        animationY: _this.flyY(0, 0, 0).export(),
        showBall: false,
        isLoading: false
      });
      _this.initCartData();
    })
  },

  // 水平动画
  flyX(bottomX, ballX, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'linear',
    })
    animation.translateX(bottomX - ballX).step();
    return animation;
  },
  // 垂直动画
  flyY(bottomY, ballY, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'ease-in',
    })
    animation.translateY(bottomY - ballY).step();
    return animation;
  },

  gotoCart() {
    wx.switchTab({
      url: '/pages/cart/cart',
    })
  },

  gotoItem(e) {
    let item = e.currentTarget.dataset['item'];
    if (item) {
      wx.navigateTo({
        url: '/pages/item/detail/detail?itemId=' + item.itemId
      })
    }
  },

  onMovableChange(e) {
    if (this.data.setFloatBtnPointTimeoutId > -1) {
      clearTimeout(this.data.setFloatBtnPointTimeoutId);
    }
    let id = setTimeout(this.setFloatBtnPoint, 1000, e.detail);
    this.setData({
      setFloatBtnPointTimeoutId: id
    });
  },

  setFloatBtnPoint(point) {
    let newFloatBtn = this.data.floatBtn;
    newFloatBtn.x = point.x;
    newFloatBtn.y = point.y;
    this.setData({
      floatBtn: newFloatBtn
    })
    wx.setStorage({
      key: 'floatBtnLocation',
      data: JSON.stringify(newFloatBtn)
    });
  },

  async onItemAddCartTap(e) {
    if(this.data.isLoading) {
      return;
    }
    app.login(() => {
      let itemId = e.currentTarget.dataset['itemId'];
      this.setCartDataItemAdd(itemId);
      this.addCart(itemId, e);
    });
  },

  async addCart(itemId, e) {
    this.setData({
      isLoading: true
    });
    const res = await request.post('ylf/trade/cart/' + app.globalData.sellerShop.accountId + '/' + itemId);
    if (res.succeed) {
      let ballX = e.touches[0].clientX;
      let ballY = e.touches[0].clientY;
      this.createAnimation(ballX, ballY);
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      });
      this.setData({
        isLoading: false
      });
    }
  },

  initCartData() {
    cart.initCartData(() => {
      let count = cart.getCartSize();
      this.setData({
        cartData: app.globalData.cartData,
        cartNum: count
      })
    });
  },

  setCartDataItemAdd(itemId) {
    cart.setCartDataItemAdd(itemId);
    let count = cart.getCartSize();
    this.setData({
      cartData: app.globalData.cartData,
      cartNum: count
    })
  },

  onPopListTap() {
    //do nothing
  },

  onPopCategoryItemTap(e) {
    let item = e.currentTarget.dataset['item'];
    this.onCategoryItemChange(item);
    this.onPopCloseTap();
  },

  onPopCloseTap() {
    this.setData({
      isShowAllCategory: false
    })
  },

  onShowAllCategoryTap() {
    this.setData({
      isShowAllCategory: true
    })
  },

  onCategoryItemTap(e) {
    let item = e.currentTarget.dataset['item'];
    this.onCategoryItemChange(item);
  },

  onCategoryItemChange(item) {
    this.setData({
      currCategoryId: item.id,
      subCategoryList: item.subList,
      currSubCategoryId: item.subList && item.subList.length > 0 ? item.subList[0].id : 0,
      pageNum: 1,
    }, () => {
      this.getItemList();
    })
  },

  onSubCategoryItemTap(e) {
    let item = e.currentTarget.dataset['item'];
    this.setData({
      currSubCategoryId: item.id,
      pageNum: 1,
    }, () => {
      this.getItemList();
    })
  },

  onTabItemTap(e) {
    this.setData({
      pageNum: 1,
      itemList: [],
      sort: e.currentTarget.dataset.sort,
      sortType: e.currentTarget.dataset.sort === 'price' ? this.data.sortType === 'desc' ? 'asc' : 'desc' : 'desc'
    }, () => {
      this.getItemList();
    });
  },

  async getItemList() {
    const params = {
      sellerAccountId: app.globalData.sellerShop.accountId,
      sort: this.data.sort,
      sortType: this.data.sortType,
      pageNum: this.data.pageNum,
      categoryId: this.data.currSubCategoryId,
    };
    if (!app.globalData.isInRange) {
      params.type = 2;
    }
    const res = await request.get('ylf/search/search', params);
    this.setData({
      itemList: this.data.pageNum == 1 ? res.data.itemSearchResult.itemPage.list : this.data.itemList.concat(res.data.itemSearchResult.itemPage.list),
      hasNextPage: res.data.itemSearchResult.itemPage.pageNum < res.data.itemSearchResult.itemPage.pages,
      isLower: false
    })
    if(this.data.pageNum < 2) {
      this.setData({
        scrollTop: 0
      });
    }
  },

  onRefresh(e) {
    this.setData({
      triggered: true
    });
    let preId = 0;
    let newId = 0;
    for (const i in this.data.subCategoryList) {
      let item = this.data.subCategoryList[i];
      if (item.id == this.data.currSubCategoryId && preId) {
        newId = preId;
        break;
      }
      preId = item.id;
    }
    if (newId) {
      this.setData({
        triggered: false,
        currSubCategoryId: newId,
        pageNum: 1,
      }, () => {
        this.getItemList();
      });
    } else {
      this.setData({
        triggered: false
      });
    }
  },

  onTouchStart(e) {
    if (!this.data.hasNextPage && this.data.isLower) {
      this.setData({
        scrollTopStart: e.changedTouches[0].clientY
      })
    }
  },

  onTouchMove(e) {
    if (!this.data.hasNextPage && this.data.isLower) {
      let scrollTopEnd = e.changedTouches[0].clientY;
      let scrollTopStart = this.data.scrollTopStart;
      let isShowGotoNextCategory = false;
      if (scrollTopStart - scrollTopEnd > 50) {
        isShowGotoNextCategory = true;
      }
      this.setData({
        isShowGotoNextCategory: isShowGotoNextCategory
      })
    }
  },

  onTouchEnd(e) {
    if (!this.data.hasNextPage && this.data.isLower) {
      let scrollTopEnd = e.changedTouches[0].clientY;
      let scrollTopStart = this.data.scrollTopStart;
      if (scrollTopStart - scrollTopEnd > 50) {
        this.gotoNextCategory();
      } else {
        this.setData({
          isShowGotoNextCategory: false,
          scrollTopStart: 0
        })
      }
    }
  },

  onScrolltolower(e) {
    if (this.data.hasNextPage) {
      this.setData({
        isLower: false,
        pageNum: this.data.pageNum + 1,
      }, () => {
        this.getItemList();
      })
    } else {
      this.setData({
        isLower: true
      })
    }
  },

  gotoNextCategory() {
    let nextIndex = -1;
    let nextId = 0;
    for (const i in this.data.subCategoryList) {
      let item = this.data.subCategoryList[i];
      if (item.id == this.data.currSubCategoryId) {
        nextIndex = Number(i) + 1;
      }
      if (nextIndex == i) {
        nextId = item.id;
        break;
      }
    }
    if (nextId) {
      this.setData({
        isShowGotoNextCategory: false,
        scrollTopStart: 0,
        triggered: false,
        currSubCategoryId: nextId,
        pageNum: 1,
      }, () => {
        this.getItemList();
      });
    } else {
      this.setData({
        isShowGotoNextCategory: false,
        scrollTopStart: 0,
        triggered: false
      });
    }
  },

  async getClassificationList() {
    let url = 'ylf/item/classification/list?accountId=' + app.globalData.sellerShop.accountId;
    if (!app.globalData.isInRange) {
      url += '&type=2';
    }
    const res = await request.get(url);
    return res.data.list;
  },

  onBtnBackTap() {
    wx.navigateBack();
  },

  onSearchTap(e) {
    wx.navigateTo({
      url: '/pages/item/search/search',
    });
  },

  async initData() {
    let categoryList = await this.getClassificationList();
    if (categoryList && categoryList.length > 0) {
      let currCategoryId;
      let subCategoryList = [];
      let currSubCategoryId;
      if (this.data.options && this.data.options.categoryId) {
        let ids = this.data.options.categoryIds.split(',');
        for (const i in categoryList) {
          let item = categoryList[i];
          if (item.id == ids[0]) {
            currCategoryId = item.id;
            if (ids.length > 1) {
              for (const si in item.subList) {
                let sitem = item.subList[si];
                if (sitem.id == ids[1]) {
                  currSubCategoryId = sitem.id;
                  break;
                }
              }
            }
            subCategoryList = item.subList;
            break;
          }
        }
      }
      if (!currCategoryId) {
        currCategoryId = categoryList[0].id;
        subCategoryList = categoryList[0].subList;
        currSubCategoryId = subCategoryList[0].id;
      }
      if (!currSubCategoryId) {
        currSubCategoryId = subCategoryList[0].id;
      }
      this.setData({
        categoryList: categoryList,
        currCategoryId: currCategoryId,
        subCategoryList: subCategoryList,
        currSubCategoryId: currSubCategoryId,
        pageNum: 1,
        isShowGotoNextCategory: false,
        scrollTopStart: 0
      }, () => {
        this.getItemList();
      });
    } else {
      this.setData({
        categoryList: [],
        currCategoryId: 0,
        subCategoryList: [],
        currSubCategoryId: 0,
        itemList: [],
        pageNum: 1,
        isShowGotoNextCategory: false,
        scrollTopStart: 0
      });
    }
    this.initCartData();
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    let floatBtn = {}
    let floatBtnStr = wx.getStorageSync('floatBtnLocation');
    if (floatBtnStr) {
      floatBtn = JSON.parse(floatBtnStr);
    } else {
      var res = wx.getSystemInfoSync();
      floatBtn = {
        x: res.windowWidth - 90,
        y: res.windowHeight - 90
      }
    }
    this.setData({
      floatBtn: floatBtn,
      options: options
    }, () => {
      this.initData();
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    this.setData({
      cartData: app.globalData.cartData
    })
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
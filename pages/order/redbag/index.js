// pages/order/redbag/index.js
const util = require('../../../utils/util.js');
const app = getApp();

Page({

  /**
   * Page initial data
   */
  data: {
    showIndex: -1,
    selectId: -1,
    redBagList: []
  },

  gotoRedbagRule() {
    wx.navigateTo({
      url: '/pages/user/redbag/rule',
    })
  },

  onItemTap(e) {
    const item = JSON.parse(JSON.stringify(e.currentTarget.dataset.item));
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    let order = prevPages.data.purchaseData;
    if(item.denomination > order.orderAmount) {
      item.denomination = order.orderAmount;
    }
    prevPages.setData({
      redbag: item,
      isReload: false
    }, () => {
      prevPages.onSelectCardHide();
      wx.navigateBack();
    });
  },

  onBtnNoRedbagTap(e) {
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    prevPages.setData({
      redbag: null,
      isReload: false
    }, () => {
      prevPages.onSelectCardHide();
      wx.navigateBack();
    });
  },

  showDetail(e) {
    const index = e.currentTarget.dataset.index;
    if (index === this.data.showIndex) {
      this.setData({
        showIndex: -1
      })
    } else {
      this.setData({
        showIndex: index
      })
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    const usable = prevPages.data.purchaseData.redBagMap.usable;
    let list = [];
    for (const i in usable) {
      const item = usable[i];
      item.isShowExtime = (item.efficientEndDate - item.now) / 1000 < item.overTime;
      item.efficientDate = util.formatTime2(new Date(item.efficientDate), '-');
      item.efficientEndDate = util.formatTime2(new Date(item.efficientEndDate), '-');
      list.push(item);
    }
    this.setData({
      redBagList: list,
      selectId: options.redbagId ? Number(options.redbagId) : -1
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
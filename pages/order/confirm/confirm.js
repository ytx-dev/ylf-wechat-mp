// pages/order/confirm/confirm.js
const app = getApp();
const request = require('../../../utils/request.js');
const util = require('../../../utils/util.js');
const pauyUtil = require('../../../utils/pay.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    isScroll: true,
    isReload: false,
    show: false,
    cartLineIds: [],
    addressId: null,
    purchaseData: {},
    fetchType: 0,
    fetchMobile: app.globalData.account ? app.globalData.account.mobile : '',
    fetchTime: '',
    fetchDateList: [],
    fetchDateSelect: '',
    fetchTimeList: [],
    fetchTimeSelect: {},
    fetchTimeSelectTemp: {},
    isSetFetchTime: false,
    riderFetchTime: '',
    riderFetchDateList: [],
    riderFetchDateSelect: '',
    riderFetchTimeList: [],
    riderFetchTimeSelect: {},
    riderFetchTimeSelectTemp: {},
    isSetRiderFetchTime: false,
    isCommitOrder: false,
    outStock: '其它商品继续配送(缺货商品退款)',
    outStockValue: {
      '其它商品继续配送(缺货商品退款)': 1,
      '电话与我沟通': 2
    },
    remark: '',
    paying: false,
    invoice: null,
    redbag: {},
    isUseCard: true,
    cardUse: 0,
    alertText: '',
    isShowAlert: false,
    isShowAddrAlert: false,
    ITEM_TYPE_TEXT: {
      1: '折',
      2: '秒'
    },
    isLoseFlod: true,
    isPintuan: false,
    pintuan: {},
    headImg: '',
    isLive: false,
    liveData: {},
    isShowShoppingFee: false
  },

  showShoppingFeeDialog() {
    this.setData({
      isShowShoppingFee: true
    })
  },

  onSetShoppingFeeClose() {
    this.setData({
      isShowShoppingFee: false
    })
  },

  initNameAndHead() {
    let fullHeadImg = (app.globalData.account && app.globalData.account.headImg ? app.globalData.account.headImg : (app.globalData.userInfo && app.globalData.userInfo.avatarUrl ? app.globalData.userInfo.avatarUrl : ''));
    if (!(/http|https/.test(fullHeadImg))) {
      fullHeadImg = app.globalData.imageDomain + fullHeadImg;
    }
    this.setData({
      headImg: fullHeadImg
    });
  },

  emptyHandler() {

  },

  onLoseFlodTap() {
    this.setData({
      isLoseFlod: !this.data.isLoseFlod
    })
  },

  onFetchTypeChange(e) {
    let val = e.currentTarget.dataset['type'];
    this.setData({
      fetchType: val
    }, () => {
      this.initData();
    });
  },

  /** 骑手 */

  setRiderFetchTime(isCommitOrder = false) {
    let riderFetchDateList = this.data.riderFetchDateList;
    let riderFetchDateSelect = this.data.riderFetchDateSelect;
    let riderFetchTimeList = this.data.riderFetchTimeList;
    let riderFetchTimeSelect = this.data.riderFetchTimeSelect;
    if (riderFetchDateList.length == 2) {
      let array = riderFetchDateList[0].children;
      let newArray = [];
      let currTime = new Date().Format('hh:mm');
      if (array) {
        for (const i in array) {
          let item = array[i];
          if (item.startTime > currTime) {
            newArray.push(item);
          }
        }
      }
      if (newArray.length > 0) {
        riderFetchDateList[0].children = newArray;
        if (this.data.riderFetchDateSelect.startsWith('今天')) {
          riderFetchTimeList = newArray;
          if (riderFetchTimeSelect.startTime < currTime) {
            riderFetchTimeSelect = {};
          }
        }
      } else {
        riderFetchDateList = [riderFetchDateList[1]];
        riderFetchDateSelect = riderFetchDateList[0].name;
        riderFetchTimeList = riderFetchDateList[0].children;
      }
    }
    this.setData({
      riderFetchDateList: riderFetchDateList,
      riderFetchDateSelect: riderFetchDateSelect,
      riderFetchTimeSelectTemp: riderFetchDateSelect,
      riderFetchTimeList: riderFetchTimeList,
      isSetRiderFetchTime: true,
      isCommitOrder: isCommitOrder
    })
  },

  onSetRiderFetchTimeClose() {
    this.setData({
      isSetRiderFetchTime: false
    })
  },

  onSetRiderFetchTimeOk() {
    if (this.data.riderFetchTimeSelectTemp && this.data.riderFetchTimeSelectTemp.id) {
      this.setData({
        riderFetchTimeSelect: this.data.riderFetchTimeSelectTemp,
        riderFetchTime: this.data.riderFetchDateSelect.startsWith('今天') ? (this.data.riderFetchTimeSelectTemp.startTime + ' - ' + this.data.riderFetchTimeSelectTemp.endTime) : (this.data.riderFetchDateSelect + ' ' + this.data.riderFetchTimeSelectTemp.startTime + ' - ' + this.data.riderFetchTimeSelectTemp.endTime),
        isSetRiderFetchTime: false
      }, () => {
        if (this.data.isCommitOrder == true) {
          this.payment();
        }
      })
    } else {
      wx.showToast({
        title: '请选择配送时间',
        icon: 'none'
      })
    }
  },

  setRiderFetchSelectDate(e) {
    let data = e.currentTarget.dataset['data'];
    this.setData({
      riderFetchDateSelect: data.name,
      riderFetchTimeList: data.children

    });
  },

  setRiderFetchSelectTime(e) {
    let data = e.currentTarget.dataset['data'];
    if (data.status != 1) {
      this.setData({
        riderFetchTimeSelectTemp: data
      });
    }
  },

  /** 自提 */
  setFetchTime(isCommitOrder = false) {
    let fetchDateList = this.data.fetchDateList;
    let fetchDateSelect = this.data.fetchDateSelect;
    let fetchTimeList = this.data.fetchTimeList;
    let fetchTimeSelect = this.data.fetchTimeSelect;
    if (fetchDateList.length == 2) {
      let array = fetchDateList[0].children;
      let newArray = [];
      let currTime = new Date().Format('hh:mm');
      if (array) {
        for (const i in array) {
          let item = array[i];
          if (item.startTime > currTime) {
            newArray.push(item);
          }
        }
      }
      if (newArray.length > 0) {
        fetchDateList[0].children = newArray;
        if (this.data.fetchDateSelect.startsWith('今天')) {
          fetchTimeList = newArray;
          if (fetchTimeSelect.startTime < currTime) {
            fetchTimeSelect = {};
          }
        }
      } else {
        fetchDateList = [fetchDateList[1]];
        fetchDateSelect = fetchDateList[0].name;
        fetchTimeList = fetchDateList[0].children;
      }
    }
    this.setData({
      fetchDateList: fetchDateList,
      fetchDateSelect: fetchDateSelect,
      fetchTimeSelectTemp: fetchDateSelect,
      fetchTimeList: fetchTimeList,
      isSetFetchTime: true,
      isCommitOrder: isCommitOrder
    })
  },

  onSetFetchTimeClose() {
    this.setData({
      isSetFetchTime: false
    })
  },

  onSetFetchTimeOk() {
    if (this.data.fetchTimeSelectTemp && this.data.fetchTimeSelectTemp.id) {
      this.setData({
        fetchTimeSelect: this.data.fetchTimeSelectTemp,
        fetchTime: this.data.fetchDateSelect.startsWith('今天') ? (this.data.fetchTimeSelectTemp.startTime + ' - ' + this.data.fetchTimeSelectTemp.endTime) : (this.data.fetchDateSelect + ' ' + this.data.fetchTimeSelectTemp.startTime + ' - ' + this.data.fetchTimeSelectTemp.endTime),
        isSetFetchTime: false
      }, () => {
        if (this.data.isCommitOrder == true) {
          this.payment();
        }
      })
    } else {
      wx.showToast({
        title: '请选择自提时间',
        icon: 'none'
      })
    }
  },

  setFetchSelectDate(e) {
    let data = e.currentTarget.dataset['data'];
    this.setData({
      fetchDateSelect: data.name,
      fetchTimeList: data.children

    });
  },

  setFetchSelectTime(e) {
    let data = e.currentTarget.dataset['data'];
    if (data.status != 1) {
      this.setData({
        fetchTimeSelectTemp: data
      });
    }
  },

  setFetchMobile(e) {
    this.setData({
      fetchMobile: e.detail.value
    })
  },

  onFetchSwiperChange(e) {
    if (e.detail.source == 'touch') {
      this.setData({
        fetchType: e.detail.current
      }, () => {
        this.initData();
      });
    }
  },

  async initData() {
    wx.showLoading({
      title: '载入中',
      mask: true
    });
    const res = await this.purchaseOrder();
    if (res.data.purchase) {
      let purchase = res.data.purchase;
      let fetchDateList = [];
      let weekDays = ["日", "一", "二", "三", "四", "五", "六"];
      if (purchase.today && purchase.today.length > 0) {
        let newDateStr = '今天';
        let date = new Date();
        newDateStr += '(周' + weekDays[date.getDay()] + ')';
        let array = purchase.today;
        for (const i in array) {
          let item = array[i];
          item.id = item.dateTime + ' ' + item.startTime + ' ' + item.endTime;
          item.startTimeVal = item.dateTime + ' ' + item.startTime;
          item.endTimeVal = item.dateTime + ' ' + item.endTime;
        }
        fetchDateList.push({
          name: newDateStr,
          children: array
        });
      }
      if (purchase.tomorrow && purchase.tomorrow.length > 0) {
        let newDateStr = '明天';
        let date = new Date();
        date.setTime(date.getTime() + 1000 * 60 * 60 * 24);
        newDateStr += '(周' + weekDays[date.getDay()] + ')';
        let array = purchase.tomorrow;
        for (const i in array) {
          let item = array[i];
          item.id = item.dateTime + ' ' + item.startTime + ' ' + item.endTime;
          item.startTimeVal = item.dateTime + ' ' + item.startTime;
          item.endTimeVal = item.dateTime + ' ' + item.endTime;
        }
        fetchDateList.push({
          name: newDateStr,
          children: array
        });
      }
      let fetchDateSelect = this.data.fetchDateSelect ? this.data.fetchDateSelect : '';
      let fetchTimeList = this.data.fetchTimeList ? this.data.fetchTimeList : [];
      if (!fetchDateSelect && fetchDateList.length > 0) {
        fetchDateSelect = fetchDateList[0].name;
        fetchTimeList = fetchDateList[0].children;
      }

      let riderFetchDateList = [];
      if (purchase.todayTime && purchase.todayTime.length > 0) {
        let newDateStr = '今天';
        let date = new Date();
        newDateStr += '(周' + weekDays[date.getDay()] + ')';
        let array = purchase.todayTime;
        for (const i in array) {
          let item = array[i];
          item.id = item.dateTime + ' ' + item.startTime + ' ' + item.endTime;
          item.startTimeVal = item.dateTime + ' ' + item.startTime;
          item.endTimeVal = item.dateTime + ' ' + item.endTime;
        }
        riderFetchDateList.push({
          name: newDateStr,
          children: array
        });
      }
      if (purchase.tomorrowTime && purchase.tomorrowTime.length > 0) {
        let newDateStr = '明天';
        let date = new Date();
        date.setTime(date.getTime() + 1000 * 60 * 60 * 24);
        newDateStr += '(周' + weekDays[date.getDay()] + ')';
        let array = purchase.tomorrowTime;
        for (const i in array) {
          let item = array[i];
          item.id = item.dateTime + ' ' + item.startTime + ' ' + item.endTime;
          item.startTimeVal = item.dateTime + ' ' + item.startTime;
          item.endTimeVal = item.dateTime + ' ' + item.endTime;
        }
        riderFetchDateList.push({
          name: newDateStr,
          children: array
        });
      }
      let riderFetchDateSelect = this.data.riderFetchDateSelect ? this.data.riderFetchDateSelect : '';
      let riderFetchTimeList = this.data.riderFetchTimeList ? this.data.riderFetchTimeList : [];
      if (!riderFetchDateSelect && riderFetchDateList.length > 0) {
        riderFetchDateSelect = riderFetchDateList[0].name;
        riderFetchTimeList = riderFetchDateList[0].children;
      }
      this.setData({
        show: true,
        purchaseData: purchase,
        redbag: purchase.redBagMap.defaultRedBag,
        isUseCard: purchase.giftCardAmount > 0 && purchase.totalAmount > 0 ? true : false,
        isShowAddrAlert: this.data.fetchType == 1 ? false : ((purchase.address && purchase.address.id) ? false : true),
        fetchDateList: fetchDateList,
        fetchDateSelect: fetchDateSelect,
        fetchTimeList: fetchTimeList,
        riderFetchDateList: riderFetchDateList,
        riderFetchDateSelect: riderFetchDateSelect,
        riderFetchTimeList: riderFetchTimeList
      }, () => {
        this.onSelectCardHide();
      })
    } else {
      this.goback(res.message);
    }
    wx.hideLoading();
  },

  async purchaseOrder() {
    let res = null;
    if (this.data.isPintuan) {
      res = await this.purchaseOrderPintuan();
    } else if (this.data.isLive) {
      res = await this.purchaseOrderLive();
    } else {
      res = await this.purchaseOrderNormal();
    }
    return res;
  },

  async purchaseOrderNormal() {
    const addressId = this.data.addressId ? this.data.addressId : '';
    const res = await request.post('ylf/trade/purchase', {
      addressId,
      cartLineIds: this.data.cartLineIds,
      shopId: app.globalData.sellerShop.accountId,
      tokenId: 0,
      fetch: this.data.fetchType,
      mobile: this.data.fetchMobile
    });
    return res;
  },

  async purchaseOrderPintuan() {
    const addressId = this.data.addressId ? this.data.addressId : '';
    let params = {
      addressId,
      itemId: this.data.pintuan.itemId,
      shopId: app.globalData.sellerShop.accountId,
      tokenId: 0,
      fetch: this.data.fetchType,
      combinationId: this.data.pintuan.combinationId,
      groupId: this.data.pintuan.groupId
    }
    if (this.data.pintuan.groupId) {
      params.groupId = this.data.pintuan.groupId;
    }
    const res = await request.post('ylf/trade/purchaseCombination', params);
    return res;
  },

  async purchaseOrderLive() {
    const addressId = this.data.addressId ? this.data.addressId : '';
    let params = {
      fetch: this.data.fetchType,
      shopId: app.globalData.sellerShop.accountId,
      itemId: this.data.liveData.itemId,
      activityId: this.data.liveData.activityId,
      addressId
    }
    const res = await request.post('ylf/trade/purchaseActivity', params);
    return res;
  },

  setAddressCancel() {
    this.setData({
      isShowAddrAlert: false
    })
  },

  setAddress(e) {
    if (this.data.purchaseData.address && this.data.purchaseData.address && e.currentTarget.dataset.id) {
      //编辑
      util.navigation_to("/pages/user/address/list/list?addressId=" + e.currentTarget.dataset.id, 1)
    } else {
      // 新添加
      util.navigation_to("/pages/user/address/edit/index?addressType=no", 1);
    }
  },

  setRedbag(e) {
    if (!this.data.paying) {
      if (this.data.purchaseData.redBagMap.usable && this.data.purchaseData.redBagMap.usable.length > 0) {
        const redbagId = this.data.redbag ? this.data.redbag.id : null;
        util.navigation_to("/pages/order/redbag/index?redbagId=" + redbagId, 1);
      } else {
        wx.showToast({
          title: '没有红包可用',
          icon: 'none',
          duration: 2000
        })
      }
    }
  },

  setCard(e) {
    if (!this.data.paying && this.data.purchaseData.giftCardAmount > 0 && this.data.purchaseData.totalAmount > 0) {
      this.setData({
        isUseCard: !this.data.isUseCard
      }, () => {
        this.onSelectCardHide();
      })
    }
  },

  onSelectCardHide() {
    let cardUse = 0;
    if (this.data.isUseCard) {
      let cardTotal = this.data.purchaseData.giftCardAmount;
      let orderTotal = this.data.purchaseData.totalAmount;
      if (this.data.redbag && this.data.redbag.denomination) {
        orderTotal = orderTotal - this.data.redbag.denomination;
      }
      if (cardTotal >= orderTotal) {
        cardUse = orderTotal;
      } else {
        cardUse = cardTotal;
      }
    }
    this.setData({
      cardUse: cardUse
    });
  },

  setOutStock(e) {
    const list = ['其它商品继续配送(缺货商品退款)', '电话与我沟通'];
    const _this = this;
    wx.showActionSheet({
      alertText: '如遇缺货',
      itemList: list,
      success(res) {
        _this.setData({
          outStock: list[res.tapIndex]
        })
      }
    });
  },

  setInvoice(e) {
    // if (!this.data.paying) {
    //   util.navigation_to('/pages/order/invoice/invoice', 1);
    // }
    if (!this.data.paying) {
      if (this.data.purchaseData.hasInvoiceHeader || (this.data.invoice && this.data.invoice.id)) {
        let id = this.data.invoice ? this.data.invoice.id : 0;
        wx.navigateTo({
          url: '/pages/user/invoice/apply/selectTitle?id=' + id
        })
      } else {
        wx.navigateTo({
          url: '/pages/user/invoice/apply/invoicing'
        })
      }
    }

  },

  setRemark(e) { //输入留言
    this.setData({
      remark: e.detail.value
    })
  },

  async payment(e) {
    const _this = this;
    const data = _this.data;
    this.setData({
      isCommitOrder: false
    });
    //地址
    if (!data.purchaseData.address) {
      wx.showToast({
        title: '请选择地址',
        icon: 'none',
        duration: 2000
      });
      return;
    }
    //自提
    if (data.fetchType == 1) {
      //自提时间
      if (!data.fetchTimeSelect || !data.fetchTimeSelect.startTime) {
        _this.setFetchTime(true);
        return;
      }
      //自提手机号
      if (!data.fetchMobile || data.fetchMobile.length != 11) {
        wx.showToast({
          title: '请填写预留手机',
          icon: 'none',
          duration: 2000
        });
        return;
      }
    } else if (data.purchaseData.deliveryScope != 2) {
      //骑手配送
      if (!data.riderFetchTimeSelect || !data.riderFetchTimeSelect.startTime) {
        _this.setRiderFetchTime(true);
        return;
      }
    }
    if (!data.purchaseData.purchaseItemList || data.purchaseData.purchaseItemList.length < 1) {
      wx.showToast({
        title: '无可购买的商品',
        icon: 'none',
        duration: 2000
      });
      return;
    }
    if (!data.paying) {
      wx.getNetworkType({
        async success(networkType) {
          if (networkType.networkType == "none" || networkType.networkType == "unknown") {
            _this.setData({
              paying: false
            });
            wx.showToast({
              title: '网络异常,请稍后重试',
              icon: 'none',
              duration: 2000
            });
          } else {
            _this.setData({
              paying: true
            })
            wx.showLoading();
            setTimeout(() => { //30秒超时提醒 下单失败
              if (data.paying) {
                wx.showToast({
                  title: '下单失败',
                  icon: 'none',
                  duration: 2000
                });
                wx.hideLoading({
                  success() {
                    _this.setData({
                      paying: false
                    })
                  }
                });
              }
            }, 30000);
            //下单
            const res = await _this.commitOrder();
            if (res && res.payment) {
              if (res.payment.payAmount > 0) {
                //付款
                const payres = await _this.payOrder(res.payment);
              } else {
                // check order payment
                pauyUtil.checkPay(res.payment.paymentSn, (payres) => {
                  wx.hideLoading();
                  _this.setData({
                    paying: false
                  });
                  if (!payres.succeed) {
                    wx.showToast({
                      icon: 'none',
                      title: payres.message,
                    })
                  }
                  wx.redirectTo({
                    url: '/pages/user/order/detail/detail?orderId=' + res.payment.orderNo,
                  });
                });
              }
            }
          }
        }
      });
    }
  },

  async commitOrder() {
    let data = {
      shopId: app.globalData.sellerShop.accountId,
      tokenId: this.data.purchaseData.tokenId,
      addressId: (this.data.purchaseData.address && this.data.purchaseData.address.id) ? this.data.purchaseData.address.id : 0,
      redbagId: this.data.redbag ? this.data.redbag.id : null,
      cardUse: this.data.isUseCard,
      orderFrom: 3,
      invoiceHeaderId: this.data.invoice ? this.data.invoice.id : null,
      comment: this.data.remark,
      stockOutDisposeType: this.data.outStockValue[this.data.outStock],
      fetch: this.data.fetchType,
      mobile: this.data.fetchMobile
    }
    if (this.data.fetchType == 1) {
      data.startTime = this.data.fetchTimeSelect.id ? this.data.fetchTimeSelect.startTimeVal : '';
      data.endTime = this.data.fetchTimeSelect.id ? this.data.fetchTimeSelect.endTimeVal : '';
    } else if (this.data.purchaseData.deliveryScope != 2) {
      data.startTime = this.data.riderFetchTimeSelect.id ? this.data.riderFetchTimeSelect.startTimeVal : '';
      data.endTime = this.data.riderFetchTimeSelect.id ? this.data.riderFetchTimeSelect.endTimeVal : '';
    }
    if (this.data.isPintuan) {
      data.combinationId = this.data.pintuan.combinationId;
      data.groupId = this.data.pintuan.groupId;
    }
    if (this.data.isLive) {
      data.activityId = this.data.liveData.activityId;
      data.activityType = 1;
    }
    const res = await request.post('ylf/trade/order', data);
    if (!res.succeed) {
      this.setData({
        paying: false
      }, () => {
        this.showErrorAlert(res.message);
      });
    } else {
      if (!this.data.fetchType) {
        //保存当前地址到缓存
        wx.setStorageSync('isReloadAddr', 'true');
        wx.setStorageSync('currAddress', JSON.stringify(this.data.purchaseData.address));
      }
    }
    return res.data;
  },

  showErrorAlert(text) {
    wx.hideLoading();
    this.setData({
      isShowAlert: true,
      alertText: text
    })
  },

  onErrorAlertTap() {
    wx.navigateBack();
  },

  async payOrder(payment) {
    const _this = this;
    wx.hideLoading();
    pauyUtil.payment(payment.orderNo, payment.paymentSn, res => {
      _this.setData({
        paying: false
      });
      if (!res) {
        wx.showToast({
          title: '支付未完成，请重新支付',
          icon: 'none',
          duration: 2000
        });
        wx.redirectTo({
          url: '/pages/user/order/detail/detail?orderId=' + payment.orderNo,
        });
      } else {
        wx.redirectTo({
          url: '/pages/user/order/payment/payment_result?orderNo=' + payment.orderNo + '&isPintuan=' + (this.data.isPintuan ? 1 : 0),
        });
      }
    });
  },

  goback(msg) {
    wx.showModal({
      title: '订单错误',
      content: msg ? msg : "订单没有商品",
      showCancel: false,
      success: function (res) {
        if (res.confirm) {
          wx.navigateBack();
        }
      }
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.initNameAndHead();
    if (options.selfMention && options.selfMention == 1) {
      this.setData({
        fetchType: 1
      })
    } else {
      this.setData({
        fetchType: 0
      })
    }
    if (options.cartLineIds && options.cartLineIds.length > 0) {
      let ids = options.cartLineIds.split(',');
      this.setData({
        cartLineIds: ids,
        isPintuan: false,
        isLive: false
      }, () => {
        app.login(() => {
          this.initData();
        });
      });
    } else if (options.isPintuan) {
      this.setData({
        isPintuan: true,
        isLive: false,
        pintuan: {
          itemId: options.itemId,
          combinationId: options.combinationId,
          groupId: options.groupId
        }
      }, () => {
        app.login(() => {
          this.initData();
        });
      })
    } else if (options.isLive) {
      this.setData({
        isPintuan: false,
        isLive: true,
        liveData: {
          itemId: options.itemId,
          activityId: options.activityId
        }
      }, () => {
        app.login(() => {
          this.initData();
        });
      })
    } else {
      this.goback();
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    const data = this.data;
    if (data.isReload) {
      this.setData({
        isReload: false
      }, () => {
        app.login(() => {
          this.initData();
        });
      });
    }
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
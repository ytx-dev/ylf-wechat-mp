// pages/order/invoice/invoice.js
const app = getApp();

Page({

  /**
   * Page initial data
   */
  data: {
    need: 1,
    invoice: {
      invoiceType: 1, //1 个人 2 公司
      invoiceTitle: '', //发票抬头
      taxCode: '', //税务号
      companyAddress: '', //公司地址
      telephone: '', //电话
    },
    isClick: false
  },

  verify() {
    let isClick = true;
    if (this.data.need === 0 && this.data.invoice.invoiceType === 2) {
      if (!this.data.invoice.invoiceTitle) {
        isClick = false;
      } else if (!this.data.invoice.taxCode || !(this.data.invoice.taxCode.length === 15 || this.data.invoice.taxCode.length === 18 || this.data.invoice.taxCode.length === 20)) {
        isClick = false;
      }
    }
    this.setData({
      isClick: isClick
    });
  },

  onInvoiceTypeTap(e) {
    let data = this.data.invoice;
    data.invoiceType = Number(e.currentTarget.dataset.value);
    data.invoiceTitle = data.invoiceType === 1 ? '个人' : '';
    this.setData({
      invoice: data
    }, () => {
      this.verify(false);
    });
  },

  setInvoiceInput(e) {
    let data = this.data.invoice;
    if (e.currentTarget.dataset.type === 'invoiceTitle') {
      data[e.currentTarget.dataset.type] = e.detail.value;
    } else {
      let value = e.detail.value;
      if (value) {
        value = value.toUpperCase();
      }
      data[e.currentTarget.dataset.type] = value;
    }
    this.setData({
      invoice: data
    }, () => {
      this.verify();
    });
  },

  onInvoiceTitleBlur() {
    if (!this.data.isClick) {
      if (this.data.need === 0 && this.data.invoice.invoiceType === 2) {
        if (!this.data.invoice.invoiceTitle) {
          wx.showToast({
            title: '请填写发票抬头',
            icon: 'none'
          })
        }
      }
    }
  },

  onTaxCodeBlur() {
    if (!this.data.isClick) {
      if (this.data.need === 0 && this.data.invoice.invoiceType === 2) {
        if (!this.data.invoice.taxCode || !(this.data.invoice.taxCode.length === 15 || this.data.invoice.taxCode.length === 18 || this.data.invoice.taxCode.length === 20)) {
          wx.showToast({
            title: '请填写正确的纳税人识别码，15位、18或者20位',
            icon: 'none'
          })
        }
      }
    }
  },

  needSel(e) {
    let invoice = this.data.invoice;
    const need = Number(e.currentTarget.dataset.value);
    if (need === 1) {
      invoice = null;
    } else if (!invoice) {
      invoice = {
        invoiceType: 1,
        invoiceTitle: '个人'
      }
    }
    this.setData({
      need: need,
      invoice: invoice
    }, () => {
      this.verify();
    });
  },

  onBtnOkTap() {
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    let invoice = this.data.need === 0 ? this.data.invoice : null;
    prevPages.setData({
      invoice: invoice,
      isReload: false
    }, () => {
      wx.navigateBack();
    });
  },

  initData() {
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    const invoice = prevPages.data.invoice;
    this.setData({
      need: invoice != null ? 0 : 1,
      invoice: invoice
    }, () => {
      this.verify();
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.initData();
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
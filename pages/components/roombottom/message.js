// pages/components/roombottom/message.js
Component({
  /**
   * Component properties
   */
  properties: {
    isTimReady: {
      type: Boolean,
      value: false
    }
  },

  /**
   * Component initial data
   */
  data: {
    text: '',
    displayInputShow: 'none',
    displayInputBottom: '0px'
  },

  /**
   * Component methods
   */
  methods: {
    bindblur() {
      this.setData({
        displayInputShow: 'none'
      })
    },
    bindkeyboardheightchange(e) {
      this.setData({
        displayInputShow: 'none'
      })
      const {
        height,
        duration
      } = e.detail
      this.setData({
        displayInputBottom: height + 'px'
      })
      if (height > 0) {
        setTimeout(() => {
          this.setData({
            displayInputShow: 'block'
          })
        }, duration * 1000)
      }
    },
    textInput(e) {
      let value = e.detail.value;
      this.setData({
        text: value
      })
    },
    confirm() {
      this.triggerEvent('send-message', this.data.text);
      this.setData({
        text: ''
      })
    },
  }
})
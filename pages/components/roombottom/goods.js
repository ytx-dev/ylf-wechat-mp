// pages/components/roombottom/goods.js
Component({
  /**
   * Component properties
   */
  properties: {

  },

  /**
   * Component initial data
   */
  data: {

  },

  /**
   * Component methods
   */
  methods: {
    onShowGoods() {
      this.triggerEvent('showgoods')
    }
  }
})
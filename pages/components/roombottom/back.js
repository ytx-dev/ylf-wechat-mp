// pages/components/roombottom/back.js
Component({
  /**
   * Component properties
   */
  properties: {

  },

  /**
   * Component initial data
   */
  data: {

  },

  /**
   * Component methods
   */
  methods: {
    onQuitGroup(){
      this.triggerEvent('quitGroup')
    }
  }
})

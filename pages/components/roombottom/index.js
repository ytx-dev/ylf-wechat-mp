// pages/components/roombottom/index.js
Component({
  /**
   * Component properties
   */
  properties: {
    isTimReady: {
      type: Boolean,
      value: false
    }
  },

  /**
   * Component initial data
   */
  data: {

  },

  /**
   * Component methods
   */
  methods: {
    onShowGoods() {
      this.triggerEvent('showgoods')
    },
    onSendMessage(e) {
      this.triggerEvent('send-message', e.detail);
    },
    onQuitGroup() {
      this.triggerEvent('quitGroup')
    }
  }
})
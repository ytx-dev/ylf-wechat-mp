// pages/components/noowner/index.js
Component({
  /**
   * Component properties
   */
  properties: {
    ownerInfo: {
      type: Object,
      value: {}
    }
  },

  /**
   * Component initial data
   */
  data: {
    
  },

  /**
   * Component methods
   */
  methods: {

  }
})

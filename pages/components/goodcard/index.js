// pages/components/goodcard/index.js
const app = getApp();

Component({
  /**
   * Component properties
   */
  properties: {
    good: {
      type: Object,
      value: {},
      observer: function (newVal, oldVal) {
        this.onGoodChange(newVal, oldVal);
      }
    }
  },

  /**
   * Component initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    isShow: false,
    countdown: 0
  },

  /**
   * Component methods
   */
  methods: {
    onGoodChange(newVal, oldVal) {
      if (newVal && newVal.activityItem) {
        let countdown = 15;
        if (newVal.activityItem && newVal.activityItem.topStatus == 0) {
          countdown = 60
        }
        this.setData({
          isShow: true,
          countdown: countdown
        }, () => {
          setTimeout(this.updateCountdown, 1000, this);
        });
      } else {
        this.setData({
          isShow: false
        });
      }

    },
    updateCountdown(_this) {
      let countdown = _this.data.countdown - 1;
      if (countdown <= 0) {
        _this.setData({
          isShow: false,
          countdown: 0
        })
      } else {
        _this.setData({
          countdown: countdown
        }, () => {
          setTimeout(_this.updateCountdown, 1000, _this);
        });
      }
    },
    gotoItem(e) {
      let item = this.data.good;
      if (item) {
        this.triggerEvent('itemTap', {
          itemId: this.data.good.activityItem.itemId
        });
        wx.navigateTo({
          url: '/pages/item/detail/detail?type=1&itemId=' + item.activityItem.itemId + '&activityId=' + item.activityItem.activityId
        })
      }
    }
  }
})
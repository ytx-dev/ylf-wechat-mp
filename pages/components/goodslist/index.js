// pages/components/goodslist/index.js
const app = getApp();

Component({
  /**
   * Component properties
   */
  properties: {
    goods: {
      type: Array,
      value: [],
      observer: function (newVal, oldVal) {
        this.onGoodsChange(newVal, oldVal);
      }
    }
  },

  /**
   * Component initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    good_list: [],
    topitem: {}
  },

  /**
   * Component methods
   */
  methods: {
    onGoodsChange(newVal, oldVal) {
      let array = [];
      let topitem = {};
      for (const i in newVal) {
        let item = newVal[i];
        if (item.activityItem && item.activityItem.topStatus == 0) {
          topitem = item;
        } else {
          array.push(item);
        }
      }
      this.setData({
        good_list: array,
        topitem: topitem
      })
    },
    onLower() {
      this.triggerEvent('lower')
    },
    onCardTap() {
      //do nothing
    },
    onBtnCloseTap() {
      this.triggerEvent('hidegoods')
    },
    gotoOrderList() {
      wx.navigateTo({
        url: '/pages/user/order/list/list',
      })
    },
    gotoItem(e) {
      let item = e.currentTarget.dataset['item'];
      if (item) {
        this.triggerEvent('itemTap', {
          itemId: item.activityItem.itemId
        });
        wx.navigateTo({
          url: '/pages/item/detail/detail?type=1&itemId=' + item.activityItem.itemId + '&activityId=' + item.activityItem.activityId
        })
      }
    },
    onItemBuyTap(e) {
      let itemId = e.currentTarget.dataset['itemId'];
      this.triggerEvent('itemBuy', {
        itemId: itemId,
        point: e.touches[0]
      })
    }
  }
})
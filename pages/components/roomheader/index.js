// pages/components/header/index.js
const app = getApp();

Component({
  /**
   * Component properties
   */
  properties: {
    liveName: {
      type: String,
      value: {}
    }
  },

  /**
   * Component initial data
   */
  data: {
    StatusBar: app.globalData.StatusBar < 21 ? 26 : app.globalData.StatusBar,
  },

  /**
   * Component methods
   */
  methods: {
    BackPage() {
      wx.navigateBack();
    }
  }
})

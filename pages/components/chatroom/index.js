// pages/components/chatroom/index.js
Component({
  /**
   * Component properties
   */
  properties: {
    message: {
      type: Array,
      value: [],
      observer: function (newVal, oldVal) {
        this.onMessageChange(newVal, oldVal);
      }
    }
  },

  /**
   * Component initial data
   */
  data: {
    viewID: ''
  },

  /**
   * Component methods
   */
  methods: {
    onMessageChange(newVal, oldVal) {
      if (newVal.length) {
        const last = newVal[newVal.length - 1]
        this.setData({
          viewID: last.id
        })
      }
    }
  }
})
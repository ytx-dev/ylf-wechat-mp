// pages/login/login.js
const app = getApp();
const util = require('../../utils/util.js');
const cart = require('../../utils/cart.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    buttonValue: '获取短信验证码',
    timer: null,
    mobile: '',
    code: '',
    captcha: '',
    sms_type: '',
    modelStatus: false,
    isShowToast: false,
    toastText: '',
    ajax: true,
    isShowCode: false,

    Length: 6, //输入框个数 
    isFocus: true, //聚焦 
    Value: "", //输入的内容 
    ispassword: false, //是否密文显示 true为密文， false为明文。 
  },

  cleanPhone() {
    this.setData({
      mobile: ''
    })
  },

  Focus(e) {
    var that = this;
    var inputValue = e.detail.value;
    that.setData({
      Value: inputValue,
    })
  },
  Tap() {
    var that = this;
    that.setData({
      isFocus: true,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // app.globalData.wentLiginPage = true;
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    // app.globalData.loginPage = true;
    // !!app.globalData.rememberMe && (app.globalData.wentLiginPage = false);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  },
  /**
   * 关闭红包
   */
  closeCred: function () {
    this.setData({
      modelStatus: false
    })
    wx.switchTab({
      url: '/pages/index/index'
    })
  },
  /**
   * 绑定用户
   */
  bindUser: function () {
    var that = this;
    if (that.data.mobile) {
      if (that.data.Value) {
        that.sqlValidateMobile(function () {
          that.sqlRegister();
        });
      } else {
        wx.showToast({
          title: '请输入验证码',
          icon: 'none'
        });
      }
    } else {
      wx.showToast({
        title: '请输入手机号',
        icon: 'none'
      });
    }
  },
  //数据绑定手机号
  setMobile: function (event) {
    this.setData({
      mobile: event.detail.value
    })
  },
  /**
   * 发送验证码
   */
  getCode: function (event) {
    var that = this;
    if (that.data.mobile) {
      that.sqlValidateMobile(function () {
        that.sqlGetCode();
      });
    } else {
      wx.showToast({
        title: '请输入手机号',
        icon: 'none'
      });
    }
  },
  /**
   * 校验验证码是否过期
   */
  sqlCheckCode: function (fn) {
    var vm = this;
    util.$http({
      url: app.globalData.apiDomain + 'ylf/user/wxapp/sms/check',
      data: {
        activation: this.data.Value,
        mobile: this.data.mobile,
        type: this.data.sms_type
      },
      callback: res => {
        if (res.data.success == true) {
          fn();
        } else {
          wx.showToast({
            title: res.data.msg || res.data.error,
            icon: 'none'
          });
          vm.setData({
            ajax: true
          });
        }
      }
    });
  },
  /**
   * 绑定&注册
   */
  sqlRegister: function () {
    var that = this;
    if (that.data.ajax) {
      that.setData({
        ajax: false
      });
      that.sqlCheckCode(function () {
        util.$http({
          url: app.globalData.apiDomain + 'ylf/user/wxapp/quick',
          data: {
            mobile: that.data.mobile,
            code: that.data.Value,
            unionId: app.globalData.unionId,
            shareToken: app.globalData.shareToken
          },
          header: {
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
          },
          type: 'POST',
          dataType: 'json',
          callback: res => {
            if (res.data.success == true) {
              if (res.data.msg) {
                wx.showToast({
                  title: res.data.msg ? res.data.msg : res.data.error,
                  icon: 'none'
                });
              }

              if (res.data.account) {
                app.globalData.account = res.data.account;
                wx.setStorage({
                  key: 'account',
                  data: JSON.stringify(app.globalData.account)
                });
              }

              app.globalData.reset = true;
              if (res.data['remember-me']) {
                app.globalData.rememberMe = res.data['remember-me'];
                wx.setStorage({
                  key: 'rememberMe',
                  data: app.globalData.rememberMe
                });
                wx.setStorage({
                  key: 'lastLogin',
                  data: new Date().getTime()
                });

                //绑定邀请
                // let spreadNumber = wx.getStorageSync('spreadNumber');
                // if (spreadNumber) {
                //   const invite = require('../../utils/invite.js');
                //   invite.bindFormLocal();
                // }
              }
              if (app.globalData.backFn) {
                app.globalData.backFn();
                app.globalData.backFn = null;
              }
              cart.initCartData(() => {
                wx.navigateBack();
              });
            } else {
              wx.showToast({
                title: res.data.msg ? res.data.msg : res.data.error,
                icon: 'none'
              });
            }
            that.setData({
              ajax: true
            });
          }
        });
      });
    }
  },
  /**
   * 校验客户手机是否注册
   */
  sqlValidateMobile: function (fn) {
    var that = this;
    util.$http({
      url: app.globalData.apiDomain + 'ylf/user/wxapp/mobile/validate',
      data: {
        mobile: that.data.mobile
      },
      dataType: 'json',
      callback: res => {
        that.setData({
          sms_type: res.data.success ? 101 : 1
        });
        fn();
      }
    });
  },
  /**
   * 获取短信验证码
   */
  sqlGetCode: function () {
    var that = this;
    if (!that.data.timer) {
      if (that.data.mobile) {
        util.$http({
          url: app.globalData.apiDomain + 'ylf/user/wxapp/sms',
          data: {
            mobile: that.data.mobile,
            type: that.data.sms_type
          },
          type: 'POST',
          header: {
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
          },
          callback: function (res) {
            if (res.data.success == true) {
              that.setData({
                isShowCode: true
              })
              runTime(that, 60);
            } else {
              wx.showToast({
                title: res.data.msg,
                icon: 'none'
              });
            }
          },
          fail: function (res) {
            wx.showToast({
              title: '短信发送失败',
              icon: 'none'
            });
          }
        });
      }
    }
  }
})

function runTime(that, time) {
  if (time <= 0) {
    that.setData({
      timer: null,
      buttonValue: "重新获取短信验证码"
    })
    return;
  } else {
    that.setData({
      buttonValue: '重新获取(' + time + 's)'
    })
    time--;
  }
  that.setData({
    timer: setTimeout(function () {
      runTime(that, time);
    }, 1000)
  })
}
// pages/user/order/detail/detail.js
const app = getApp();
const request = require('../../../../utils/request.js');
const util = require('../../../../utils/util.js');
const pauyUtil = require('../../../../utils/pay.js');
const QR = require("../../../../lib/qrcode/qrcode.js");
const UiUtils = require("../../../../lib/qrcode/UiUtils.js");
/*
订单状态
-1:无效订单 10:等待支付 20:订单待发货 30:订单待收货 50:订单取消 51:订单已完成 52:订单关闭
*/
Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    serialNumber: '',
    disputeCode: null,
    disputeStatus: null,
    order: {},
    showType: 'none',
    distributionOrderDetail: null,
    distributionStatusName: '',
    centerPoint: {
      longitude: '108.987666',
      latitude: '34.278594'
    },
    markers: [],
    reasonList: [],
    expiryTime: '',
    isReload: false,
    setTimeoutId: 0,
    disputeStatusNames: {
      '0': '售后关闭',
      '1': '售后中',
      '2': '售后完成',
      '3': '售后中',
      '4': '售后中'
    },
    HEADER_TYPE_TEXT: {
      0: '个人',
      1: '单位'
    },
    outStockValue: {
      1: '其它商品继续配送(缺货商品退款)',
      2: '电话与我沟通'
    },
    paymentTypeNames: {
      '-1': '伪支付',
      '0': '免单',
      '1': '支付宝',
      '2': '微信支付',
      '3': '微信支付',
      '4': '微信支付',
      '5': '抵扣支付'
    },
    orderCombinationVo: {},
    pintuanData: {},
    pintuanTime: 0,
    pintuanTimer: null,
    isShowMore: false
  },

  emptyHandler() {
    // do nothing
  },

  gotoDispute(e) {
    wx.navigateTo({
      url: '/pages/user/order/refund/refund?disputeCode=' + e.currentTarget.dataset.code,
    })
  },

  onBtnMoreClose() {
    this.setData({
      isShowMore: false
    })
  },

  onBtnMoreTap() {
    this.setData({
      isShowMore: true
    })
  },

  onViewDeliveryTap() {
    if (this.data.order.resultItem) {
      wx.navigateTo({
        url: '/pages/user/order/delivery/index?orderNo=' + this.data.serialNumber,
      })
    } else {
      wx.showToast({
        title: '暂无物流信息,请稍后查看',
        icon: 'none'
      })
    }
  },

  async onBtnPayTap() {
    const res = await request.get('ylf/trade/pay/' + this.data.serialNumber);
    if (!res.succeed) {
      wx.showToast({
        icon: 'none',
        title: res.message,
      })
    } else {
      if (res.data.orderPaymentHistory.payAmount > 0) {
        pauyUtil.payment(this.data.serialNumber, res.data.orderPaymentHistory.paymentSn, res => {
          if (res) {
            this.initData();
          } else {
            wx.showToast({
              title: '支付未完成，请重新支付',
              icon: 'none',
              duration: 2000
            });
          }
        });
      } else {
        // check order payment
        pauyUtil.checkPay(res.data.orderPaymentHistory.paymentSn, (res) => {
          if (res.succeed) {
            this.initData();
          } else {
            wx.showToast({
              icon: 'none',
              title: res.message,
            })
          }
        });
      }
    }
  },

  onCallRiderTap() {
    if (this.data.distributionOrderDetail && this.data.distributionOrderDetail.deliverymanTel) {
      wx.makePhoneCall({
        phoneNumber: this.data.distributionOrderDetail.deliverymanTel
      });
    }
  },

  onBtnReceiptTap() {
    this.orderReceipt();
  },

  async orderReceipt() {
    const res = await request.put('ylf/trade/order/' + this.data.serialNumber);
    if (res.succeed) {
      wx.showToast({
        title: '确认收货成功',
        icon: 'success',
        duration: 2000
      });
      this.initData();
    } else {
      wx.showToast({
        title: '确认收货失败',
        icon: 'none',
        duration: 2000
      });
    }
  },

  async onBtnBuyTap() {
    const res = await request.post('ylf/trade/cart/again?orderNo=' + this.data.serialNumber);
    if (res.succeed) {
      wx.switchTab({
        url: '/pages/cart/cart',
      });
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none',
        duration: 2000
      });
    }
  },

  callShop() {
    this.onBtnMoreClose();
    let num = '';
    if (this.data.order.phoneNumber) {
      if (this.data.order.phoneAreaNumber) {
        num = this.data.order.phoneAreaNumber;
      } else {
        num = '029';
      }
      num += '-' + this.data.order.phoneNumber;
    } else {
      num = this.data.order.mobile;
    }
    if (num) {
      wx.makePhoneCall({
        phoneNumber: num
      });
    }
  },

  onGoodItemTap(e) {
    wx.navigateTo({
      url: '/pages/item/detail/detail?itemId=' + e.currentTarget.dataset.value.itemId,
    });
  },

  showInvoiceInfo() {
    this.onBtnMoreClose();
    if (this.data.order.status == 51) {
      wx.navigateTo({
        url: '/pages/user/invoice/detail?orderNo=' + this.data.serialNumber,
      })
    } else {
      wx.showToast({
        title: '订单完成后开具发票',
        icon: 'none'
      })
    }
  },

  async onBtnCancelTap(e) {
    if (this.data.order.status === 10) {
      this.showReasons();
    } else {
      /**
       * skipType == cancel: 进入订单取消申请页
       * skipType == dispute: 进入订单售后申请页
       * skipType == detail: 进入售后详情页，code即详情页地址
       * skipType == notSale: 没有售后 购物卡
       */
      const skipData = await this.getDisputesSkip();
      if (skipData.skipType === 'cancel') {
        let data = {
          orderNo: this.data.order.serialNumber,
          myState: 1,
          disputeJournalItemList: this.data.order.orderItemList
        };
        util.navigation_to("/pages/user/order/cansel/index?order=" + JSON.stringify(data), 1);
      } else if (skipData.skipType === 'dispute') {
        util.navigation_to("/pages/user/order/dispute/dispute?serialNumber=" + this.data.serialNumber, 1);
      } else if (skipData.skipType === 'detail') {
        util.navigation_to("/pages/user/order/refund/refund?disputeCode=" + skipData.code, 1);
      } else if (skipData.skipType === 'notSale') {
        wx.showToast({
          title: '购物卡订单无售后',
          icon: 'none',
          duration: 2000
        })
      }
    }
  },

  async getDisputesSkip() {
    const res = await request.get('ylf/buyer/buyer/disputes/operation/skip', {
      orderNo: this.data.order.serialNumber
    });
    return res.data;
  },

  async showReasons() {
    const _this = this;
    const reasonList = await _this.getReasons();
    wx.showActionSheet({
      alertText: '请选择取消原因',
      itemList: reasonList,
      success(res) {
        _this.cancelOrder(_this.data.order.serialNumber, _this.data.reasonList[res.tapIndex]);
      }
    });
  },

  async getReasons() {
    const res = await request.get('ylf/buyer/buyer/disputes/reasons');
    this.setData({
      reasonList: res.data.reasonList
    });
    const reasonList = [];
    for (const i in res.data.reasonList) {
      reasonList.push(res.data.reasonList[i].reason);
    }
    return reasonList;
  },

  async cancelOrder(serialNumber, reason) {
    const res = await request.put('ylf/trade/order?order_no=' + serialNumber + '&cancel_reason=' + reason.reason);
    if (res.succeed) {
      this.initData();
    }
  },

  async getOrder(serialNumber) {
    const res = await request.get('ylf/trade/order/' + serialNumber);
    let order = res.data.order;
    order.createdTime = util.formatTime(new Date(order.createdAt));
    if (order.confirmAt) {
      order.confirmTime = util.formatTime(new Date(order.confirmAt));
    }
    let distributionOrderDetail = res.data.distributionOrderDetail;
    let distributionStatusName = '';
    if (distributionOrderDetail) {
      switch (distributionOrderDetail.distributionStatus) {
        case 0:
          distributionStatusName = '未配送';
          break;
        case 1:
          distributionStatusName = '待接单';
          break;
        case 2:
          distributionStatusName = '骑手 ' + distributionOrderDetail.deliverymanName + ' 正在赶往超市';
          break;
        case 3:
          distributionStatusName = '骑手 ' + distributionOrderDetail.deliverymanName + ' 正在配送';
          break;
        case 4:
          distributionStatusName = '骑手 ' + distributionOrderDetail.deliverymanName + ' 已完成配送';
          break;
        case 5:
          distributionStatusName = '已取消';
          break;
        case 6:
          distributionStatusName = '已过期';
          break;
        case 7:
          distributionStatusName = '已过期';
          break;
        case 8:
          distributionStatusName = '指派单';
          break;
        case 9:
          distributionStatusName = '未妥投，请保持手机通畅，门店将与您联系';
          break;
        default:
          distributionStatusName = '待处理';
          break;
      }
    }

    if ((order.status === 51 || order.status === 52 || order.status == 20) && res.data.spliceResult) {
      const array1 = res.data.spliceResult.split(',');
      let spliceResult = {};
      for (const i in array1) {
        const array2 = array1[i].split(':');
        spliceResult[array2[0]] = array2;
      }
      for (const i in order.orderItemList) {
        let item = order.orderItemList[i];
        const splice = spliceResult[item.itemSkuId];
        if (splice) {
          item.disputeCode = splice[1];
          item.disputeStatus = splice[2];
          item.disputeStatusName = this.data.disputeStatusNames[item.disputeStatus + ''];
        } else {
          item.disputeCode = 0;
        }
      }
    }

    let orderCombinationVo = res.data.orderCombinationVo;
    let pintuanTime = null;
    if (orderCombinationVo) {
      await this.sqlGetOrderCount();
      pintuanTime = res.data.orderCombinationVo.stopTime - res.data.orderCombinationVo.nowTime;
      if (orderCombinationVo.detailList) {
        for (const i in orderCombinationVo.detailList) {
          let item = orderCombinationVo.detailList[i];
          if (item.headerImg) {
            item.headerImg = this.data.imageDomain + item.headerImg;
          } else if (item.nikeName == this.data.nickName) {
            item.headerImg = this.data.headImg
          }
        }
      }
    }
    if (this.data.pintuanTimer) {
      clearTimeout(this.data.pintuanTimer);
    }
    this.setData({
      disputeCode: res.data.disputeCode,
      disputeStatus: res.data.disputeStatus,
      order: order,
      showType: res.data.showType,
      distributionOrderDetail: res.data.distributionOrderDetail,
      distributionStatusName: distributionStatusName,
      orderCombinationVo: orderCombinationVo,
      pintuanData: {},
      pintuanTime: pintuanTime,
      pintuanTimer: null
    }, () => {
      this.setExpiryTime();
      this.setMapData();
      if (order.fetch == 1 && (order.status == 20 || order.status == 30)) {
        this.renderPayCode();
      }
      if (pintuanTime) {
        this.setCountdown();
      }
    });
  },

  setCountdown() {
    let time = this.data.pintuanTime - 1000;
    if (time > 0) {
      let timer = setTimeout(() => {
        this.setCountdown();
      }, 1000);
      let pintuanData = this.data.pintuanData;
      let timeStr = util.convertTimeDian3(time);
      timeStr = timeStr.split(':');
      pintuanData.hh = timeStr[0];
      pintuanData.mm = timeStr[1];
      pintuanData.ss = timeStr[2];
      this.setData({
        pintuanData: pintuanData,
        pintuanTime: time,
        pintuanTimer: timer
      });
    } else {
      this.setData({
        pintuanData: {
          hh: '00',
          mm: '00',
          ss: '00'
        },
        pintuanTime: null,
        pintuanTimer: null
      });
    }
  },

  initNameAndHead(name, headImg) {
    let fullHeadImg = headImg ? headImg : (app.globalData.account && app.globalData.account.headImg ? app.globalData.account.headImg : (app.globalData.userInfo && app.globalData.userInfo.avatarUrl ? app.globalData.userInfo.avatarUrl : ''));
    if (!(/http|https/.test(fullHeadImg))) {
      fullHeadImg = app.globalData.imageDomain + fullHeadImg;
    }
    this.setData({
      headImg: fullHeadImg,
      nickName: name ? name : (app.globalData.account && app.globalData.account.nickName ? app.globalData.account.nickName : '')
    });
  },

  async sqlGetOrderCount() {
    const res = await request.get('ylf/buyer/buyer/show');
    const data = res.data;
    if (data) {
      this.initNameAndHead(data.name, data.headImg);
    }
  },

  renderPayCode() {
    let w2 = UiUtils.rpx2px(240)
    let code = this.data.order.fetchOrderNo ? this.data.order.fetchOrderNo : this.data.order.serialNumber + 'ymsh'
    QR.qrApi.draw(code, "qrcodeCanvas", w2, w2);
  },

  setMapData() {
    if (this.data.order.status === 30 && this.data.distributionOrderDetail && (this.data.distributionOrderDetail.distributionStatus == 2 || this.data.distributionOrderDetail.distributionStatus == 3)) {
      let id = setTimeout(this.getLocationData, 10000);
      this.setData({
        centerPoint: {
          longitude: this.data.distributionOrderDetail.destinationLng,
          latitude: this.data.distributionOrderDetail.destinationLat
        },
        markers: [{
          longitude: this.data.distributionOrderDetail.destinationLng,
          latitude: this.data.distributionOrderDetail.destinationLat,
          iconPath: 'https://g3.ytx.com/ylf_mp/point.png',
          width: 37,
          height: 47
        }],
        setTimeoutId: id
      });
    }
  },

  async getLocationData() {
    const res = await request.get('ylf/trade/distribution/' + this.data.order.serialNumber);
    if (res && res.data && res.data.distributionOrderDetail) {
      this.setData({
        distributionOrderDetail: res.data.distributionOrderDetail
      }, () => {
        this.setMapData();
      });
    }
  },

  setExpiryTime() {
    if (this.data.order.status === 10 && this.data.order.expiryTime) {
      const time = (15 * 60 * 1000 + this.data.order.createdAt) - new Date().getTime();
      if (time > 0) {
        let expiryTime = util.convertTime(time);
        this.setData({
          expiryTime: expiryTime
        }, () => {
          setTimeout(this.setExpiryTime, 1000);
        });
      } else {
        this.setData({
          expiryTime: ''
        }, () => {
          setTimeout(this.initData, 2000);
        })
      }
    }
  },

  async initData() {
    this.getOrder(this.data.serialNumber);
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      serialNumber: options.orderId
    }, () => {
      this.initData();
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    if (this.data.isReload) {
      this.setData({
        isReload: false
      }, () => {
        this.initData();
      })
    }
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {
    if (this.data.setTimeoutId) {
      clearTimeout(this.data.setTimeoutId);
    }
  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    if (this.data.orderCombinationVo.status == 0 || (this.data.orderCombinationVo.status == 2 && this.data.order.status != 10)) {
      const promise = new Promise(resolve => {
        request.get('ylf/trade/imageCombiner/combination', {
          groupId: this.data.orderCombinationVo.groupId
        }).then((res) => {
          if (res.data && res.data.shareUrl) {
            let path = '/pages/pintuan/share/index?groupId=' + this.data.orderCombinationVo.groupId + '&shopId=' + app.globalData.sellerShop.accountId;
            if (app.globalData.spreadNumber) {
              path += '&spreadNumber=' + app.globalData.spreadNumber;
            }
            resolve({
              title: res.data.shareTitle,
              path: path,
              imageUrl: res.data.shareUrl
            });
          }
        });
      });
      let pd = '/pages/pintuan/share/index?groupId=' + this.data.orderCombinationVo.groupId + '&shopId=' + app.globalData.sellerShop.accountId;
      if (app.globalData.spreadNumber) {
        pd += '&spreadNumber=' + app.globalData.spreadNumber;
      }
      return {
        title: '拼团活动',
        path: pd,
        promise
      }
    } else {
      return app.getShareCommonData();
    }
  }
})
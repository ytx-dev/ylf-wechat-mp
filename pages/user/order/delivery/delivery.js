// pages/user/order/delivery/delivery.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

    /**
     * 页面的初始数据
     */
    data: {
        deliveryStatus: 0,
        imageDomain: app.globalData.imageDomain,
        deliveryList: null


    },
    checkStatus: function (e) {
        this.setData({
            deliveryStatus: e.currentTarget.dataset.type
        })
    },
    setStateText: function (state) {
        switch (state) {
            case 0:
                return "在途中";
                break;
            case 1:
                return "已揽收";
                break;
            case 2:
                return "疑难";
                break;
            case 3:
                return "已签收";
                break;
            case 4:
                return "退钱";
                break;
            case 5:
                return "派件";
                break;
            case 6:
                return "正在退货";
                break;
        }
    },

    async initData(options) {
        wx.showLoading();
        const res = await request.get('trade/order/delivery/' + options.serialNumber);
        let datas = res.data;
        datas.parcels.forEach(function (delivery, index) {
            delivery.stateText = vm.setStateText(Number(delivery.result.state));
        });
        vm.setData({
            deliveryList: datas
        });
        wx.hideLoading();
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.initData(options);
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        return app.getShareCommonData();
    }
})
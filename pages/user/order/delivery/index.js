// pages/user/order/delivery/index.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    orderNo: '',
    deliveryInfo: {},
    lastResult: {}
  },

  onCopyTap() {
    wx.setClipboardData({
      data: this.data.deliveryInfo.expressNo
    })
  },

  async init() {
    let res = await request.get('ylf/trade/delivery_order/' + this.data.orderNo + '/deliver');
    this.setData({
      deliveryInfo: res.data.deliveryInfo,
      lastResult: res.data.lastResult,
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    let orderNo = options.orderNo;
    if (orderNo) {
      this.setData({
        orderNo: orderNo,
        deliveryInfo: {},
        lastResult: {}
      }, () => {
        this.init();
      });
    } else {
      wx.navigateBack();
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
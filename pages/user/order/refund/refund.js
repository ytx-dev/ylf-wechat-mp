// pages/user/order/refund/refund.js
const app = getApp();
const request = require('../../../../utils/request.js');
const util = require('../../../../utils/util.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    disputeCode: null,
    disputeJournalSummary: {
      disputeStatus: 0
    },
    storeAddress: '',
    receivingAddress: {},
    receivingTime: '',
    pickupDate: '',
    pickupAddress: {},
    lastDelivery: {},
    updatedAt: null,
    isReload: false
  },

  onViewDeliveryTap() {
    wx.navigateTo({
      url: '/pages/user/order/refund/delivery/delivery/index?code=' + this.data.disputeCode,
    })
  },

  btnCancelReserveTap() {
    const _this = this;
    wx.showModal({
      title: '确定要取消寄件吗？',
      content: '取消寄件后不可再预约上门取件',
      success(res) {
        if (res.confirm) {
          _this.cancelReserve();
        }
      }
    })

  },

  async cancelReserve() {
    const res = await request.get('ylf/buyer/buyer/delivery/cancel', {
      code: this.data.disputeCode
    });
    if (res.succeed) {
      wx.showToast({
        title: '取消成功',
        icon: 'success',
        duration: 2000
      });
      this.getData();
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none',
        duration: 2000
      })
    }
  },

  btnEidtReserveTap() {

  },

  btnSelfTap() {
    wx.navigateTo({
      url: '/pages/user/order/refund/delivery/self/index?data=' + JSON.stringify(this.data.disputeJournalSummary),
    });
  },

  btnReserveTap() {
    wx.navigateTo({
      url: '/pages/user/order/refund/delivery/reserve/index?data=' + JSON.stringify(this.data.disputeJournalSummary),
    });
  },

  onBtnOK2Tap() {
    const _this = this;
    wx.showActionSheet({
      alertText: '操作',
      itemList: ['添加留言', '修改申请', '撤销申请'],
      success(res) {
        if (res.tapIndex === 0) {
          _this.addRemark();
        } else if (res.tapIndex === 1) {
          _this.editAppt();
        } else {
          _this.revokeAppt();
        }
      }
    });
  },

  addRemark() {
    util.navigation_to("/pages/user/order/refund/addRemark?disputeCode=" + this.data.disputeCode + '&updatedAt=' + this.data.updatedAt, 1);
  },

  editAppt() {
    if (this.data.disputeJournalSummary.disputeType === 0) {
      util.navigation_to("/pages/user/order/cansel/index?disputeCode=" + this.data.disputeCode, 1);
    } else {
      util.navigation_to("/pages/user/order/dispute/dispute?disputeCode=" + this.data.disputeCode, 1);
    }
  },

  revokeAppt() {
    const _this = this;
    wx.showModal({
      title: '提示',
      content: '您撤销后，不可再次申请退款，确认撤销吗？',
      success(res) {
        if (res.confirm) {
          _this.doRevokeApply();
        }
      }
    });
  },
  async doRevokeApply() {
    const res = await request.post('ylf/buyer/buyer/disputes/' + this.data.disputeCode + '/rescission?updatedAt=' + this.data.updatedAt);
    if (res.succeed) {
      wx.showToast({
        title: '申请已撤销',
        icon: 'success',
        duration: 2000
      });
      this.getData();
    }
  },

  onGoodImageTap(e) {
    const url = this.data.imageDomain + e.currentTarget.dataset.url;
    let list = [];
    for (const i in this.data.disputeJournalSummary.disputeJournalItems) {
      list.push(this.data.imageDomain + this.data.disputeJournalSummary.disputeJournalItems[i].itemIconImageKey);
    }
    wx.previewImage({
      current: url,
      urls: list
    });
  },

  onImageTap(e) {
    const url = this.data.imageDomain + e.currentTarget.dataset.url;
    let list = [];
    for (const i in e.currentTarget.dataset.list) {
      list.push(this.data.imageDomain + e.currentTarget.dataset.list[i]);
    }
    wx.previewImage({
      current: url,
      urls: list
    });
  },

  initData() {
    this.getData();
  },

  async getData() {
    const res = await request.get('ylf/buyer/buyer/disputes/' + this.data.disputeCode);
    let data = res.data.disputeJournalSummary;
    let countdown = data.operateTitleArgsText;
    if (data.operateTitleArgsType == "countdown") {
      countdown = Number(countdown);
      if (countdown < 1) {
        countdown = 0;
      }
      countdown = util.convertTime(countdown);
    }
    let titles = data.operateTitle.replace('#' + data.operateTitleArgsType + '#', countdown).split('\n');
    data.title = titles[0];
    if (titles.length > 1) {
      data.subtitle = titles[1];
    }
    let list = data.dynamicChangeSummaryList;
    for (const i in list) {
      let item = list[i];
      item.time = util.formatTimemdhms(new Date(item.dynamicTime));
      if (!item.operatorLogo) {
        item.operatorLogo = 'https://g2.ytx.com/enter/1000/b7a68a36ff264b979fdbfc963e9e8fd8.jpg'
      }
      if (item.imageProofUrlList && item.imageProofUrlList.length === 1 && !item.imageProofUrlList[0]) {
        item.imageProofUrlList = [];
      }
    }
    this.setData({
      disputeJournalSummary: data,
      storeAddress: res.data.storeAddress,
      receivingAddress: res.data.receivingAddress,
      receivingTime: res.data.receivingTime,
      pickupDate: res.data.pickupDate,
      pickupAddress: res.data.pickupAddress,
      lastDelivery: res.data.lastDelivery,
      updatedAt: res.data.updatedAt
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      disputeCode: options.disputeCode
    }, () => {
      this.initData();
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    if (this.data.isReload) {
      this.setData({
        isReload: false
      }, () => {
        this.initData();
      });
    }
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
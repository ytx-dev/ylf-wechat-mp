// pages/user/order/refund/delivery/reserve/index.js
const app = getApp();
const request = require('../../../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    disputeJournalSummary: {},
    selectTime: {},
    selectTimeTmp: {},
    timeList: [],
    address: {},
    gasFee: 0,
    isClick: false,
    isShowSelectTime: false,
    selectTimeLeftIndex: 0
  },

  onSelectTimeTap() {
    let timeList = [];
    let date = new Date();
    let str = date.Format('yyyy-MM-dd');
    let today = [];
    if (date.getHours() < 12) {
      today.push({
        id: str + '-08:00-13:00',
        label: '今天',
        date: str,
        startTime: '08:00',
        endTime: '13:00'
      });
    }
    if (date.getHours() < 18) {
      today.push({
        id: str + '-13:00-19:00',
        label: '今天',
        date: str,
        startTime: '13:00',
        endTime: '19:00'
      });
    }
    if (today.length > 0) {
      timeList.push({
        label: '今天',
        timeList: today
      });
    }
    let tmrw = new Date();
    tmrw.setDate(tmrw.getDate() + 1);
    tmrw = tmrw.Format('yyyy-MM-dd');
    timeList.push({
      label: '明天',
      timeList: [{
        id: tmrw + '-08:00-13:00',
        label: '明天',
        date: tmrw,
        startTime: '08:00',
        endTime: '13:00'
      }, {
        id: tmrw + '-13:00-19:00',
        label: '明天',
        date: tmrw,
        startTime: '13:00',
        endTime: '19:00'
      }]
    });
    let tdat = new Date();
    tdat.setDate(tdat.getDate() + 2)
    tdat = tdat.Format('yyyy-MM-dd');
    timeList.push({
      label: '后天',
      timeList: [{
        id: tdat + '-08:00-13:00',
        label: '后天',
        date: tdat,
        startTime: '08:00',
        endTime: '13:00'
      }, {
        id: tdat + '-13:00-19:00',
        label: '后天',
        date: tdat,
        startTime: '13:00',
        endTime: '19:00'
      }]
    });
    let selectTimeLeftIndex = 0;
    if (this.data.selectTime && this.data.selectTime.id) {
      for (const i in timeList) {
        let item = timeList[i];
        if (item.label == this.data.selectTime.label) {
          selectTimeLeftIndex = i;
          break;
        }
      }
    }
    this.setData({
      timeList: timeList,
      isShowSelectTime: true,
      selectTimeTmp: this.data.selectTime,
      selectTimeLeftIndex: selectTimeLeftIndex
    })
  },

  onPopCloseTap() {
    this.setData({
      isShowSelectTime: false
    })
  },

  onSelectTimeOKTap() {
    let item = this.data.selectTimeTmp && this.data.selectTimeTmp.id ? this.data.selectTimeTmp : {};
    this.setData({
      selectTime: item,
      selectTimeTmp: {},
      isShowSelectTime: false
    })
  },

  onPopCardTap() {

  },

  onLeftItemTap(e) {
    const index = e.currentTarget.dataset.index;
    this.setData({
      selectTimeLeftIndex: index
    })
  },

  onRightTap(e) {
    const item = e.currentTarget.dataset.item;
    this.setData({
      selectTimeTmp: item
    })
  },

  onSelectAddrTap() {
    let url = '/pages/user/address/list/list2';
    if (this.data.address && this.data.address.id) {
      url += '?addressId=' + this.data.address.id;
    }
    wx.navigateTo({
      url: url,
    })
  },

  async getGasFee() {
    if (this.data.disputeJournalSummary && this.data.disputeJournalSummary.disputeJournalCode && this.data.address && this.data.address.id) {
      let res = await request.get('ylf/buyer/buyer/delivery/freight', {
        code: this.data.disputeJournalSummary.disputeJournalCode,
        addressId: this.data.address.id
      });
      this.setData({
        gasFee: res.data.price
      })
    }
    this.verify();
  },

  verify() {
    let isClick = true;
    if (!(this.data.selectTime && this.data.selectTime.startTime)) {
      isClick = false;
    } else if (!(this.data.address && this.data.address.id)) {
      isClick = false;
    }
    if (this.data.isClick != isClick) {
      this.setData({
        isClick: isClick
      });
    }
  },

  async btnOKTap() {
    let params = {
      code: this.data.disputeJournalSummary.disputeJournalCode,
      dayType: this.data.selectTime.label,
      startTime: this.data.selectTime.startTime,
      endTime: this.data.selectTime.endTime,
      addressId: this.data.address.id
    };
    let res = await request.get('ylf/buyer/buyer/delivery/pickup', params);
    if (res.succeed) {
      const pages = getCurrentPages();
      const prevPages = pages[pages.length - 2];
      prevPages.setData({
        isReload: true
      }, () => {
        wx.navigateBack();
      });
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    let data = options.data;
    if (data) {
      this.setData({
        disputeJournalSummary: JSON.parse(data),
        expressNo: '',
        deliverySupplier: {},
        mobile: '',
        remark: '',
        imageList: [],
        isClick: false
      })
    } else {
      wx.navigateBack();
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {
    this.getGasFee();
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
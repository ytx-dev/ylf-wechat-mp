// pages/user/order/refund/delivery/self/index.js
const app = getApp();
const request = require('../../../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    disputeJournalSummary: {
      disputeStatus: 0
    },
    expressNo: '',
    deliverySupplier: {},
    mobile: '',
    remark: '',
    imageList: [],
    isClick: false
  },

  onImageTap(e) {
    const list = [];
    for (const i in this.data.imageList) {
      list.push(imageDomain + this.data.imageList[i]);
    }
    wx.previewImage({
      current: imageDomain + e.currentTarget.dataset.url,
      urls: list
    })
  },

  onImageDelTap(e) {
    const index = e.currentTarget.dataset.index;
    const list = this.data.imageList;
    list.splice(index, 1);
    this.setData({
      imageList: list
    });
  },

  btnAddImage(e) {
    const _this = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      success(res) {
        if (res.tempFilePaths && res.tempFilePaths.length > 0) {
          wx.showLoading({
            title: '上传中',
          })
          _this.uploadImage(res.tempFilePaths[0]);
        }
      }
    })
  },

  async uploadImage(path) {
    const _this = this;
    wx.uploadFile({
      url: app.globalData.apiDomain + 'ylf/buyer/buyer/disputes/application/reason/proof2',
      filePath: path,
      name: 'file',
      header: {
        'cookie': 'remember-me=' + app.globalData.rememberMe
      },
      success(res) {
        const data = JSON.parse(res.data);
        if (data.succeed && data.data && data.data.imagePath) {
          let list = _this.data.imageList;
          list.push(data.data.imagePath);
          _this.setData({
            imageList: list
          })
        }
      },
      complete() {
        wx.hideLoading();
      }
    })
  },

  bindExpressNoInput(e) {
    this.setData({
      expressNo: e.detail.value
    }, () => {
      this.verify();
    })
  },

  bindMobileInput(e) {
    this.setData({
      mobile: e.detail.value
    }, () => {
      this.verify();
    })
  },

  bindExpressNoInput(e) {
    this.setData({
      expressNo: e.detail.value
    }, () => {
      this.verify();
    })
  },

  onSelectCourierTap() {
    let url = '/pages/user/order/refund/delivery/selectCourier/index';
    if(this.data.deliverySupplier && this.data.deliverySupplier.id) {
      url += '?data=' + this.data.deliverySupplier.id
    }
    wx.navigateTo({
      url: url
    })
  },

  verify() {
    let isClick = true;
    if (!this.data.expressNo) {
      isClick = false;
    } else if (!(this.data.deliverySupplier && this.data.deliverySupplier.id)) {
      isClick = false;
    } else if (!this.data.mobile || this.data.mobile.length != 11) {
      isClick = false;
    }
    if (this.data.isClick != isClick) {
      this.setData({
        isClick: isClick
      });
    }
  },

  async btnOKTap() {
    let params = {
      expressNo: this.data.expressNo,
      code: this.data.disputeJournalSummary.disputeJournalCode,
      deliverySupplierId: this.data.deliverySupplier.id,
      deliverySupplierName: this.data.deliverySupplier.supplierName,
      mobile: this.data.mobile,
      remark: this.data.remark
    };
    let images = '';
    for(const i in this.data.imageList) {
      let item = this.data.imageList[i];
      if(images) {
        images += ',';
      }
      images += item;
    }
    if(images) {
      params.images = images;
    }
    let res = await request.get('ylf/buyer/buyer/delivery/send', params);
    if (res.succeed) {
      const pages = getCurrentPages();
      const prevPages = pages[pages.length - 2];
      prevPages.setData({
        isReload: true
      }, () => {
        wx.navigateBack();
      });
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    let data = options.data;
    if (data) {
      this.setData({
        disputeJournalSummary: JSON.parse(data),
        expressNo: '',
        deliverySupplier: {},
        mobile: '',
        remark: '',
        imageList: [],
        isClick: false
      })
    } else {
      wx.navigateBack();
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
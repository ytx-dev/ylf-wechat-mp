// pages/user/order/refund/delivery/delivery/index.js
const app = getApp();
const request = require('../../../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    disputeCode: '',
    deliveryInfo: {},
    lastResult: {}
  },

  onCopyTap() {
    wx.setClipboardData({
      data: this.data.deliveryInfo.expressNo
    })
  },

  async init() {
    let res = await request.get('ylf/buyer/buyer/disputes/delivery/' + this.data.disputeCode);
    this.setData({
      deliveryInfo: res.data.deliveryInfo,
      lastResult: res.data.lastResult,
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    let code = options.code;
    if (code) {
      this.setData({
        disputeCode: code,
        deliveryInfo: {},
        lastResult: {}
      }, () => {
        this.init();
      });
    } else {
      wx.navigateBack();
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
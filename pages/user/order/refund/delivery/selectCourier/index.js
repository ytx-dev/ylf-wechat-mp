// pages/user/order/refund/delivery/selectCourier/index.js
const app = getApp();
const request = require('../../../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    dataList: [],
    currRow: {}
  },

  onItemTap(e) {
    let item = e.currentTarget.dataset.item;
    this.setData({
      currRow: item
    }, () => {
      const pages = getCurrentPages();
      const prevPages = pages[pages.length - 2];
      prevPages.setData({
        deliverySupplier: item
      })
    })
  },

  async init(id) {
    let res = await request.get('ylf/buyer/buyer/delivery/deliverySupplier', {
      sellerAccountId: app.globalData.sellerShop.accountId
    });
    let curr = {};
    if (id) {
      for (const i in res.data.deliverySupplier) {
        let item = res.data.deliverySupplier[i];
        if (item.id == id) {
          curr = item;
          break;
        }
      }
    }
    this.setData({
      dataList: res.data.deliverySupplier,
      currRow: curr
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    this.init(options.data);
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
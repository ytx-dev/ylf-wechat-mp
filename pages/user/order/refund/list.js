// pages/user/order/refund/list.js
const app = getApp();
const request = require('../../../../utils/request.js');
const util = require('../../../../utils/util.js');

Page({

  /**
   * Page initial data
   */
  data: {
    isShow: false,
    imageDomain: app.globalData.imageDomain,
    isReload: false,
    pageNum: 1,
    itemList: [],
    isLastPage: false,
    statusNames: {
      0: '退款关闭',
      1: '待门店处理退款',
      2: '退款完成',
      3: '待买家修改申请',
      4: '待买家退货'
    }
  },

  onItemTap(e) {
    util.navigation_to("/pages/user/order/refund/refund?disputeCode=" + e.currentTarget.dataset.code, 1);
  },

  async initData() {
    this.setData({
      pageNum: 1
    }, () => {
      this.getList();
    })
  },

  async getList() {
    const res = await request.get('ylf/buyer/buyer/disputes', {
      pageNum: this.data.pageNum
    });
    let list = [];
    let isLastPage = true;
    if(res.data) {
      list = this.data.itemList.concat(res.data.disputeJournalPage.list);
      isLastPage = res.data.disputeJournalPage.isLastPage;
    }
    this.setData({
      isShow: true,
      itemList: list,
      isLastPage: isLastPage
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.initData();
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    if (this.data.isReload) {
      this.setData({
        isReload: false
      }, () => {
        this.initData();
      })
    }
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {
    if (!this.data.isLastPage) {
      this.setData({
        pageNum: this.data.pageNum + 1
      }, () => {
        this.getList();
      });
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
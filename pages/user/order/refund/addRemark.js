// pages/user/order/refund/addRemark.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    disputeCode: null,
    updatedAt: null,
    emarks: '',
    imageList: [],
    isClick: false
  },

  bindTextAreaBlur(e) {
    this.setData({
      remarks: e.detail.value
    }, () => {
      this.verify();
    })
  },

  onImageTap(e) {
    const list = [];
    for (const i in this.data.imageList) {
      list.push(imageDomain + this.data.imageList[i]);
    }
    wx.previewImage({
      current: imageDomain + e.currentTarget.dataset.url,
      urls: list
    })
  },

  onImageDelTap(e) {
    const index = e.currentTarget.dataset.index;
    const list = this.data.imageList;
    list.splice(index, 1);
    this.setData({
      imageList: list
    });
  },

  btnAddImage(e) {
    const _this = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      success(res) {
        if (res.tempFilePaths && res.tempFilePaths.length > 0) {
          wx.showLoading({
            title: '上传中',
          })
          _this.uploadImage(res.tempFilePaths[0]);
        }
      }
    })
  },

  async uploadImage(path) {
    const _this = this;
    wx.uploadFile({
      url: app.globalData.apiDomain + 'ylf/buyer/buyer/disputes/application/reason/proof2',
      filePath: path,
      name: 'file',
      header: {
        'cookie': 'remember-me=' + app.globalData.rememberMe
      },
      success(res) {
        const data = JSON.parse(res.data);
        if (data.succeed && data.data && data.data.imagePath) {
          let list = _this.data.imageList;
          list.push(data.data.imagePath);
          _this.setData({
            imageList: list
          })
        }
      },
      complete() {
        wx.hideLoading();
      }
    })
  },

  verify() {
    let isClick = true;
    if (!this.data.remarks) {
      isClick = false;
    }
    if (this.data.isClick != isClick) {
      this.setData({
        isClick: isClick
      });
    }
  },

  async btnOKTap() {
    let imageProofs = '';
    if (this.data.imageList) {
      for (const i in this.data.imageList) {
        if (imageProofs) {
          imageProofs += ',';
        }
        imageProofs += this.data.imageList[i];
      }
    }
    const res = await request.post('ylf/buyer/buyer/disputes/' + this.data.disputeCode + '/message?updatedAt=' + this.data.updatedAt + '&message=' + this.data.remarks + '&imageProofs=' + imageProofs);
    if (res.succeed) {
      const pages = getCurrentPages();
      const prevPages = pages[pages.length - 2];
      prevPages.setData({
        isReload: true
      }, () => {
        wx.navigateBack();
      });
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      disputeCode: options.disputeCode,
      updatedAt: options.updatedAt
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
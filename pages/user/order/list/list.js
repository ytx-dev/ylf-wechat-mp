// pages/user/order/list/list.js
const app = getApp();
const request = require('../../../../utils/request.js');
const util = require('../../../../utils/util.js');
const pauyUtil = require('../../../../utils/pay.js');

Page({

  /**
   * Page initial data
   */
  data: {
    isShow: false,
    imageDomain: app.globalData.imageDomain,
    orderList: [],
    status: '',
    reasonList: [],
    isLoading: false
  },

  async onBtnBuyTap(e) {
    const item = e.currentTarget.dataset.item;
    const res = await request.post('ylf/trade/cart/again?orderNo=' + item.serialNumber);
    if (res.succeed) {
      wx.switchTab({
        url: '/pages/cart/cart',
      });
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none',
        duration: 2000
      });
    }
  },

  openDetail(e) {
    util.navigation_to("/pages/user/order/detail/detail?orderId=" + e.currentTarget.dataset.type, 1);
  },

  async onBtnCancelTap(e) {
    const item = e.currentTarget.dataset.item;
    if (item.status === 10) {
      this.showReasons(item.serialNumber);
    } else {
      /**
       * skipType == cancel: 进入订单取消申请页
       * skipType == dispute: 进入订单售后申请页
       * skipType == detail: 进入售后详情页，code即详情页地址
       */
      const type = await this.getDisputesTyoe(item.serialNumber);
      if (type === 'cancel') {
        let data = {
          orderNo: item.serialNumber,
          myState: 1,
          disputeJournalItemList: item.orderItemList
        };
        util.navigation_to("/pages/user/order/cansel/index?order=" + JSON.stringify(data), 1);
      } else if (type === 'dispute') {

      } else if (type === 'detail') {

      }
    }
  },

  async getDisputesTyoe(serialNumber) {
    const res = await request.get('ylf/buyer/buyer/disputes/operation/skip', {
      orderNo: serialNumber
    });
    return res.data.skipType;
  },

  async showReasons(serialNumber) {
    const _this = this;
    const reasonList = await _this.getReasons();
    wx.showActionSheet({
      alertText: '请选择取消原因',
      itemList: reasonList,
      success(res) {
        _this.cancelOrder(serialNumber, _this.data.reasonList[res.tapIndex]);
      }
    });
  },

  async onBtnPayTap(e) {
    const orderNo = e.currentTarget.dataset.item.serialNumber;
    const res = await request.get('ylf/trade/pay/' + orderNo);
    if (!res.succeed) {
      wx.showToast({
        icon: 'none',
        title: res.message,
      })
    } else {
      if (res.data.orderPaymentHistory.payAmount > 0) {
        pauyUtil.payment(orderNo, res.data.orderPaymentHistory.paymentSn, res => {
          if (!res) {
            wx.showToast({
              title: '支付未完成，请重新支付',
              icon: 'none',
              duration: 2000
            });
          }
          this.setData({
            paying: false
          });
          wx.redirectTo({
            url: '/pages/user/order/detail/detail?orderId=' + orderNo,
          });
        });
      } else {
        // check order payment
        pauyUtil.checkPay(res.data.orderPaymentHistory.paymentSn, (res) => {
          if (!res.succeed) {
            wx.showToast({
              icon: 'none',
              title: res.message,
            })
          }
          this.setData({
            paying: false
          });
          wx.redirectTo({
            url: '/pages/user/order/detail/detail?orderId=' + orderNo,
          });
        });
      }
    }

  },

  onBtnDistTap(e) {

  },

  onBtnCompleteTap(e) {
    this.complateOrder(e.currentTarget.dataset.item.serialNumber);
  },

  onStatusTap(e) {
    this.setData({
      status: e.currentTarget.dataset.type + ''
    }, () => {
      this.initData();
    });
  },

  initData() {
    this.setData({
      orderList: []
    }, () => {
      this.getList();
    });
  },

  async getList() {
    this.setData({
      isLoading: true
    })
    wx.showLoading({
      title: '加载中',
    });
    let orderId = null;
    if (this.data.orderList && this.data.orderList.length > 0) {
      orderId = this.data.orderList[this.data.orderList.length - 1].id;
    }
    let params = {
      status: this.data.status
    };
    if (orderId) {
      params['order_id'] = orderId;
    }
    const res = await request.get('ylf/trade/order', params);
    let orders = res.data.orders;
    for(const i in orders) {
      let order = orders[i];
      let orderItemList = order.orderItemList;
      let total = 0;
      for(const si in orderItemList) {
        total += orderItemList[si].number;
      }
      order.itemCount = total;
    }
    this.setData({
      isShow: true,
      orderList: this.data.orderList.concat(orders)
    }, () => {
      wx.hideLoading();
      this.setData({
        isLoading: false
      });
      this.setExpiryTime();
    });
  },

  setExpiryTime() {
    if (!this.data.status || this.data.status === '10') {
      const list = this.data.orderList;
      let isChange = false;
      for (const i in list) {
        const item = list[i];
        if (item.status === 10 && item.expiryTime) {
          const time = (15 * 60 * 1000 + item.createdAt) - new Date().getTime();
          if (time > 0) {
            let expiryTime = util.convertTimeDian(time);
            list[i].expiryTimeStr = expiryTime;
          } else {
            list[i].expiryTimeStr = '00:00';
            list[i].status = 50;
          }
          isChange = true;
        }
      }
      if (isChange) {
        this.setData({
          orderList: list
        }, () => {
          setTimeout(this.setExpiryTime, 1000);
        })
      }
    }
  },

  async getReasons() {
    const res = await request.get('ylf/buyer/buyer/disputes/reasons');
    this.setData({
      reasonList: res.data.reasonList
    });
    const reasonList = [];
    for (const i in res.data.reasonList) {
      reasonList.push(res.data.reasonList[i].reason);
    }
    return reasonList;
  },

  async cancelOrder(serialNumber, reason) {
    const res = await request.put('ylf/trade/order?order_no=' + serialNumber + '&cancel_reason=' + reason.reason);
    if (res.succeed) {
      this.initData();
    }
  },

  async complateOrder(serialNumber) {
    const res = await request.put('ylf/trade/order/' + serialNumber);
    if (res.succeed) {
      wx.showToast({
        title: '确认收货成功',
        icon: 'success',
        duration: 2000
      });
      this.initData();
    } else {
      wx.showToast({
        title: '确认收货失败',
        icon: 'none',
        duration: 2000
      });
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    let status = '';
    if (options.status) {
      status = options.status;
    }
    this.setData({
      status: status
    }, () => {
      this.initData();
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {
    if (!this.data.isLoading) {
      this.getList();
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
// pages/user/order/payment/payment_result.js
const app = getApp();
const util = require('../../../../utils/util.js');
const request = require('../../../../utils/request.js');
const UiUtils = require("../../../../lib/qrcode/UiUtils.js");
const cart = require('../../../../utils/cart.js');

Page({

  /**
   * Page initial data
   */
  data: {
    isPintuan: false,
    paymentData: {},
    orderCombinationVo: {},
    imageDomain: app.globalData.imageDomain,
    cartNumData: app.globalData.cartData,
    headImg: '',
    nickName: '',
    animationX: null,
    animationY: null,
    ballX: 0,
    ballY: 0,
    showBall: false,
    isLoading: false,
    guessList: [],
    orderBy: '',
    pageNumber: 1,
    hasNextPage: true,
    pintuanData: {},
    pintuanTime: 0,
    pintuanTimer: null,
  },

  gotoOrder() {
    wx.redirectTo({
      url: '/pages/user/order/detail/detail?orderId=' + this.data.paymentData.orderNo,
    });
  },

  gotoHome() {
    wx.switchTab({
      url: '/pages/index/index/index',
    })
  },

  initGuessList() {
    this.setData({
      guessList: [],
      pageNumber: 1,
      orderBy: '',
      hasNextPage: true
    }, () => {
      this.getGuessList();
    });
  },

  initCartData() {
    cart.initCartData(() => {
      this.setData({
        cartNumData: app.globalData.cartData
      });
    });
  },

  setCartDataItemAdd(itemId) {
    cart.setCartDataItemAdd(itemId);
    this.setData({
      cartNumData: app.globalData.cartData
    })
  },

  gotoItem(e) {
    let item = e.currentTarget.dataset['item'];
    if (item) {
      wx.navigateTo({
        url: '/pages/item/detail/detail?itemId=' + item.itemId
      })
    }
  },

  async onItemAddCartTap(e) {
    if (this.data.isLoading) {
      return;
    }
    app.login(() => {
      let itemId = e.currentTarget.dataset['itemId'];
      this.setCartDataItemAdd(itemId);
      this.addCart(itemId, e);
    });
  },

  async addCart(itemId, e) {
    this.setData({
      isLoading: true
    });
    const res = await request.post('ylf/trade/cart/' + app.globalData.sellerShop.accountId + '/' + itemId);
    if (res.succeed) {
      let ballX = e.touches[0].clientX;
      let ballY = e.touches[0].clientY;
      this.createAnimation(ballX, ballY);
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      });
      this.setData({
        isLoading: false
      });
    }
  },

  setDelayTime(sec) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, sec)
    });
  },

  // 创建动画
  createAnimation(ballX, ballY) {
    var sysinfo = wx.getSystemInfoSync();
    let _this = this;
    let bottomX = UiUtils.rpx2px(480);
    let bottomY = sysinfo.windowHeight;
    let animationX = _this.flyX(bottomX, ballX); // 创建小球水平动画
    let animationY = _this.flyY(bottomY, ballY); // 创建小球垂直动画
    _this.setData({
      ballX: ballX,
      ballY: ballY,
      showBall: true
    });
    _this.setDelayTime(100).then(() => {
      // 100ms延时,  确保小球已经显示
      _this.setData({
        animationX: animationX.export(),
        animationY: animationY.export()
      })
      // 400ms延时, 即小球的抛物线时长
      return _this.setDelayTime(400);
    }).then(() => {
      _this.setData({
        animationX: _this.flyX(0, 0, 0).export(),
        animationY: _this.flyY(0, 0, 0).export(),
        showBall: false,
        isLoading: false
      });
      _this.initCartData();
    })
  },

  // 水平动画
  flyX(bottomX, ballX, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'linear',
    })
    animation.translateX(bottomX - ballX).step();
    return animation;
  },

  // 垂直动画
  flyY(bottomY, ballY, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'ease-in',
    })
    animation.translateY(bottomY - ballY).step();
    return animation;
  },

  async getGuessList() {
    let params = {
      pageNum: this.data.pageNumber,
      sellerAccountId: app.globalData.sellerShop.accountId
    }
    if (!app.globalData.isInRange) {
      params.type = 2;
    }
    if (this.data.orderBy) {
      params.sort = this.data.orderBy;
    }
    let res = await request.get('ylf/search/guess', params);
    if (res && res.data) {
      let page = res.data.page;
      this.setData({
        guessList: this.data.guessList.concat(page.list),
        hasNextPage: page.pageNum < page.pages,
        orderBy: page.orderBy
      });
    }
  },

  initNameAndHead(name, headImg) {
    let fullHeadImg = headImg ? headImg : (app.globalData.account && app.globalData.account.headImg ? app.globalData.account.headImg : (app.globalData.userInfo && app.globalData.userInfo.avatarUrl ? app.globalData.userInfo.avatarUrl : ''));
    if (!(/http|https/.test(fullHeadImg))) {
      fullHeadImg = app.globalData.imageDomain + fullHeadImg;
    }
    this.setData({
      headImg: fullHeadImg,
      nickName: name ? name : (app.globalData.account && app.globalData.account.nickName ? app.globalData.account.nickName : '')
    });
  },

  async sqlGetOrderCount() {
    const res = await request.get('ylf/buyer/buyer/show');
    const data = res.data;
    if (data) {
      this.initNameAndHead(data.name, data.headImg);
    }
  },

  async getOrderData(orderNo) {
    let res = await request.get('ylf/trade/paySuccess?orderNo=' + orderNo);
    let currentTime = null;
    if (res.data.type == 1 && res.data.orderCombinationVo) {
      await this.sqlGetOrderCount();
      currentTime = res.data.orderCombinationVo.stopTime - res.data.orderCombinationVo.nowTime;
      res.data.orderCombinationVo.needNum = res.data.orderCombinationVo.peopleNumber - res.data.orderCombinationVo.detailList.length;
      if (res.data.orderCombinationVo.detailList) {
        for (const i in res.data.orderCombinationVo.detailList) {
          let item = res.data.orderCombinationVo.detailList[i];
          if (item.headerImg) {
            item.headerImg = this.data.imageDomain + item.headerImg;
          } else if (item.nikeName == this.data.nickName) {
            item.headerImg = this.data.headImg
          }
        }
      }
    }
    if (this.data.pintuanTimer) {
      clearTimeout(this.data.pintuanTimer);
    }
    this.setData({
      paymentData: res.data,
      orderCombinationVo: res.data.orderCombinationVo,
      pintuanData: {},
      pintuanTime: currentTime,
      pintuanTimer: null
    }, () => {
      if (currentTime) {
        this.setCountdown();
      }
    })
  },

  setCountdown() {
    let time = this.data.pintuanTime - 1000;
    if (time > 0) {
      let timer = setTimeout(() => {
        this.setCountdown();
      }, 1000);
      let pintuanData = this.data.pintuanData;
      let timeStr = util.convertTimeDian3(time);
      timeStr = timeStr.split(':');
      pintuanData.hh = timeStr[0];
      pintuanData.mm = timeStr[1];
      pintuanData.ss = timeStr[2];
      this.setData({
        pintuanData: pintuanData,
        pintuanTime: time,
        pintuanTimer: timer
      });
    } else {
      this.setData({
        pintuanData: {
          hh: '00',
          mm: '00',
          ss: '00'
        },
        pintuanTime: null,
        pintuanTimer: null
      });
    }
  },

  async init(orderNo) {
    this.initCartData();
    this.getOrderData(orderNo);
    this.initGuessList();
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    this.setData({
      isPintuan: options.isPintuan > 0 ? true : false
    }, () => {
      this.init(options.orderNo);
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {
    if (this.data.hasNextPage) {
      this.setData({
        pageNumber: this.data.pageNumber + 1,
      }, () => {
        this.getGuessList();
      })
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    //
    if (this.data.isPintuan && (this.data.orderCombinationVo.status == 0 || this.data.orderCombinationVo.status == 2)) {
      const promise = new Promise(resolve => {
        request.get('ylf/trade/imageCombiner/combination', {
          groupId: this.data.orderCombinationVo.groupId
        }).then((res) => {
          if (res.data && res.data.shareUrl) {
            let path = '/pages/pintuan/share/index?groupId=' + this.data.orderCombinationVo.groupId + '&shopId=' + app.globalData.sellerShop.accountId;
            if (app.globalData.spreadNumber) {
              path += '&spreadNumber=' + app.globalData.spreadNumber;
            }
            resolve({
              title: res.data.shareTitle,
              path: path,
              imageUrl: res.data.shareUrl
            });
          }
        });
      });
      let pd = '/pages/pintuan/share/index?groupId=' + this.data.orderCombinationVo.groupId + '&shopId=' + app.globalData.sellerShop.accountId;
      if (app.globalData.spreadNumber) {
        pd += '&spreadNumber=' + app.globalData.spreadNumber;
      }
      return {
        title: '拼团活动',
        path: pd,
        promise
      }
    } else {
      return app.getShareCommonData();
    }
  }
})
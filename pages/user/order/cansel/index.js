// pages/user/order/cansel/index.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    order: null,
    shippingFee: 0,
    maxRefundAmount: 0,
    reasonList: [],
    selectReason: {
      id: 0,
      reason: '请选择'
    },
    remarks: '',
    imageList: [],
    isClick: false,
    disputeCode: '',
    updatedAt: null,
    modalName: ''
  },

  async initData() {
    this.getAmount();
  },

  async initEditData() {
    const res = await request.get('ylf/buyer/buyer/disputes/' + this.data.disputeCode + '/modification');
    const data = res.data.disputeJournalDetail;
    let images = [];
    if (data.disputeJournalApplyReason.disputeJournalReasonImageProofList) {
      for (const i in data.disputeJournalApplyReason.disputeJournalReasonImageProofList) {
        images.push(data.disputeJournalApplyReason.disputeJournalReasonImageProofList[i].imageUrl);
      }
    }
    this.setData({
      order: {
        orderNo: data.disputeJournalOrder.orderNo,
        disputeJournalItemList: data.disputeJournalItemList
      },
      shippingFee: data.refundDistributionAmount,
      maxRefundAmount: data.amount,
      selectReason: data.disputeJournalApplyReason,
      remarks: data.disputeJournalApplyReason.remarks,
      imageList: images,
      updatedAt: data.updatedAt,
      isClick: false
    })
  },

  getArgs() {
    let args = '';
    for (const i in this.data.order.disputeJournalItemList) {
      const item = this.data.order.disputeJournalItemList[i];
      if (args) {
        args += ',';
      }
      args += item.itemId + ':' + (item.itemSkuId ? item.itemSkuId : item.skuId) + ':' + (item.number ? item.number : item.itemCount);
    }
    return args;
  },

  async getAmount() {
    const args = this.getArgs();
    const res = await request.get('ylf/buyer/buyer/disputes/amount', {
      orderNo: this.data.order.orderNo,
      disputeItemArgs: args,
      applyReasonTemplateId: this.data.selectReason.id
    });
    this.setData({
      maxRefundAmount: res.data.maxRefundAmount,
      shippingFee: res.data.shippingFee
    });
  },

  async getReasons() {
    const res = await request.get('ylf/buyer/buyer/disputes/reasons');
    const reasonList = [];
    for (const i in res.data.reasonList) {
      let item = res.data.reasonList[i];
      if (item.status == 1) {
        reasonList.push(item);
      }
    }
    return reasonList;
  },

  async onReasonTap() {
    const _this = this;
    const reasonList = await _this.getReasons();
    this.setData({
      modalName: 'bottomModal',
      reasonList: reasonList
    })
  },

  onReasonItemTap(e) {
    let item = e.currentTarget.dataset.data;
    this.setData({
      selectReason: item
    }, () => {
      this.verify();
      this.getAmount();
      this.hideModal();
    })
  },

  hideModal() {
    this.setData({
      modalName: ''
    })
  },

  bindTextAreaBlur(e) {
    this.setData({
      remarks: e.detail.value
    }, () => {
      this.verify();
    })
  },

  onImageTap(e) {
    const list = [];
    for (const i in this.data.imageList) {
      list.push(imageDomain + this.data.imageList[i]);
    }
    wx.previewImage({
      current: imageDomain + e.currentTarget.dataset.url,
      urls: list
    })
  },

  onImageDelTap(e) {
    const index = e.currentTarget.dataset.index;
    const list = this.data.imageList;
    list.splice(index, 1);
    this.setData({
      imageList: list
    });
  },

  btnAddImage(e) {
    const _this = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      success(res) {
        if (res.tempFilePaths && res.tempFilePaths.length > 0) {
          wx.showLoading({
            title: '上传中',
          })
          _this.uploadImage(res.tempFilePaths[0]);
        }
      }
    })
  },

  async uploadImage(path) {
    const _this = this;
    wx.uploadFile({
      url: app.globalData.apiDomain + 'ylf/buyer/buyer/disputes/application/reason/proof2',
      filePath: path,
      name: 'file',
      header: {
        'cookie': 'remember-me=' + app.globalData.rememberMe
      },
      success(res) {
        const data = JSON.parse(res.data);
        if (data.succeed && data.data && data.data.imagePath) {
          let list = _this.data.imageList;
          list.push(data.data.imagePath);
          _this.setData({
            imageList: list
          })
        }
      },
      complete() {
        wx.hideLoading();
      }
    })
  },

  verify() {
    let isClick = true;
    if (this.data.selectReason.id < 1) {
      isClick = false;
    } else if (!this.data.remarks) {
      isClick = false;
    }
    if (this.data.isClick != isClick) {
      this.setData({
        isClick: isClick
      });
    }
  },

  async btnOKTap() {
    const args = this.getArgs();
    let imageProofs = '';
    if (this.data.imageList) {
      for (const i in this.data.imageList) {
        if (imageProofs) {
          imageProofs += ',';
        }
        imageProofs += this.data.imageList[i];
      }
    }
    let params = '';
    if (this.data.order.orderNo) {
      params += 'orderNo=' + this.data.order.orderNo;
    }
    if (args) {
      if (params) {
        params += '&';
      }
      params += 'disputeItemArgs=' + args;
    }
    if (params) {
      params += '&';
    }
    params += 'amount=' + this.data.maxRefundAmount;
    if (this.data.selectReason) {
      if (params) {
        params += '&';
      }
      params += 'applyReasonTemplateId=' + this.data.selectReason.id;
    }
    if (this.data.selectReason) {
      if (params) {
        params += '&';
      }
      params += 'reason=' + this.data.selectReason.reason;
    }
    if (this.data.remarks) {
      if (params) {
        params += '&';
      }
      params += 'remarks=' + this.data.remarks;
    }
    if (imageProofs) {
      if (params) {
        params += '&';
      }
      params += 'imageProofs=' + imageProofs;
    } else {
      if (params) {
        params += '&';
      }
      params += 'imageProofs=';
    }
    if (this.data.disputeCode && this.data.updatedAt) {
      params += '&updatedAt=' + this.data.updatedAt;
      const res = await request.post('ylf/buyer/buyer/disputes/' + this.data.disputeCode + '?' + params);
      if (res.succeed) {
        const pages = getCurrentPages();
        const prevPages = pages[pages.length - 2];
        prevPages.setData({
          isReload: true
        }, () => {
          wx.navigateBack();
        });
      } else {
        wx.showToast({
          title: res.message,
          icon: 'none'
        })
      }
    } else {
      const res = await request.post('ylf/buyer/buyer/disputes/application?' + params);
      if (res.succeed) {
        const pages = getCurrentPages();
        const prevPages = pages[pages.length - 2];
        prevPages.setData({
          isReload: true
        }, () => {
          wx.redirectTo({
            url: '/pages/user/order/refund/refund?disputeCode=' + res.data.disputeJournalCode
          });
        });
      }
    }

  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    if (options.order) {
      const order = JSON.parse(options.order);
      this.setData({
        order: order
      }, () => {
        this.initData();
      })
    } else {
      this.setData({
        disputeCode: options.disputeCode
      }, () => {
        this.initEditData();
      })
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
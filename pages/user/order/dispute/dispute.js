// pages/user/order/dispute/dispute.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    currentStage: 0,
    serialNumber: '',
    orderItemList: [],
    invalidOrderItemList: [],
    selected: false,
    order: null,
    selectReasonType: {
      name: '请选择',
      type: 0
    },
    selectReason: {
      id: 0,
      reason: '请选择'
    },
    reasonList: [],
    shippingFee: 0,
    maxRefundAmount: 0,
    refundAmount: 0,
    remarks: '',
    imageList: [],
    showReason: false,
    showAllReason: false,
    isClick: false,
    disputeCode: '',
    updatedAt: null,
    modalName: ''
  },

  async onBtnItemAddDelTap() {
    const res = await request.get('ylf/buyer/buyer/disputes/item', {
      orderNo: this.data.serialNumber,
      edit: 1
    });
    const selecedItems = {};
    for (const i in this.data.order.disputeJournalItemList) {
      const item = this.data.order.disputeJournalItemList[i];
      selecedItems[item.itemId] = true;
    }
    let list = [];
    for (const i in res.data.orderItemList) {
      let item = res.data.orderItemList[i];
      item.selected = selecedItems[item.itemId] ? true : false;
      list.push(item);
    }
    this.setData({
      currentStage: 0,
      orderItemList: list,
      invalidOrderItemList: res.data.invalidOrderItemList
    })
  },

  /** 选择退货商品 */
  onBtnOK1Tap(e) {
    let list = [];
    for (const i in this.data.orderItemList) {
      const item = this.data.orderItemList[i];
      if (item.selected) {
        item.itemCount = item.number;
        list.push(item);
      }
    }
    if (list.length) {
      this.setData({
        order: {
          disputeJournalItemList: list,
          orderNo: this.data.serialNumber,
        },
        currentStage: 1
      }, () => {
        this.verify();
      });
    } else {
      wx.showToast({
        title: '请选择商品',
        icon: "none"
      });
    }
  },

  clickGoods(e) {
    const index = e.currentTarget.dataset.index;
    let orderItemList = this.data.orderItemList;
    let selectCount = 0;
    for (const i in orderItemList) {
      let item = orderItemList[index];
      if (i === index + '') {
        item.selected = !item.selected;
      }
      if (item.selected) {
        selectCount++;
      }
    }
    this.setData({
      orderItemList: orderItemList,
      selected: selectCount === orderItemList.length
    });
  },

  clickTotal(e) {
    let orderItemList = this.data.orderItemList;
    let selected = !this.data.selected;
    for (const i in orderItemList) {
      orderItemList[i].selected = selected;
    }
    this.setData({
      orderItemList: orderItemList,
      selected: selected
    });
  },

  async getOrderItemList() {
    const res = await request.get('ylf/buyer/buyer/disputes/item', {
      orderNo: this.data.serialNumber,
      edit: 0
    });
    let list = [];
    for (const i in res.data.orderItemList) {
      let item = res.data.orderItemList[i];
      item.selected = false;
      list.push(item);
    }
    this.setData({
      orderItemList: list,
      invalidOrderItemList: res.data.invalidOrderItemList
    })
  },

  async initData() {
    this.getOrderItemList();
  },

  /** 选择退货商品 */

  /** 申请退款 */
  async initEditData() {
    const res = await request.get('ylf/buyer/buyer/disputes/' + this.data.disputeCode + '/modification');
    const data = res.data.disputeJournalDetail;
    let images = [];
    if (data.disputeJournalApplyReason.disputeJournalReasonImageProofList) {
      for (const i in data.disputeJournalApplyReason.disputeJournalReasonImageProofList) {
        images.push(data.disputeJournalApplyReason.disputeJournalReasonImageProofList[i].imageUrl);
      }
    }
    let items = [];
    for (const i in data.disputeJournalItemList) {
      let item = data.disputeJournalItemList[i];
      item.number = item.actualReturnsCount;
      items.push(item);
    }
    this.setData({
      currentStage: 1,
      serialNumber: data.disputeJournalOrder.orderNo,
      order: {
        orderNo: data.disputeJournalOrder.orderNo,
        disputeJournalItemList: items
      },
      shippingFee: data.refundDistributionAmount,
      refundAmount: data.amount,
      maxRefundAmount: data.maxRefundAmount,
      selectReasonType: {
        name: data.type === 0 ? '取消订单' : data.type === 1 ? '仅退款' : '退货退款',
        type: data.type
      },
      selectReason: data.disputeJournalApplyReason,
      remarks: data.disputeJournalApplyReason.remarks,
      imageList: images,
      updatedAt: data.updatedAt,
      showReason: true,
      showAllReason: true,
      isClick: false
    }, () => {
      this.verify();
    });
  },

  onReasonTypeTap() {
    const _this = this;
    wx.showActionSheet({
      itemList: ['退货退款', '仅退款'],
      success(res) {
        let data = {};
        if (res.tapIndex === 0) {
          data = {
            name: '退货退款',
            type: 2
          }
        } else {
          data = {
            name: '仅退款',
            type: 1
          }
        }
        if (_this.data.selectReasonType.type != data.type) {
          _this.setData({
            showReason: true,
            selectReason: {
              id: 0,
              reason: '请选择'
            },
            selectReasonType: data
          });
        }
      }
    })
  },

  async onReasonTap() {
    const _this = this;
    const reasonList = await _this.getReasons();
    this.setData({
      modalName: 'bottomModal',
      reasonList: reasonList
    })
  },

  onReasonItemTap(e) {
    let item = e.currentTarget.dataset.data;
    this.setData({
      showAllReason: true,
      selectReason: item
    }, () => {
      this.verify();
      this.getAmount();
      this.hideModal();
    })
  },

  hideModal() {
    this.setData({
      modalName: ''
    })
  },

  dealCount(e) {
    const type = e.currentTarget.dataset.type;
    let index = e.currentTarget.dataset.index;
    let order = this.data.order;
    let item = order.disputeJournalItemList[index];
    let number = item.number;
    let itemCount = item.itemCount;
    if (type === '0') {
      //减
      itemCount--;
      if (itemCount < 1) {
        itemCount = 1;
      }
    } else {
      //加
      itemCount++;
      if (itemCount > number) {
        itemCount = number;
      }
    }
    item.itemCount = itemCount;
    order.disputeJournalItemList[index] = item;
    this.setData({
      order: order
    }, () => {
      if (this.data.selectReason.id)
        this.getAmount();
    })
  },

  async getAmount() {
    const args = this.getArgs();
    const res = await request.get('ylf/buyer/buyer/disputes/amount', {
      orderNo: this.data.order.orderNo,
      disputeItemArgs: args,
      applyReasonTemplateId: this.data.selectReason.id
    });
    this.setData({
      maxRefundAmount: res.data.maxRefundAmount,
      refundAmount: res.data.maxRefundAmount,
      shippingFee: res.data.shippingFee
    });
  },

  async getReasons() {
    let itemIds = '';
    for (const i in this.data.order.disputeJournalItemList) {
      const item = this.data.order.disputeJournalItemList[i];
      if (itemIds) {
        itemIds += ',';
      }
      itemIds += item.itemId
    }
    const res = await request.get('ylf/buyer/buyer/disputes/reasons', {
      disputeJournalType: this.data.selectReasonType.type,
      itemIds: itemIds
    });
    const reasonList = [];
    for (const i in res.data.reasonList) {
      let item = res.data.reasonList[i];
      if (item.status == 1) {
        reasonList.push(item);
      }
    }
    return reasonList;
  },

  getArgs() {
    let args = '';
    for (const i in this.data.order.disputeJournalItemList) {
      const item = this.data.order.disputeJournalItemList[i];
      if (args) {
        args += ',';
      }
      args += item.itemId + ':' + (item.itemSkuId ? item.itemSkuId : item.skuId) + ':' + item.itemCount;
    }
    return args;
  },

  onAmountBlur(e) {
    let value = Number(e.detail.value);
    if (value > this.data.maxRefundAmount) {
      wx.showToast({
        title: '输入金额不能大于' + this.data.maxRefundAmount + '元',
        icon: "none"
      });
      value = this.data.maxRefundAmount;
    }
    this.setData({
      refundAmount: value
    }, () => {
      this.verify();
    });
  },

  bindTextAreaBlur(e) {
    this.setData({
      remarks: e.detail.value
    }, () => {
      this.verify();
    })
  },

  btnAddImage(e) {
    const _this = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      success(res) {
        if (res.tempFilePaths && res.tempFilePaths.length > 0) {
          wx.showLoading({
            title: '上传中',
          })
          _this.uploadImage(res.tempFilePaths[0]);
        }
      }
    })
  },

  async uploadImage(path) {
    const _this = this;
    wx.uploadFile({
      url: app.globalData.apiDomain + 'ylf/buyer/buyer/disputes/application/reason/proof2',
      filePath: path,
      name: 'file',
      header: {
        'cookie': 'remember-me=' + app.globalData.rememberMe
      },
      success(res) {
        const data = JSON.parse(res.data);
        if (data.succeed && data.data && data.data.imagePath) {
          let list = _this.data.imageList;
          list.push(data.data.imagePath);
          _this.setData({
            imageList: list
          }, () => {
            _this.verify();
          });
        }
      },
      complete() {
        wx.hideLoading();
      }
    })
  },

  verify() {
    let isClick = true;
    if (this.data.selectReasonType.type < 1) {
      isClick = false;
    } else if (this.data.selectReason.id < 1) {
      isClick = false;
    } else if (!this.data.remarks) {
      isClick = false;
    }
    if (this.data.isClick != isClick) {
      this.setData({
        isClick: isClick
      });
    }
  },

  async onBtnOK2Tap() {
    const args = this.getArgs();
    let imageProofs = '';
    if (this.data.imageList) {
      for (const i in this.data.imageList) {
        if (imageProofs) {
          imageProofs += ',';
        }
        imageProofs += this.data.imageList[i];
      }
    }
    let params = '';

    if (this.data.order.orderNo) {
      params += 'orderNo=' + this.data.order.orderNo;
    }
    if (this.data.selectReasonType) {
      if (params) {
        params += '&';
      }
      params += 'disputeJournalType=' + this.data.selectReasonType.type;
    }
    if (args) {
      if (params) {
        params += '&';
      }
      params += 'disputeItemArgs=' + args;
    }
    if (params) {
      params += '&';
    }
    params += 'amount=' + this.data.refundAmount;
    if (this.data.selectReason) {
      if (params) {
        params += '&';
      }
      params += 'applyReasonTemplateId=' + this.data.selectReason.id;
    }
    if (this.data.selectReason) {
      if (params) {
        params += '&';
      }
      params += 'reason=' + this.data.selectReason.reason;
    }
    if (this.data.remarks) {
      if (params) {
        params += '&';
      }
      params += 'remarks=' + this.data.remarks;
    }
    if (imageProofs) {
      if (params) {
        params += '&';
      }
      params += 'imageProofs=' + imageProofs;
    } else {
      if (params) {
        params += '&';
      }
      params += 'imageProofs=';
    }
    if (this.data.disputeCode && this.data.updatedAt) {
      params += '&updatedAt=' + this.data.updatedAt;
      const res = await request.post('ylf/buyer/buyer/disputes/' + this.data.disputeCode + '?' + params);
      if (res.succeed) {
        const pages = getCurrentPages();
        const prevPages = pages[pages.length - 2];
        prevPages.setData({
          isReload: true
        }, () => {
          wx.navigateBack();
        });
      } else {
        wx.showToast({
          title: res.message,
          icon: 'none'
        })
      }
    } else {
      const res = await request.post('ylf/buyer/buyer/disputes/application?' + params);
      if (res.succeed) {
        const pages = getCurrentPages();
        const prevPages = pages[pages.length - 2];
        prevPages.setData({
          isReload: true
        }, () => {
          wx.redirectTo({
            url: '/pages/user/order/refund/refund?disputeCode=' + res.data.disputeJournalCode
          });
        });
      }
    }
  },

  /** 申请退款 */

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    if (options.serialNumber) {
      this.setData({
        serialNumber: options.serialNumber
      }, () => {
        this.initData();
      })
    } else {
      this.setData({
        disputeCode: options.disputeCode
      }, () => {
        this.initEditData();
      })
    }

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
// pages/user/redbag/redbag.js
const app = getApp();
const request = require('../../../utils/request.js');
const util = require('../../../utils/util.js');

Page({

  /**
   * Page initial data
   */
  data: {
    showIndex: -1,
    credType: 2,
    canUseCount: 0,
    invalidCount: 0,
    receiveCount: 0,
    redBagList: [],
    pageNumber: 1,
    isLastPage: false
  },

  gotoItemList(e) {
    let item = e.currentTarget.dataset.item;
    console.log(item);
    if (item.usingRange === 0) {
      wx.switchTab({
        url: '/pages/index/index/index',
      })
    } else {
      wx.navigateTo({
        url: '/pages/item/redbag/list?redbag=' + JSON.stringify(item),
      })
    }
    // if (item.usingRange === 1) {
    //   //指定类目可用

    // } else if (item.usingRange === 2) {
    //   //指定商品可用
    //   wx.navigateTo({
    //     url: '/pages/item/redbag/list?redbag=' + JSON.stringify(item),
    //   })
    // } else {
    //   wx.switchTab({
    //     url: '/pages/index/index/index',
    //   })
    // }
  },

  gotoList(categoryId, categoryIds, categoryName) {
    const array = categoryIds.split(',');
    if (array.length === 3) {
      categoryId = array[1];
      categoryIds = array[1] + ',' + array[2];
    }
    util.navigation_to('/pages/item/list/list2?categoryId=' + categoryId + '&categoryIds=' + categoryIds + '&navigationBarTitleText=' + categoryName, 1);
  },

  gotoRedbagCenter() {
    wx.navigateTo({
      url: '/pages/user/redbag/center',
    })
  },

  gotoRedbagRule() {
    wx.navigateTo({
      url: '/pages/user/redbag/rule',
    })
  },

  showDetail(e) {
    const index = e.currentTarget.dataset.index;
    if (index === this.data.showIndex) {
      this.setData({
        showIndex: -1
      })
    } else {
      this.setData({
        showIndex: index
      })
    }
  },

  changeCredType(e) {
    if (this.data.credType !== e.currentTarget.dataset.type) {
      this.setData({
        redBagList: [],
        pageNumber: 1,
        credType: e.currentTarget.dataset.type
      }, () => {
        this.getList();
      });
    }
  },

  async initData() {
    this.getCount();
    this.getList();
  },

  async getCount() {
    const res = await request.get('ylf/buyer/buyer/redbag/count');
    const res2 = await request.get('ylf/buyer/buyer/redbag/list');
    this.setData({
      canUseCount: res.data.canUseCount,
      invalidCount: res.data.invalidCount,
      receiveCount: res2 && res2.data && res2.data.redBagBatches ? res2.data.redBagBatches.length : 0
    });
  },

  async getList() {
    let url = 'ylf/buyer/buyer/redbag/findPageUsableRedBag';
    if (this.data.credType === 1) {
      url = 'ylf/buyer/buyer/redbag/findPageDisabledRedBag';
    }
    const res = await request.get(url, {
      pageNumber: this.data.pageNumber
    });
    let list = [];
    for (const i in res.data.redBagPage.list) {
      const item = res.data.redBagPage.list[i];
      item.isShowExtime = (item.efficientEndDate - item.now) / 1000 < item.overTime;
      item.efficientDate = util.formatTime2(new Date(item.efficientDate), '-');
      item.efficientEndDate = util.formatTime2(new Date(item.efficientEndDate), '-');
      list.push(item);
    }
    this.setData({
      showIndex: -1,
      redBagList: this.data.redBagList.concat(list),
      isLastPage: res.data.redBagPage.pageNum <= res.data.redBagPage.pages
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    app.login(() => {
      this.initData();
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {
    if (!this.data.isLastPage) {
      this.setData({
        pageNumber: ++this.data.pageNumber
      }, () => {
        this.getList();
      });
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
// pages/user/redbag/center.js
const app = getApp();
const util = require('../../../utils/util.js');
const request = require('../../../utils/request.js');
const UiUtils = require("../../../lib/qrcode/UiUtils.js");
const cart = require('../../../utils/cart.js');

Page({

  /**
   * Page initial data
   */
  data: {
    showIndex: -1,
    dataList: [],
    imageDomain: app.globalData.imageDomain,
    cartNumData: app.globalData.cartData,
    animationX: null,
    animationY: null,
    ballX: 0,
    ballY: 0,
    showBall: false,
    isLoading: false,
    guessList: [],
    orderBy: '',
    pageNumber: 1,
    hasNextPage: true
  },

  gotoItemList(e) {
    let item = e.currentTarget.dataset.item;
    console.log(item);
    if (item.usingRange === 0) {
      wx.switchTab({
        url: '/pages/index/index/index',
      })
    } else {
      wx.navigateTo({
        url: '/pages/item/redbag/list?redbag=' + JSON.stringify(item),
      })
    }
    // if (item.usingRange === 1) {
    //   //指定类目可用

    // } else if (item.usingRange === 2) {
    //   //指定商品可用
    //   wx.navigateTo({
    //     url: '/pages/item/redbag/list?redbag=' + JSON.stringify(item),
    //   })
    // } else {
    //   wx.switchTab({
    //     url: '/pages/index/index/index',
    //   })
    // }
  },

  gotoList(categoryId, categoryIds, categoryName) {
    const array = categoryIds.split(',');
    if (array.length === 3) {
      categoryId = array[1];
      categoryIds = array[1] + ',' + array[2];
    }
    util.navigation_to('/pages/item/list/list2?categoryId=' + categoryId + '&categoryIds=' + categoryIds + '&navigationBarTitleText=' + categoryName, 1);
  },

  async onBtnReceiveTap(e) {
    let data = e.currentTarget.dataset.item;
    let res = await request.get('ylf/buyer/buyer/redbag/receive', {
      batchCode: data.batchCode
    });
    if (res.succeed) {
      let array = this.data.dataList;
      for (const i in array) {
        let item = array[i];
        if (item.id == data.id) {
          item.isReceive = true;
          break;
        }
      }
      this.setData({
        dataList: array
      })
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      })
    }
  },

  showDetail(e) {
    const index = e.currentTarget.dataset.index;
    if (index === this.data.showIndex) {
      this.setData({
        showIndex: -1
      })
    } else {
      this.setData({
        showIndex: index
      })
    }
  },

  initGuessList() {
    this.setData({
      guessList: [],
      pageNumber: 1,
      orderBy: '',
      hasNextPage: true
    }, () => {
      this.getGuessList();
    });
  },

  initCartData() {
    cart.initCartData(() => {
      this.setData({
        cartNumData: app.globalData.cartData
      });
    });
  },

  setCartDataItemAdd(itemId) {
    cart.setCartDataItemAdd(itemId);
    this.setData({
      cartNumData: app.globalData.cartData
    })
  },

  gotoItem(e) {
    let item = e.currentTarget.dataset['item'];
    if (item) {
      wx.navigateTo({
        url: '/pages/item/detail/detail?itemId=' + item.itemId
      })
    }
  },

  async onItemAddCartTap(e) {
    if (this.data.isLoading) {
      return;
    }
    app.login(() => {
      let itemId = e.currentTarget.dataset['itemId'];
      this.setCartDataItemAdd(itemId);
      this.addCart(itemId, e);
    });
  },

  async addCart(itemId, e) {
    this.setData({
      isLoading: true
    });
    const res = await request.post('ylf/trade/cart/' + app.globalData.sellerShop.accountId + '/' + itemId);
    if (res.succeed) {
      let ballX = e.touches[0].clientX;
      let ballY = e.touches[0].clientY;
      this.createAnimation(ballX, ballY);
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      });
      this.setData({
        isLoading: false
      });
    }
  },

  setDelayTime(sec) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, sec)
    });
  },

  // 创建动画
  createAnimation(ballX, ballY) {
    var sysinfo = wx.getSystemInfoSync();
    let _this = this;
    let bottomX = UiUtils.rpx2px(480);
    let bottomY = sysinfo.windowHeight;
    let animationX = _this.flyX(bottomX, ballX); // 创建小球水平动画
    let animationY = _this.flyY(bottomY, ballY); // 创建小球垂直动画
    _this.setData({
      ballX: ballX,
      ballY: ballY,
      showBall: true
    });
    _this.setDelayTime(100).then(() => {
      // 100ms延时,  确保小球已经显示
      _this.setData({
        animationX: animationX.export(),
        animationY: animationY.export()
      })
      // 400ms延时, 即小球的抛物线时长
      return _this.setDelayTime(400);
    }).then(() => {
      _this.setData({
        animationX: _this.flyX(0, 0, 0).export(),
        animationY: _this.flyY(0, 0, 0).export(),
        showBall: false,
        isLoading: false
      });
      _this.initCartData();
    })
  },

  // 水平动画
  flyX(bottomX, ballX, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'linear',
    })
    animation.translateX(bottomX - ballX).step();
    return animation;
  },

  // 垂直动画
  flyY(bottomY, ballY, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'ease-in',
    })
    animation.translateY(bottomY - ballY).step();
    return animation;
  },

  async getGuessList() {
    let params = {
      pageNum: this.data.pageNumber,
      sellerAccountId: app.globalData.sellerShop.accountId
    }
    if (!app.globalData.isInRange) {
      params.type = 2;
    }
    if (this.data.orderBy) {
      params.sort = this.data.orderBy;
    }
    let res = await request.get('ylf/search/guess', params);
    if (res && res.data) {
      let page = res.data.page;
      this.setData({
        guessList: this.data.guessList.concat(page.list),
        hasNextPage: page.pageNum < page.pages,
        orderBy: page.orderBy
      });
    }
  },

  async initRedbagList() {
    let res = await request.get('ylf/buyer/buyer/redbag/list');
    let array = [];
    for (const i in res.data.redBagBatches) {
      let item = res.data.redBagBatches[i];
      item.efficientDate = util.formatTime2(new Date(item.efficientDate), '-');
      item.efficientEndDate = util.formatTime2(new Date(item.efficientEndDate), '-');
      array.push(item);
    }
    this.setData({
      showIndex: -1,
      dataList: array
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    this.initRedbagList();
    this.initGuessList();
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {
    if (this.data.hasNextPage) {
      this.setData({
        pageNumber: this.data.pageNumber + 1,
      }, () => {
        this.getGuessList();
      })
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
// pages/user/coupon/coupon.js
const app = getApp();
const request = require('../../../utils/request.js');
const status = {
    '1': '未使用',
    '2': '已使用',
    '3': '已过期'
}
Page({

    /**
     * 页面的初始数据
     */
    data: {
        couponType: '1',
        couponList: [],
        pageNum: 1,
        pages: 1,
        loading: false,
        bottomWarring: false,
        //未使用
        canUseCount: 0,
        //已使用
        useCount: 0,
        //已过期
        loseCount: 0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        app.login(function () {
            that.getCouponsCount();
            that.getCoupons();
        });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.setData({
            pageNum: ++this.data.pageNum
        });
        this.getCoupons();
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        return app.getShareCommonData();
    },
    /**
     * 改变优惠券 Type
     */
    changeCouponType: function (event) {
        if (this.data.credType !== event.currentTarget.dataset.type) {
            this.setData({
                couponList: [],
                pageNum: 1,
                couponType: event.currentTarget.dataset.type
            });
            this.getCoupons();
        }
    },
    /**
     * 获取优惠券的单个状态详细列表
     */
    async getCoupons() {
        var that = this;
        if (!that.data.pages || that.data.pageNum <= that.data.pages) {
            wx.showLoading();
            const res = await request.get('buyer/coupons/wechatList', {
                'status': that.data.couponType,
                'pageNum': that.data.pageNum,
            });
            if (res && res.data.list) {
                for (var i = 0, len = res.data.list.length; i < len; i++) {
                    var item = res.data.list[i];
                    if (that.data.couponType == 1 && new Date().getTime() + 86400000 > item.endTime) {
                        item.status = '即将到期';
                    }
                    item.startTime = new Date(item.startTime).Format('yyyy.MM.dd');
                    item.endTime = new Date(item.endTime).Format('yyyy.MM.dd hh:mm');
                }
                that.setData({
                    couponList: that.data.couponList.concat(res.data.list),
                    pages: res.data.pages,
                    pageNum: res.data.pageNum
                });
            } else {
                that.setData({
                    couponList: [],
                    pages: 1,
                    pageNum: 1,
                    loading: false
                });
            }
            that.setData({
                bottomWarring: that.data.couponList.length > 10 ? true : false,
                loading: that.data.couponList.length == 0 ? true : false
            })
            wx.hideLoading();
        }
    },
    /**
     * 获取优惠券数量
     */
    async getCouponsCount() {
        var that = this;
        const res = await request.get('buyer/coupons/wechatCount');
        that.setData({
            canUseCount: res.data.canUseCount,
            useCount: res.data.useCount,
            loseCount: res.data.loseCount
        });
    }
})
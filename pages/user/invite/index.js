// pages/user/invite/index.js
const app = getApp();
const request = require('../../../utils/request.js');
const QR = require("../../../lib/qrcode/qrcode.js");
const UiUtils = require("../../../lib/qrcode/UiUtils.js");

Page({

  /**
   * Page initial data
   */
  data: {
    isShowQrcodeDialog: false,
    activeTabItem: 1,
    inviteData: {
      orderPeopleNumber: 0,
      spreadNumber: 0,
      spreadLogVo: []
    }
  },

  renderCodeImage() {
    var w2 = UiUtils.rpx2px(300);
    let url = this.data.activeTabItem > 0 ? 'https://ytx.com/ym/mp/invite' : this.data.inviteData.spreadAppUrl;
    if (url.indexOf('?spreadNumber=') < 0) {
      url += '?spreadNumber=' + this.data.inviteData.spreadNumber;
    }
    QR.qrApi.draw(url, "qrcodeCanvas", w2, w2);
  },

  onBtnQrcodeSaveTap() {
    wx.canvasToTempFilePath({
      canvasId: 'qrcodeCanvas',
      success(res) {
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success(res) {
            if(res.errMsg == 'saveImageToPhotosAlbum:ok') {
              wx.showToast({
                title: '保存成功',
                icon: 'none'
              })
            }
          }
        })
      }
    });
  },

  onDialogTabItemTap(e) {
    let index = e.currentTarget.dataset['index'];
    this.setData({
      activeTabItem: index
    }, () => {
      this.renderCodeImage();
    });
  },

  onBtnQrcodeTap() {
    this.setData({
      isShowQrcodeDialog: !this.data.isShowQrcodeDialog
    }, () => {
      this.renderCodeImage();
    });
  },

  doNothing() {},

  gotoInviteList() {
    if (this.data.inviteData.spreadLogVo && this.data.inviteData.spreadLogVo.length) {
      wx.navigateTo({
        url: '/pages/user/invite/list',
      })
    }
  },

  async getData() {
    let res = await request.get('ylf/buyer/spread/spread');
    if (res && res.succeed) {
      this.setData({
        inviteData: res.data
      })
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      })
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    this.getData();
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
// pages/user/invite/list.js
const app = getApp();
const request = require('../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    dataList: [],
    total: 0,
    pageNumber: 1
  },

  async getList() {
    let res = await request.get('ylf/buyer/spread/logList', {
      pageNumber: this.data.pageNumber
    });
    let page = res.data.page;
    this.setData({
      dataList: this.data.dataList.concat(page.list),
      total: page.total
    })
  },

  init() {
    this.setData({
      dataList: [],
      total: 0,
      pageNumber: 1
    }, () => {
      this.getList();
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    this.init();
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {
    if (this.data.dataList.length < this.data.total) {
      this.setData({
        pageNumber: ++this.data.pageNumber
      }, () => {
        this.getList();
      });
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
// pages/user/card/bind/cardBindConfirm.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    cardData: {}
  },

  async onBtnOKTap() {
    wx.showLoading({
      mask: true
    });
    const res = await request.post('ylf/gift/gift/bind?cardNo=' + this.data.cardData.cardNo + '&cardPassword=' + this.data.cardData.password, {});
    wx.hideLoading();
    if (res && res.succeed) {
      wx.showToast({
        title: '绑定成功',
        icon: 'none',
        duration: 2000
      });
      wx.navigateBack();
    } else {
      wx.showToast({
        title: '绑定失败',
        icon: 'none',
        duration: 2000
      })
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      cardData: JSON.parse(options.cardData)
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
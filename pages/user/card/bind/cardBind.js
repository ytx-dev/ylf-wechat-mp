// pages/user/card/cardBind.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    cardNo: '',
    cardPassword: '',
    isPassword: true
  },

  showPassowrd(){
    this.setData({
      isPassword: !this.data.isPassword
    })
  },

  bindCardNoInput(e) {
    this.setData({
      cardNo: e.detail.value
    })
  },

  bindPasswordInput(e) {
    this.setData({
      cardPassword: e.detail.value
    })
  },

  formSubmit(e) {
    if (this.data.cardNo && this.data.cardPassword) {
      this.checkCard();
    } else {
      wx.showToast({
        title: '请输入卡号和密码',
        icon: 'none',
        duration: 2000
      })
    }
  },

  async checkCard() {
    wx.showLoading({
      mask: true
    });
    const res = await request.post('ylf/gift/gift/validate?cardNo=' + this.data.cardNo + '&cardPassword=' + this.data.cardPassword, {});
    wx.hideLoading();
    const data = res.data.cardBindVerify;
    // const data = {
    //   "balance": 100,
    //   "cardNo": "string",
    //   "endDate": "2021-10-14 01:28:55",
    //   "zjBalance": 100
    // }
    if (data && data.balance) {
      const params = {
        password: this.data.cardPassword,
        ...data
      };
      wx.navigateTo({
        url: '/pages/user/card/bind/cardBindConfirm?cardData=' + JSON.stringify(params),
      });
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none',
        duration: 2000
      })
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    this.setData({
      cardNo: '',
      cardPassword: ''
    })
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
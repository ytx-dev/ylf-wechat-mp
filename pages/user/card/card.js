// pages/user/card/card.js
const app = getApp();
const request = require('../../../utils/request.js');

let _animation;
let _animationIndex = 0;
const _ANIMATION_TIME = 500;

Page({

  /**
   * Page initial data
   */
  data: {
    cardData: {
      balance: 0
    },
    animation: ''
  },

  startAnimation(){
    _animation = wx.createAnimation({
      duration: _ANIMATION_TIME,
      timingFunction: 'linear',
      delay: 0,
      transformOrigin: '50% 50% 0'
    });
    this.rotateAni(++_animationIndex);
  },

  rotateAni(n){
    _animation.rotate(360 * n).step()
    this.setData({
      animation: _animation.export()
    })
  },

  onBtnBindTap() {
    wx.navigateTo({
      url: '/pages/user/card/bind/cardBind',
    });
  },

  onBtnLogTap(e) {
    if (this.data.cardData && this.data.cardData.cardNo) {
      wx.navigateTo({
        url: '/pages/user/card/payment/paymentLog?cardNo=' + this.data.cardData.cardNo,
      });
    } else {
      wx.showToast({
        title: '暂无账单信息',
        icon: 'none',
        duration: 2000
      });
    }
  },

  onBtnQrcodeTap(e) {
    if (this.data.cardData.balance > 0) {
      wx.navigateTo({
        url: '/pages/user/card/payment/payment?cardNo=' + this.data.cardData.cardNo,
      });
    } else {
      wx.showToast({
        title: '购物卡余额为0，无法进行付款',
        icon: 'none',
        duration: 2000
      });
    }
  },

  async onBtnRefreshTap() {
    this.startAnimation();
    const res = await request.get('ylf/gift/gift/card/refresh', {});
    if (res.succeed) {
      wx.showToast({
        title: '余额已刷新',
        icon: 'none',
        duration: 1500
      });
      this.initData();
    }
  },

  async getList() {
    const res = await request.get('ylf/gift/gift/info', {});
    const data = res.data.giftCard;
    this.setData({
      cardData: data
    });
  },

  async initData() {
    this.getList();
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    app.login(() => {
      this.initData();
    });
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
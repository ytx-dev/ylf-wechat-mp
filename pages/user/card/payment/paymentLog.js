// pages/user/card/paymentLog.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    isShow: false,
    cardNo: '',
    pageNum: 1,
    hasNextPage: false,
    logList: [],
  },

  async getList() {
    const params = {
      pageNum: this.data.pageNum,
      pageSize: 10,
      cardNo: this.data.cardNo
    };
    const res = await request.get('ylf/gift/gift/bill', params);
    const data = res.data.bills;
    for(const i in data.list) {
      let item = data.list[i];
      if(item.summary === 'C'){
        item.icon = 'buy';
        item.typeName = '充值';
      } else if(item.summary === 'E') {
        item.icon = 'recharge';
        item.typeName = '绑卡';
      } else if(item.summary === 'S') {
        item.icon = 'consume';
        item.typeName = '消费';
      } else {
        item.icon = 'refund';
        item.typeName = '退款';
      }
    }
    this.setData({
      isShow: true,
      hasNextPage: data.pageNum < data.pages,
      logList: this.data.logList.concat(data.list)
    }, () => {
      wx.hideLoading();
    })
  },

  async initData() {
    wx.showLoading({
      title: '加载中',
    })
    this.getList();
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      pageNum: 1,
      cardNo: options.cardNo,
      logList: []
    }, () => {
      this.initData();
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {
    if (this.data.hasNextPage) {
      this.setData({
        pageNum: this.data.pageNum + 1,
      }, () => {
        this.getList();
      })
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
// pages/user/card/payment.js
const app = getApp();
const request = require('../../../../utils/request.js');
const QR = require("../../../../lib/qrcode/qrcode.js");
const barcode = require("../../../../lib/qrcode/barcode.js");
const UiUtils = require("../../../../lib/qrcode/UiUtils.js");

Page({

  /**
   * Page initial data
   */
  data: {
    cardNo: '',
    cardData: {},
    timeoutId: null,
    intervalId: null
  },

  async getList() {
    const res = await request.get('ylf/gift/gift/pay/code', {
      cardNo: this.data.cardNo
    });
    const data = res.data.giftCardPay;
    // const data = {
    //   "id": 1,
    //   "cardNo": "P000000002",
    //   "userId": 2,
    //   "memberId": "C00100000550",
    //   "billNo": null,
    //   "cardUserId": 1,
    //   "issuedStore": null,
    //   "faceValue": 0.00,
    //   "maxAmount": 0.00,
    //   "status": "Y",
    //   "qrcode": null,
    //   "payCode": "ZF210930001075402385",
    //   "balance": 84.00,
    //   "canDeposit": "Y",
    //   "canTransfer": null,
    //   "cardType": "12",
    //   "cardTypeName": "线上储值",
    //   "operatorId": null,
    //   "enableDate": "2021-09-16 00:00:00",
    //   "disableDate": "2023-09-16 00:00:00",
    //   "createdAt": "2021-09-30 11:19:18",
    //   "updatedAt": "2021-10-09 11:10:14"
    // };
    this.setData({
      cardData: data
    }, () => {
      this.renderPayCode();
      // clearTimeout(this.data.timeoutId);
      if (this.data.intervalId) {
        clearInterval(this.data.intervalId);
      }
      let siId = setInterval(this.checkResult, 3000);
      this.setData({
        intervalId: siId
      })
    });
  },

  renderPayCode() {
    var w = UiUtils.rpx2px(690)
    var h = UiUtils.rpx2px(200)
    barcode.createBarcode('barcodeCanvas', this.data.cardData.payCode, w, h);
    var w2 = UiUtils.rpx2px(400)
    QR.qrApi.draw(this.data.cardData.payCode, "qrcodeCanvas", w2, w2);
    let stId = setTimeout(this.getList, 60000);
    this.setData({
      timeoutId: stId
    });
    wx.hideLoading();
  },

  refreshQrcode() {
    clearTimeout(this.data.timeoutId);
    this.getList();
  },

  async checkResult() {
    const res = await request.post('ylf/gift/gift/payCheck?payCode='+this.data.cardData.payCode, {});
    const data = res.data.payCheck;
    // const data = {
    //   "balance": 10,
    //   "pay": true,
    //   "payAmount": 10
    // }
    if (data && data.pay) {
      this.gotoResult(data.balance, data.payAmount);
    }
  },

  gotoResult(balance, payAmount) {
    if (this.data.intervalId) {
      clearInterval(this.data.intervalId);
    }
    wx.redirectTo({
      url: '/pages/user/card/payment/paymentResult?balance=' + balance + '&payAmount=' + payAmount,
    });
  },

  async initData() {
    wx.showLoading({
      title: '加载中',
    })
    this.getList();
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      cardNo: options.cardNo
    }, () => {
      this.initData();
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {
    clearTimeout(this.data.timeoutId);
    clearInterval(this.data.intervalId);
  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
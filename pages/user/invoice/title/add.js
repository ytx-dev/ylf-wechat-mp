// pages/user/invoice/title/add.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    isFold: true,
    formData: {
      invoiceType: 0,
      headerType: 0
    }
  },

  onInvoiceTitleInput(e) {
    let formData = this.data.formData;
    formData.invoiceTitle = e.detail.value;
    this.setData({
      formData
    });
  },

  onTaxCodeInput(e) {
    let formData = this.data.formData;
    formData.taxCode = e.detail.value;
    this.setData({
      formData
    });
  },

  onCompanyAddressInput(e) {
    let formData = this.data.formData;
    formData.companyAddress = e.detail.value;
    this.setData({
      formData
    });
  },

  onTelephoneInput(e) {
    let formData = this.data.formData;
    formData.telephone = e.detail.value;
    this.setData({
      formData
    });
  },

  onBankInput(e) {
    let formData = this.data.formData;
    formData.bank = e.detail.value;
    this.setData({
      formData
    });
  },

  onBankNoInput(e) {
    let formData = this.data.formData;
    formData.bankNo = e.detail.value;
    this.setData({
      formData
    });
  },

  onTypeChange(e) {
    let type = Number(e.detail.value);
    let formData = this.data.formData;
    formData.headerType = type;
    this.setData({
      formData
    });
  },

  formSubmit() {
    let data = this.data.formData;
    if (this.checkForm(data)) {
      this.save();
    }
  },

  async save() {
    const res = await request.post('ylf/buyer/invoice/header', this.data.formData);
    if (res.succeed) {
      wx.navigateBack();
    } else {
      wx.showToast({
        icon: 'none',
        title: res.message,
      })
    }
  },

  checkForm(data) {
    let result = true;
    if (data.headerType) {
      if (!data.invoiceTitle) {
        result = false;
        wx.showToast({
          icon: 'none',
          title: '请填写发票抬头',
        })
      } else if (!data.taxCode) {
        result = false;
        wx.showToast({
          icon: 'none',
          title: '请填写单位税号',
        })
      } else if (!this.checkTax(data.taxCode)) {
        result = false;
        wx.showToast({
          icon: 'none',
          title: '单位税号为10到20位数字字母组合，请检查',
        })
      }
    } else {
      if (!data.invoiceTitle) {
        result = false;
        wx.showToast({
          icon: 'none',
          title: '请填写发票抬头',
        })
      }
    }
    return result;
  },

  checkTax(obj) {
    if (/^[a-zA-Z0-9]{10,20}$/.test(obj)) {
      return true;
    } else {
      return false;
    }
  },

  onBtnFoldTap() {
    this.setData({
      isFold: !this.data.isFold
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
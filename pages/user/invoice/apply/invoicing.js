// pages/user/invoice/apply/invoicing.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    isFold: true,
    formData: {
      invoiceType: 0,
      headerType: 0
    },
    HEADER_TYPE_TEXT: {
      0: '个人',
      1: '单位'
    },
    orderNo: ''
  },

  onInvoiceTitleInput(e) {
    let formData = this.data.formData;
    formData.invoiceTitle = e.detail.value;
    this.setData({
      formData
    });
  },

  onTaxCodeInput(e) {
    let formData = this.data.formData;
    formData.taxCode = e.detail.value;
    this.setData({
      formData
    });
  },

  onCompanyAddressInput(e) {
    let formData = this.data.formData;
    formData.companyAddress = e.detail.value;
    this.setData({
      formData
    });
  },

  onTelephoneInput(e) {
    let formData = this.data.formData;
    formData.telephone = e.detail.value;
    this.setData({
      formData
    });
  },

  onBankInput(e) {
    let formData = this.data.formData;
    formData.bank = e.detail.value;
    this.setData({
      formData
    });
  },

  onBankNoInput(e) {
    let formData = this.data.formData;
    formData.bankNo = e.detail.value;
    this.setData({
      formData
    });
  },

  onTypeChange(e) {
    let type = Number(e.detail.value);
    let formData = this.data.formData;
    formData.headerType = type;
    this.setData({
      formData
    });
  },

  checkForm(data) {
    let result = true;
    if (data.headerType) {
      if (!data.invoiceTitle) {
        result = false;
        wx.showToast({
          icon: 'none',
          title: '请填写发票抬头',
        })
      } else if (!data.taxCode) {
        result = false;
        wx.showToast({
          icon: 'none',
          title: '请填写单位税号',
        })
      }
    } else {
      if (!data.invoiceTitle) {
        result = false;
        wx.showToast({
          icon: 'none',
          title: '请填写发票抬头',
        })
      }
    }
    return result;
  },

  onBtnFoldTap() {
    this.setData({
      isFold: !this.data.isFold
    })
  },

  onBtnOkTap() {
    let data = this.data.formData;
    if (this.checkForm(data)) {
      this.save();
    }
  },

  async save() {
    const res = await request.post('ylf/buyer/invoice/header', this.data.formData);
    if (res.succeed && res.data.invoiceHeader) {
      if (this.data.orderNo) {
        this.apply(res.data.invoiceHeader);
      } else {
        this.backWithResult(res.data.invoiceHeader);
      }
    } else {
      wx.showToast({
        icon: 'none',
        title: res.message,
      })
    }

  },

  async apply(data) {
    const res = await request.post('ylf/trade/invoice/open?orderNo=' + this.data.orderNo + '&invoiceHeaderId=' + data.id);
    if (res.succeed) {
      wx.navigateBack();
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      });
    }
  },

  backWithResult(data) {
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    if(data) {
      // data.invoiceContext = '普票(商品明细-' + data.invoiceTitle + ')';
      data.invoiceContext = '电子普通发票-商品明细-' + this.data.HEADER_TYPE_TEXT[data.headerType];
    }
    prevPages.setData({
      invoice: data,
      isReload: false
    }, () => {
      wx.navigateBack();
    });
  },

  backWithOutResult() {
    this.backWithResult(null);
  },

  onBtnBackTap() {
    if (this.data.orderNo) {
      wx.navigateBack();
    } else {
      this.backWithOutResult();
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      orderNo: options.orderNo
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
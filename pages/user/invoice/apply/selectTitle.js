// pages/user/invoice/apply/selectTitle.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    id: 0,
    orderNo: '',
    titlePList: [],
    titleCList: [],
    currItem: {},
    HEADER_TYPE_TEXT: {
      0: '个人',
      1: '单位'
    }
  },

  onBtnOKTap() {
    if (this.data.currItem.id) {
      if (this.data.orderNo) {
        this.apply();
      } else {
        this.backWithResult(this.data.currItem);
      }
    } else {
      wx.showToast({
        title: '请选择发票抬头',
        icon: 'none'
      });
    }
  },

  async apply() {
    const res = await request.post('ylf/trade/invoice/open?orderNo=' + this.data.orderNo + '&invoiceHeaderId=' + this.data.currItem.id);
    if (res.succeed) {
      wx.navigateBack();
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      });
    }
  },

  backWithResult(data) {
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    if (data) {
      // data.invoiceContext = '普票(商品明细-' + data.invoiceTitle + ')';
      data.invoiceContext = '电子普通发票-商品明细-' + this.data.HEADER_TYPE_TEXT[data.headerType];
    }
    prevPages.setData({
      invoice: data,
      isReload: false
    }, () => {
      wx.navigateBack();
    });
  },

  backWithOutResult() {
    this.backWithResult(null);
  },

  onBtnBackTap() {
    if (this.data.orderNo) {
      wx.navigateBack();
    } else {
      this.backWithOutResult();
    }
  },

  onPItemSelected(e) {
    let data = e.currentTarget.dataset['item'];
    let titlePList = this.data.titlePList;
    for (const i in titlePList) {
      let item = titlePList[i];
      if (item.id == data.id) {
        item.selected = true;
      } else {
        item.selected = false;
      }
    }
    let titleCList = this.data.titleCList;
    for (const i in titleCList) {
      titleCList[i].selected = false;
    }
    this.setData({
      titlePList: titlePList,
      titleCList: titleCList,
      currItem: data
    });
  },

  onCItemSelected(e) {
    let data = e.currentTarget.dataset['item'];
    let titleCList = this.data.titleCList;
    for (const i in titleCList) {
      let item = titleCList[i];
      if (item.id == data.id) {
        item.selected = true;
      } else {
        item.selected = false;
      }
    }
    let titlePList = this.data.titlePList;
    for (const i in titlePList) {
      titlePList[i].selected = false;
    }
    this.setData({
      titlePList: titlePList,
      titleCList: titleCList,
      currItem: data
    });
  },

  onBtnAddTitleTap() {
    wx.navigateTo({
      url: '/pages/user/invoice/title/add',
    })
  },

  onBtnEditTitleTap(e) {
    let data = e.currentTarget.dataset['item'];
    wx.navigateTo({
      url: '/pages/user/invoice/title/edit?data=' + JSON.stringify(data),
    })
  },

  async initData() {
    const res = await request.get('ylf/buyer/invoice/header/list');
    let individualList = res.data.individualList ? res.data.individualList : [];
    let companyList = res.data.companyList ? res.data.companyList : [];
    if (this.data.id) {
      let flag = false;
      for (const i in individualList) {
        let item = individualList[i];
        if (item.id == this.data.id) {
          item.selected = true;
          flag = true;
          break;
        }
      }
      if (!flag) {
        for (const i in companyList) {
          let item = companyList[i];
          if (item.id == this.data.id) {
            item.selected = true;
            break;
          }
        }
      }
    }
    this.setData({
      currItem: {},
      titlePList: individualList,
      titleCList: companyList
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      orderNo: options.orderNo,
      id: options.id
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    this.initData();
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
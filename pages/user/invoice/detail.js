// pages/user/invoice/detail.js
const app = getApp();
const request = require('../../../utils/request.js');
const util = require('../../../utils/util.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    orderNo: '',
    invoiceData: {},
    STATUS_TEXT: {
      0: '不可开票',
      1: '未开票',
      2: '开票中',
      3: '已开票'
    },
    HEADER_TYPE_TEXT: {
      0: '个人',
      1: '单位'
    },
    ORDER_STATUS_TEXT: {
      '-1': '无效订单',
      '10': '等待支付',
      '20': '待发货',
      '30': '待收货',
      '50': '已关闭',
      '51': '已完成',
      '52': '已关闭',
    }
  },

  gotoViewInvoice() {
    if (this.data.invoiceData.invoice && this.data.invoiceData.invoice.pdfUrl) {
      wx.showLoading();
      wx.downloadFile({
        url: this.data.invoiceData.invoice.pdfUrl,
        success: function (res) {
          wx.shareFileMessage({
            filePath: res.tempFilePath,
            success() {
              wx.hideLoading();
            },
            fail: function(err){
              wx.hideLoading();
              wx.showToast({
                title: '发票转发失败',
                icon: 'none'
              })
            }
          })
        },
        fail: function (err) {
          wx.hideLoading();
          wx.showToast({
            title: '下载发票失败',
            icon: 'none'
          })
        }
      })
    }
  },

  async onBtnApplyTap() {
    if (this.data.invoiceData.hasInvoiceHeader) {
      wx.navigateTo({
        url: '/pages/user/invoice/apply/selectTitle?orderNo=' + this.data.orderNo
      })
    } else {
      wx.navigateTo({
        url: '/pages/user/invoice/apply/invoicing?orderNo=' + this.data.orderNo
      })
    }
  },

  async initData(orderNo) {
    const no = orderNo ? orderNo : this.data.orderNo;
    const res = await request.get('ylf/buyer/invoice/' + no);
    let item = res.data.result;
    let count = 0;
    for (const cc in item.orderItemList) {
      count += item.orderItemList[cc].number;
    }
    item.count = count;
    item.createdTime = util.formatTime(new Date(item.createdAt));
    if (item.invoice) {
      if (item.invoice.createdAt) {
        item.invoice.createdTime = util.formatTime(new Date(item.invoice.createdAt));
      }
      if (item.invoice.invoiceAt) {
        item.invoice.invoiceTime = util.formatTime2(new Date(item.invoice.invoiceAt), '-');
      }
    }
    if (item.invoiceHeader) {
      if (item.invoiceHeader.bank == 'null') {
        item.invoiceHeader.bank = '';
      }
      if (item.invoiceHeader.bankNo == 'null') {
        item.invoiceHeader.bankNo = '';
      }
      if (item.invoiceHeader.telephone == 'null') {
        item.invoiceHeader.telephone = '';
      }
      if (item.invoiceHeader.companyAddress == 'null') {
        item.invoiceHeader.companyAddress = '';
      }
    }

    this.setData({
      orderNo: no,
      invoiceData: item
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.initData(options.orderNo);
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    if (this.data.orderNo) {
      this.initData();
    }
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
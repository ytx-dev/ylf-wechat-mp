<!--pages/user/invoice/detail.wxml-->
<wxs module="numberFilter" src="../../../utils/numberFilter.wxs"></wxs>
<view class="page-invoice-detail">
  <view class="page-invoice-detail-banner">
    <view class="banner-title">{{ STATUS_TEXT[invoiceData.invoiceType] }}</view>
    <view wx:if="{{ invoiceData.invoiceType == 0 }}" class="banner-subtitle">该订单无实际支付额,不可开具发票</view>
    <view wx:if="{{ invoiceData.invoiceType == 1 }}" class="banner-subtitle">预计可开发票金额：¥{{ numberFilter.numberToFixed(invoiceData.payAmount) }}</view>
    <view wx:if="{{ invoiceData.invoiceType > 1 }}" class="banner-subtitle">预计24小时内完成开票</view>
  </view>
  <view wx:if="{{ invoiceData.invoiceType > 0 }}" class="invoice-card">
    <view class="invoice-steps">
      <view class="invoice-steps-item step-done">
        <image lazy-load src="https://g3.ytx.com/ylf_mp/inlife-invoice-detail-step-done.png" />
        <view class="invoice-steps-item-title">订单提交</view>
      </view>
      <view class="invoice-steps-item step-done">
        <image lazy-load src="https://g3.ytx.com/ylf_mp/inlife-invoice-detail-step-done.png" />
        <view class="invoice-steps-item-title">订单完成</view>
        <div class="step-line-start"></div>
      </view>
      <view class="invoice-steps-item {{ invoiceData.invoiceType == 3 ? 'step-done' : 'step-undo' }}">
        <image lazy-load src="https://g3.ytx.com/ylf_mp/inlife-invoice-detail-step-{{ invoiceData.invoiceType == 3 ? 'done' : 'undo' }}.png" />
        <view class="invoice-steps-item-title">开票完成</view>
        <div class="step-line-end"></div>
      </view>
    </view>
  </view>
  <view wx:if="{{ invoiceData.invoiceType > 1 }}" class="invoice-card">
    <view class="order-row">
      <view class="order-row-label">发票类型：</view>
      <view class="order-row-value">电子普通发票</view>
    </view>
    <view class="order-row">
      <view class="order-row-label">发票内容：</view>
      <view class="order-row-value">商品明细</view>
    </view>
    <view class="order-row">
      <view class="order-row-label">抬头类型：</view>
      <view class="order-row-value">{{ invoiceData.invoiceHeader ? HEADER_TYPE_TEXT[invoiceData.invoiceHeader.headerType] : '' }}</view>
    </view>
    <view class="order-row">
      <view class="order-row-label">开票金额：</view>
      <view class="order-row-value">¥{{ invoiceData.invoice && invoiceData.invoice.totalAmount ? numberFilter.numberToFixed(invoiceData.invoice.totalAmount) : numberFilter.numberToFixed(invoiceData.payAmount) }}</view>
    </view>
    <view class="order-row">
      <view class="order-row-label">抬头名称：</view>
      <view class="order-row-value">{{ invoiceData.invoiceHeader ? invoiceData.invoiceHeader.invoiceTitle : '' }}</view>
    </view>
    <view wx:if="{{ invoiceData.invoiceHeader && invoiceData.invoiceHeader.headerType == 1 }}" class="order-row">
      <view class="order-row-label">单位税号：</view>
      <view class="order-row-value">{{ invoiceData.invoiceHeader ? invoiceData.invoiceHeader.taxCode : '' }}</view>
    </view>
    <view wx:if="{{ invoiceData.invoiceHeader && invoiceData.invoiceHeader.headerType == 1 && invoiceData.invoiceHeader.bank }}" class="order-row">
      <view class="order-row-label">开户银行：</view>
      <view class="order-row-value">{{ invoiceData.invoiceHeader ? invoiceData.invoiceHeader.bank : '' }}</view>
    </view>
    <view wx:if="{{ invoiceData.invoiceHeader && invoiceData.invoiceHeader.headerType == 1 && invoiceData.invoiceHeader.bankNo }}" class="order-row">
      <view class="order-row-label">银行账号：</view>
      <view class="order-row-value">{{ invoiceData.invoiceHeader ? invoiceData.invoiceHeader.bankNo : '' }}</view>
    </view>
    <view wx:if="{{ invoiceData.invoiceHeader && invoiceData.invoiceHeader.headerType == 1 && invoiceData.invoiceHeader.telephone }}" class="order-row">
      <view class="order-row-label">注册电话：</view>
      <view class="order-row-value">{{ invoiceData.invoiceHeader ? invoiceData.invoiceHeader.telephone : '' }}</view>
    </view>
    <view wx:if="{{ invoiceData.invoiceHeader && invoiceData.invoiceHeader.headerType == 1 && invoiceData.invoiceHeader.companyAddress }}" class="order-row">
      <view class="order-row-label">注册地址：</view>
      <view class="order-row-value">{{ invoiceData.invoiceHeader ? invoiceData.invoiceHeader.companyAddress : '' }}</view>
    </view>
    <view class="order-row">
      <view class="order-row-label">申请时间：</view>
      <view class="order-row-value">{{ invoiceData.invoice ? invoiceData.invoice.createdTime : '' }}</view>
    </view>
    <view class="order-row">
      <view class="order-row-label">开票时间：</view>
      <view class="order-row-value">{{ invoiceData.invoice && invoiceData.invoice.status > 0 ? invoiceData.invoice.invoiceTime : '' }}</view>
    </view>
    <view wx:if="{{ invoiceData.invoice && invoiceData.invoice.pdfUrl }}" class="invoice-thumb" catchtap="gotoViewInvoice">
      <image lazy-load src="https://g3.ytx.com/ylf_mp/inlife-invoice-thumb.png" />
      <view class="invoice-thumb-text">点击转发发票</view>
    </view>
  </view>
  <view class="invoice-card">
    <view class="order-item">
      <view class="order-item-header">
        <view class="item-header-title">
          <image lazy-load src="https://g3.ytx.com/ylf_mp/inlife-logo.png" />
          <view>{{ invoiceData.sellerName }}</view>
        </view>
        <view class="item-header-amount"><text>¥</text>{{numberFilter.numberToFixed(invoiceData.payAmount)}}</view>
      </view>
      <view class="order-good-list" wx:if="{{ invoiceData.orderItemList && invoiceData.orderItemList.length > 1 }}">
        <scroll-view class="order-good-list-scroll" scroll-x="true">
          <image lazy-load wx:for="{{ invoiceData.orderItemList }}" wx:for-index="orderImgIndex" wx:for-item="orderImgItem" wx:key="id" src="{{ imageDomain + orderImgItem.iconImageKey }}?x-oss-process=image/resize,m_lfit,h_345,w_345" />
        </scroll-view>
        <view>共{{ invoiceData.count }}件</view>
      </view>
      <view wx:if="{{ invoiceData.orderItemList && invoiceData.orderItemList.length < 2 }}" class="invoice-good-item">
        <image lazy-load src="{{ imageDomain + invoiceData.orderItemList[0].iconImageKey }}?x-oss-process=image/resize,m_lfit,h_345,w_345" />
        <view>{{ invoiceData.orderItemList[0].itemName }}</view>
      </view>
      <view class="order-row">
        <view class="order-row-label">订单状态：</view>
        <view class="order-row-value">已完成</view>
      </view>
      <view class="order-row">
        <view class="order-row-label">订单编号：</view>
        <view class="order-row-value">{{ invoiceData.orderNo }}</view>
      </view>
      <view class="order-row">
        <view class="order-row-label">下单时间：</view>
        <view class="order-row-value">{{ invoiceData.createdTime }}</view>
      </view>
    </view>
  </view>
  <view wx:if="{{ invoiceData.invoiceType == 1 }}" class="page-invoice-detail-btn-row">
    <view class="btn-apply" catchtap="onBtnApplyTap">申请开票</view>
  </view>
</view>
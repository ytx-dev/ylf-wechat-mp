// pages/user/invoice/list.js
const app = getApp();
const request = require('../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    isInit: true,
    imageDomain: app.globalData.imageDomain,
    active: 0,
    invoiceList: [],
    INVOICE_TYPE_TEXT: {
      0: '不可开票',
      1: '未开票',
      2: '开票中',
      3: '已开票'
    },
    pageNum: 1,
    hasNextPage: false,
    titlePList: [],
    titleCList: [],
  },

  onTabItemTap(e) {
    let index = e.currentTarget.dataset.index;
    this.setData({
      active: index,
      invoiceList: [],
      titlePList: [],
      titleCList: [],
      pageNum: 1
    }, () => {
      this.initData();
    });
  },

  onListLower(e) {
    if (this.data.hasNextPage) {
      this.setData({
        pageNum: this.data.pageNum + 1,
      }, () => {
        this.getInvoiceList();
      })
    }
  },

  onBtnAddTitleTap() {
    wx.navigateTo({
      url: '/pages/user/invoice/title/add',
    })
  },

  onBtnEditTitleTap(e) {
    let data = e.currentTarget.dataset['item'];
    wx.navigateTo({
      url: '/pages/user/invoice/title/edit?data=' + JSON.stringify(data),
    })
  },

  gotoDetail(e) {
    const item = e.currentTarget.dataset['item'];
    wx.navigateTo({
      url: '/pages/user/invoice/detail?orderNo=' + item.orderNo,
    })
  },

  async getInvoiceList() {
    const res = await request.get('ylf/buyer/invoice/list', {
      pageNum: this.data.pageNum,
      type: this.data.active
    });
    let list = res.data.page.list;
    for (const i in list) {
      let item = list[i];
      let count = 0;
      for (const cc in item.orderItemList) {
        count += item.orderItemList[cc].number;
      }
      item.count = count;
    }
    this.setData({
      isInit: false,
      invoiceList: this.data.invoiceList.concat(res.data.page.list),
      hasNextPage: res.data.page.pageNum < res.data.page.pages
    })
  },

  async getTitleList() {
    const res = await request.get('ylf/buyer/invoice/header/list');
    this.setData({
      isInit: false,
      titlePList: res.data && res.data.individualList ? res.data.individualList : [],
      titleCList: res.data && res.data.companyList ? res.data.companyList : []
    })
  },

  initData() {
    if (this.data.active < 3) {
      this.getInvoiceList();
    } else {
      this.getTitleList();
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.initData();
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    if (!this.data.isInit) {
      this.setData({
        invoiceList: []
      }, () => {
        this.initData();
      });
    }
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
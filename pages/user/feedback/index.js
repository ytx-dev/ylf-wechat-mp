// pages/user/feedback/index.js
const app = getApp();
const request = require('../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    imageList: [],
    form: {
      desc: '',
      phone: ''
    }
  },

  bindDescInput(e) {
    this.setData({
      form: {
        desc: e.detail.value,
        phone: this.data.form.phone
      }
    })
  },
  bindPhoneInput(e) {
    this.setData({
      form: {
        desc: this.data.form.desc,
        phone: e.detail.value
      }
    })
  },

  onImageTap(e) {
    const list = [];
    for (const i in this.data.imageList) {
      list.push(imageDomain + this.data.imageList[i]);
    }
    wx.previewImage({
      current: imageDomain + e.currentTarget.dataset.url,
      urls: list
    })
  },

  onImageDelTap(e) {
    const index = e.currentTarget.dataset.index;
    const list = this.data.imageList;
    list.splice(index, 1);
    this.setData({
      imageList: list
    })
    this.setData({
      imageList: list
    });
  },

  btnAddImage(e) {
    const _this = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      success(res) {
        if (res.tempFilePaths && res.tempFilePaths.length > 0) {
          wx.showLoading({
            title: '上传中',
          })
          _this.uploadImage(res.tempFilePaths[0]);
        }
      }
    })
  },

  async uploadImage(path) {
    const fs = wx.getFileSystemManager();
    try {
      const base64 = fs.readFileSync(path, 'base64');
      const res = await request.post('ylf/buyer/buyer/imgs', base64)
      if (res.result && res.path) {
        let list = this.data.imageList;
        list.push(res.path);
        this.setData({
          imageList: list
        })
      }
    } catch (e) {}
    wx.hideLoading();
  },

  formSubmit(e) {
    if (this.data.form && this.data.form.desc) {
      this.onSave();
    } else {
      wx.showToast({
        title: '请输问题描述',
        icon: 'none',
        duration: 2000
      })
    }
  },

  async onSave() {
    let query = 'contents=' + this.data.form.desc;
    if (this.data.imageList) {
      let str = '';
      for (const i in this.data.imageList) {
        if (str) {
          str += ',';
        }
        str += this.data.imageList[i]
      }
      if (str) {
        query += '&images=' + str;
      }
    }
    if (this.data.form.phone) {
      query += '&contract=' + this.data.form.phone
    }
    const res = await request.post('ylf/buyer/buyer/feedback?' + query, {});
    if (res.succeed) {
      wx.showToast({
        title: '提交成功',
        icon: 'none',
        duration: 2000,
        complete: () => {
          setTimeout(() => {
            wx.navigateBack()
          }, 1500);
        }
      })
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none',
        duration: 2000
      })
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      imageList: [],
      form: {
        desc: '',
        phone: ''
      }
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    this.setData({
      form: {
        desc: '',
        images: [],
        phone: ''
      }
    })
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
// pages/user/address/list/list.js
const request = require('../../../../utils/request.js');
const util = require('../../../../utils/util.js');
const app = getApp();

Page({

  /**
   * Page initial data
   */
  data: {
    addressId: 0,
    addressList: [],
    addressIndex: 0,
    toastText: '',
    currData: null
  },

  onItemSelect(e) {
    const index = e.currentTarget.dataset.index;
    const item = this.data.addressList[index];
    this.setSelect(item);
  },

  setSelect(item) {
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    prevPages.setData({
      addressId: item.id,
      isReload: true,
      userInfoSet: true
    }, () => {
      wx.navigateBack();
    });
  },

  onBtnAddTap(e) {
    util.navigation_to('/pages/user/address/edit/index', 1);
  },

  onBtnEditTap(e) {
    const item = e.currentTarget.dataset.item;
    this.setData({
      currData: item
    });
    util.navigation_to('/pages/user/address/edit/index?id=' + item.id, 1);
  },

  async initData() {
    const res = await this.getList();
    let index = 0;
    if (this.data.addressId) {
      for (const i in res.addressList) {
        if (res.addressList[i].id + '' === this.data.addressId) {
          index = i;
          break;
        }
      }
    }
    this.setData({
      addressList: res.addressList,
      addressIndex: index
    });
  },

  async getList() {
    const res = await request.get('ylf/buyer/buyer/address/list');
    return res.data;
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      addressId: options.addressId
    }, () => {
      this.initData(options);
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    // if (this.data.currData && this.data.currData.id) {
    //     this.setSelect(this.data.currData);
    // }
    this.initData();
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
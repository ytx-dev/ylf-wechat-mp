// pages/user/address/edit/map.js
const amap = require('../../../../utils/amap.js');
const address = require('../../../../utils/address.js');
const request = require('../../../../utils/request.js');
const app = getApp();

Page({

  /**
   * Page initial data
   */
  data: {
    keywords: '',
    searchVals: [],
    isSearching: false,
    latitude: '',
    longitude: '',
    cityCode: null,
    pois: [],
    shopList: [],
    markers: [],
    polygons: [],
    currentLocation: {}
  },

  onFocus(e) {
    this.setData({
      isSearching: true
    });
  },

  onCancelTap() {
    this.setData({
      isSearching: false,
      searchVals: ''
    });
  },

  onInput(e) {
    const keyword = e.detail.value;
    this.setData({
      keywords: keyword
    });
    this.getTips(keyword);
  },

  async getTips(keyword) {
    const res = await amap.inputtips(keyword, this.data.cityCode);
    this.setData({
      searchVals: res
    });
  },

  onTipsTap(e) {
    const points = e.currentTarget.dataset['location'].split(',');
    this.updateData(points[1], points[0]);
  },

  async onPoiTap(e) {
    const item = e.currentTarget.dataset['item'];
    const points = item.location.split(',');
    let result = {
      ...this.data.currentLocation,
      sellerAccountId: app.globalData.sellerShop.accountId,
      latitude: points[1],
      longitude: points[0],
      address: item.address,
      poiName: item.name
    }
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    prevPages.setData({
      formData: {
        ...prevPages.data.formData,
        ...result
      }
    }, () => {
      wx.navigateBack();
    });
  },

  async onPoiTap_b1(e) {
    const item = e.currentTarget.dataset['item'];
    const points = item.location.split(',');
    const res = await address.checkAddr(this.data.cityCode, points[1], points[0]);
    if (res.isCreateAddress && res.sellerAccountId) {
      let result = {
        ...this.data.currentLocation,
        sellerAccountId: res.sellerAccountId,
        latitude: points[1],
        longitude: points[0],
        address: item.address,
        poiName: item.name
      }
      const pages = getCurrentPages();
      const prevPages = pages[pages.length - 2];
      prevPages.setData({
        formData: {
          ...prevPages.data.formData,
          ...result
        }
      }, () => {
        wx.navigateBack();
      });
    } else {
      wx.showToast({
        icon: 'error',
        title: '地址不在配送范围内',
      })
    }
  },

  onRegChange(e) {
    if (e.type === 'end' && e.causedBy === 'drag') {
      this.updateData(e.detail.centerLocation.latitude, e.detail.centerLocation.longitude);
    }
  },

  async getShops(cityCode) {
    const res = request.get('ylf/buyer/buyer/address/sellershop/list', {
      cityCode
    });
    return res;
  },

  async updateData(latitude, longitude) {
    let shops = [];
    let markers = [];
    let polygons = [];

    markers.push({
      id: 1,
      latitude: latitude,
      longitude: longitude,
      iconPath: 'https://g3.ytx.com/ylf_mp_2/icon-map-marker.png',
      width: 24,
      height: 24
    });

    const location = longitude + ',' + latitude;
    const res = await address.getPoisByAmap(location);
    if (res && res.cityCode) {
      const shopData = await this.getShops(res.cityCode);
      if (shopData.data && shopData.data.distributionScopeList) {
        shops = shopData.data.distributionScopeList;
        for (const i in shops) {
          const item = shops[i];
          markers.push({
            id: item.sellerAccountId,
            latitude: item.latitude,
            longitude: item.longitude,
            iconPath: 'https://g3.ytx.com/ylf_mp_2/icon-map-shop.png',
            width: 24,
            height: 39
          });
          const polygonPoints = [];
          const array = item.areas.split(',');
          for (const si in array) {
            const locations = array[si].split(' ');
            polygonPoints.push({
              longitude: locations[0],
              latitude: locations[1]
            });
          }
          polygons.push({
            points: polygonPoints,
            fillColor: '#00B86133',
            strokeColor: '#00B861'
          });
        }
      } else {
        wx.showToast({
          icon: 'error',
          title: '该城市没有门店',
        })
      }
    }
    this.setData({
      isSearching: false,
      longitude: longitude,
      latitude: latitude,
      cityCode: res ? res.cityCode : null,
      pois: res ? res.pois : [],
      shopList: shops,
      markers: markers,
      polygons: polygons,
      currentLocation: {
        country: '中国',
        countryCode: '86',
        province: res ? res.regeocode.addressComponent.province : '',
        provinceCode: res ? res.regeocode.addressComponent.adcode : '',
        city: res ? res.regeocode.addressComponent.city : '',
        cityCode: res ? res.cityCode : '',
        area: res ? res.regeocode.addressComponent.district : '',
        areaCode: res ? res.regeocode.addressComponent.adcode : '',
      }
    });
  },

  async initData() {
    let points = await address.getCoordinate();
    if (!points || !points.latitude) {
      points = {
        latitude: 34.279823,
        longitude: 108.987877
      }
    }
    this.updateData(points.latitude, points.longitude);
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.initData();
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
// pages/user/address/edit/index.js
const app = getApp();
const request = require('../../../../utils/request.js');
const util = require('../../../../utils/util.js');

Page({

  /**
   * Page initial data
   */
  data: {
    formData: {},
    addressType: ''
  },

  onDelTap() {
    const _this = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除地址吗?',
      success(res) {
        if (res.confirm) {
          _this.delAddr();
        }
      }
    });
  },

  async delAddr() {
    const res = await request.del('ylf/buyer/buyer/address/delete?addressId=' + this.data.formData.id);
    if (res.succeed) {
      wx.navigateBack();
    }
  },

  async formSubmit() {
    let data = this.data.formData;
    if (this.checkForm(data)) {
      wx.showLoading();
      const res = await request.post('ylf/buyer/buyer/address/save', data);
      wx.hideLoading();
      if (res.succeed) {
        this.saveResult(res.data.address);
      } else {
        wx.showToast({
          icon: 'none',
          duration: 3000,
          title: res.message,
        })
      }
    }
  },

  checkForm(data) {
    let result = true;
    if (!data.sellerAccountId) {
      result = false;
      wx.showToast({
        icon: 'none',
        duration: 3000,
        title: '无门店信息，请重新选择地址',
      })
    } else if (!data.doorNumber) {
      result = false;
      wx.showToast({
        icon: 'none',
        duration: 3000,
        title: '请输入门牌号',
      })
    } else if (!data.consignee) {
      result = false;
      wx.showToast({
        icon: 'none',
        duration: 3000,
        title: '请输入收件人',
      })
    } else if (data.consignee.length < 1 || data.consignee.length > 15) {
      result = false;
      wx.showToast({
        icon: 'none',
        duration: 3000,
        title: '请输入1-15字之间的收件人姓名',
      })
    } else if (!data.phone || data.phone.length != 11) {
      result = false;
      wx.showToast({
        icon: 'none',
        duration: 3000,
        title: '请输入正确格式的11位手机号码',
      })
    }
    return result;
  },

  saveResult(item) {
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    if (this.data.addressType === 'no') {
      prevPages.setData({
        addressId: item.id,
        userInfoSet: true,
        isReload: true,
      }, () => {
        wx.navigateBack();
      });
    } else {
      prevPages.setData({
        currData: item
      }, () => {
        wx.navigateBack();
      });
    }
  },

  onDoorNumberInput(e) {
    let formData = this.data.formData;
    formData.doorNumber = e.detail.value;
    this.setData({
      formData
    });
  },

  onConsigneeInput(e) {
    let formData = this.data.formData;
    formData.consignee = e.detail.value;
    this.setData({
      formData
    });
  },

  onPhoneInput(e) {
    let formData = this.data.formData;
    formData.mobile = e.detail.value;
    formData.phone = e.detail.value;
    this.setData({
      formData
    });
  },

  onSelectAddrTap() {
    util.navigation_to('/pages/user/address/edit/map', 1);
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    let title = '添加收货地址';
    if (options.id) {
      title = '编辑收货地址';
      const pages = getCurrentPages();
      const prevPages = pages[pages.length - 2];
      this.setData({
        formData: prevPages.data.currData
      });
    }
    wx.setNavigationBarTitle({
      title
    });
    if (options.addressType) {
      this.setData({
        addressType: options.addressType
      });
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
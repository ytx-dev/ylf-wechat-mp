// pages/user/address/mgtList/index.js
const request = require('../../../../utils/request.js');
const util = require('../../../../utils/util.js');
const app = getApp();

Page({

  /**
   * Page initial data
   */
  data: {
    addressList: [],
    currData: null
  },

  onBtnAddTap(e) {
    util.navigation_to('/pages/user/address/edit/index', 1);
  },

  onBtnEditTap(e) {
    const item = e.currentTarget.dataset.item;
    this.setData({
      currData: item
    });
    util.navigation_to('/pages/user/address/edit/index?id=' + item.id, 1);
  },

  onBtnDelTap(e) {
    const item = e.currentTarget.dataset.item;
    const _this = this;
    wx.showModal({
      title: '提示',
      content: '确定要删除地址吗?',
      success(res) {
        if (res.confirm) {
          _this.delAddr(item);
        }
      }
    });
  },

  async delAddr(data) {
    const res = await request.del('ylf/buyer/buyer/address/delete?addressId=' + data.id);
    if (res.succeed) {
      let list = [];
      for (const i in this.data.addressList) {
        const item = this.data.addressList[i];
        if (item.id != data.id) {
          list.push(item);
        }
      }
      this.setData({
        addressList: list
      });
    }
  },

  async initData() {
    const res = await this.getList();
    this.setData({
      addressList: res.addressList
    });
  },

  async getList() {
    const res = await request.get('ylf/buyer/buyer/address/list');
    return res.data;
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    this.initData();
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
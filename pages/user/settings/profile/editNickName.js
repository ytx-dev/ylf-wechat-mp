// pages/user/settings/profile/editNickName.js
const app = getApp();
const request = require('../../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    formData: {}
  },

  bindPhoneInput(e) {
    this.setData({
      formData: {
        nickName: e.detail.value
      }
    })
  },

  formSubmit() {
    if (this.data.formData.nickName) {
      this.updateProfile({
        nickName: this.data.formData.nickName
      });
    } else {
      wx.showToast({
        title: '请填写昵称',
        icon: 'none'
      })
    }
  },

  async updateProfile(data) {
    let res = await request.get('ylf/buyer/buyer/profile', data);
    if (res && res.succeed) {
      wx.navigateBack();
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      })
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    let nickName = options.nickName;
    this.setData({
      formData: {
        nickName: nickName
      }
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
// pages/user/settings/profile/index.js
const app = getApp();
const request = require('../../../../utils/request.js');
const util = require('../../../../utils/util');
Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    profileData: {},
    isShowSex: false,
    selectSex: null
  },

  onBtnAvatarTap() {
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      success(res) {
        if (res.tempFilePaths && res.tempFilePaths.length > 0) {
          this.uploadAvatar(res.tempFilePaths[0]);
        }
      }
    })
  },

  onEditNick() {
    wx.navigateTo({
      url: '/pages/user/settings/profile/editNickName?nickName=' + this.data.profileData.nickName,
    })
  },

  bindDateChange(e) {
    let profileData = this.data.profileData;
    profileData.birthday = e.detail.value;
    this.setData({
      profileData: profileData
    }, () => {
      this.updateProfile({
        birthday: profileData.birthday
      });
    })
  },

  onEditBSexTap() {
    this.setData({
      selectSex: this.data.profileData.sex != null ? this.data.profileData.sex : 1,
      isShowSex: true
    })
  },

  setSelectSex(e) {
    this.setData({
      selectSex: e.currentTarget.dataset.value
    })
  },

  onSelectSexCancel() {
    this.setData({
      selectSex: null,
      isShowSex: false
    })
  },

  onSelectSexResult() {
    let profileData = this.data.profileData;
    profileData.sex = this.data.selectSex;
    this.setData({
      profileData: profileData
    }, () => {
      this.updateProfile({
        sex: profileData.sex
      });
      this.onSelectSexCancel();
    })
  },

  gotoAddr() {
    wx.navigateTo({
      url: '/pages/user/address/mgtList/index',
    })
  },

  async initData() {
    let res = await request.get('ylf/buyer/buyer');
    let url = (app.globalData.userInfo && app.globalData.userInfo.avatarUrl) ? app.globalData.userInfo.avatarUrl : '';
    if (!res.headImg && url) {
      this.initAvatar();
    }
    if (res.birthday) {
      res.birthdayStr = util.formatTime2(new Date(res.birthday));
    }
    if (res.sex != null) {
      res.sexStr = res.sex > 0 ? '男' : '女';
    }
    this.setData({
      profileData: res
    });
  },

  async initAvatar() {
    const _this = this;
    let url = (app.globalData.userInfo && app.globalData.userInfo.avatarUrl) ? app.globalData.userInfo.avatarUrl : '';
    if (url) {
      wx.downloadFile({
        url: url,
        success(res) {
          if (res.statusCode === 200) {
            _this.uploadAvatar(res.tempFilePath);
          }
        }
      })
    }
  },

  async uploadAvatar(path) {
    wx.uploadFile({
      filePath: path,
      name: 'file',
      url: app.globalData.apiDomain + 'ylf/buyer/buyer/img/update',
      header: {
        'cookie': 'remember-me=' + app.globalData.rememberMe
      },
      success(res) {
        this.initData();
      }
    })
  },

  async updateProfile(data) {
    let res = await request.get('ylf/buyer/buyer/profile', data);
    if (res && res.succeed) {
      this.initData();
    }
  },

  async getImageTobase64_url(tempFilePath) {
    const base64 = await new Promise(resolve => {
      wx.request({
        url: tempFilePath,
        responseType: 'arraybuffer', //最关键的参数，设置返回的数据格式为arraybuffer
        success: res => {
          //把arraybuffer转成base64
          let data = wx.arrayBufferToBase64(res.data);
          return resolve(data); //('data:image/jpeg;base64,' + data)
        }
      });
    });
    return base64;
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {
    this.initData();
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
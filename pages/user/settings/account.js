// pages/user/settings/account.js
const app = getApp();

Page({

  /**
   * Page initial data
   */
  data: {
    phone: ''
  },

  gotoEditPhone() {
    wx.navigateTo({
      url: '/pages/user/settings/editPhone',
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    let phone = app.globalData.account ? app.globalData.account.mobile : '';
    if (phone) {
      phone = phone.substring(0, 3) + '****' + phone.substring(7);
    }
    this.setData({
      phone: phone
    });
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
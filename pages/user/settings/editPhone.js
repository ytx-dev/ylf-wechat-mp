// pages/user/settings/editPhone.js
const app = getApp();
const request = require('../../../utils/request.js');

// http://api.ytx5.com/buyer/sms/send?captcha=&mobile=18706775901&captchaType=1&type=212

/**
 {
   result: false,
   needCaptcha: true
 }
 */

// http://api.ytx5.com/buyer/sms/send?captcha=CN31_4f.-rMGWZMOTqhBftYCzkGV9nwHmwi5uQhzF8x8OaUcq20XRSNCGpxryyp5UU78pKm5p2-xix0Biq8VB1Nd6P06LOdl0OLLtUlHjfLBpeDyhYpqKinCTIxr-ZtwzS5QesCcIpVkFjvi6cJkmTWS6s8NPFfgJL8PH1-88Jt255VmAnYtKGKjElO5lFbrpTM9atJSofiMyoGYe.l-GBtPnt0vodhvT9pEXnrdmzn68rn2_VPaMtrPpFWhayGyXUTP2NUBhdYzinoexaYG6iZjK-o6ZAiozfr._.bxAkKMiNWF0lY-2suB0dtGsw8M0hZNHKDr6NKJ7YBEEHbxlNc6jI882pN8VvqhtezyKuVIAX_hRVrlA6rnkijndJUSx_rKOGz4cUWwLUZWX6IxZrwWpndnZSGskwRtRjUlSHjuWGVritR.kGxXDQ8hrYhVHEBGPq--z.0nJqHs_5H8M9RBO-mg-HSUmh2URddAVevsj.TffDq90tHVsXI0nmcI3&mobile=18706775901&captchaType=1&type=212

// { result: true }

// http://api.ytx5.com/buyer/sms/authorize?captcha=CN31_4f.-rMGWZMOTqhBftYCzkGV9nwHmwi5uQhzF8x8OaUcq20XRSNCGpxryyp5UU78pKm5p2-xix0Biq8VB1Nd6P06LOdl0OLLtUlHjfLBpeDyhYpqKinCTIxr-ZtwzS5QesCcIpVkFjvi6cJkmTWS6s8NPFfgJL8PH1-88Jt255VmAnYtKGKjElO5lFbrpTM9atJSofiMyoGYe.l-GBtPnt0vodhvT9pEXnrdmzn68rn2_VPaMtrPpFWhayGyXUTP2NUBhdYzinoexaYG6iZjK-o6ZAiozfr._.bxAkKMiNWF0lY-2suB0dtGsw8M0hZNHKDr6NKJ7YBEEHbxlNc6jI882pN8VvqhtezyKuVIAX_hRVrlA6rnkijndJUSx_rKOGz4cUWwLUZWX6IxZrwWpndnZSGskwRtRjUlSHjuWGVritR.kGxXDQ8hrYhVHEBGPq--z.0nJqHs_5H8M9RBO-mg-HSUmh2URddAVevsj.TffDq90tHVsXI0nmcI3&captchaType=1&activation=578811&type=212

Page({

  /**
   * Page initial data
   */
  data: {
    step: 1,
    oldMobile: '',
    mobile: '',
    captcha: '',
    captchaData: {
      captchaId: '',
      captchaType: ''
    },
    timer: 0,

    Length: 6, //输入框个数 
    isFocus: true, //聚焦 
    Value: "", //输入的内容 
    ispassword: false, //是否密文显示 true为密文， false为明文。 
  },

  handlerVerify(e) {
    if (!e.detail[0]) {
      this.setData({
        captcha: e.detail[1]
      }, () => {
        this.getCode();
      })
    }
  },

  async getCode() {
    if (this.data.timer < 1) {
      let res = await this.sendSMS();
      if (res.result) {
        this.runTime(60);
        this.setData({
          step: this.data.step == 1 ? 2 : 4
        })
      } else if (res.needCaptcha) {
        this.setData({
          Value: '',
          captcha: ''
        });
        this.selectComponent('#captcha').reset();
        this.selectComponent('#captcha').popup();
      } else {
        wx.showToast({
          title: res.error,
          icon: 'none'
        })
      }
    }
  },

  async checkPhone() {
    let url = 'ylf/buyer/sms/authorize?captcha=' + this.data.captcha + '&mobile=' + (this.data.step == 1 ? this.data.oldMobile : this.data.mobile) + '&activation=' + this.data.Value + '&captchaType=1&type=212';
    let res = await request.post(url);
    if (res.result) {
      this.setData({
        step: 3,
        timer: 0,
        Value: '',
        captcha: ''
      });
      this.selectComponent('#captcha').reset()
    } else {
      wx.showToast({
        title: res.error,
        icon: 'none'
      })
    }
  },

  async updatePhone() {
    let url = 'ylf/buyer/mobile/update_by_origin?captcha=' + this.data.captcha + '&mobile=' + this.data.mobile + '&activation=' + this.data.Value + '&captchaType=1&type=212';
    let res = await request.post(url);
    if (res.result) {
      wx.showToast({
        title: '手机号修改成功',
        icon: 'none'
      });
      let account = app.globalData.account;
      if (!account) {
        account = {};
      }
      account.mobile = this.data.mobile;
      app.globalData.account = account;
      wx.setStorageSync('account', JSON.stringify(app.globalData.account));
      wx.navigateBack();
    } else {
      wx.showToast({
        title: res.error,
        icon: 'none'
      })
    }
  },

  onBtnStep3Tap() {
    if(this.data.mobile && this.data.mobile.length == 11) {
      this.getCode();
    } else {
      wx.showToast({
        title: '请填写新手机号码',
        icon: 'none'
      })
    }
  },

  setMobile(e) {
    this.setData({
      mobile: e.detail.value
    });
  },

  cleanPhone() {
    this.setData({
      mobile: ''
    })
  },

  Focus(e) {
    var that = this;
    var inputValue = e.detail.value;
    that.setData({
      Value: inputValue,
    })
  },
  Tap() {
    var that = this;
    that.setData({
      isFocus: true,
    })
  },

  runTime() {
    let time = this.data.timer;
    if (time <= 0) {
      this.setData({
        timer: 0
      })
      return;
    } else {
      time--;
      this.setData({
        timer: time
      }, () => {
        setTimeout(() => {
          this.runTime();
        }, 1000);
      })
    }
  },

  async sendSMS() {
    let url = 'ylf/buyer/sms/send?captcha=' + this.data.captcha + '&mobile=' + (this.data.step == 1 ? this.data.oldMobile : this.data.mobile) + '&captchaType=1&type=212';
    let res = await request.post(url, {});
    return res;
  },

  async getCaptchaId() {
    let res = await request.get('user/captcha/necaptcha/type');
    this.setData({
      captchaData: res.data
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    let phone = app.globalData.account ? app.globalData.account.mobile : '';
    this.setData({
      oldMobile: phone
    });
    this.getCaptchaId();
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    this.setData({
      step: 1
    })
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
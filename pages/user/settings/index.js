// pages/user/settings/index.js
const app = getApp();

Page({

  /**
   * Page initial data
   */
  data: {

  },

  gotoProfile() {
    wx.navigateTo({
      url: '/pages/user/settings/profile/index',
    })
  },

  gotoAccount() {
    wx.navigateTo({
      url: '/pages/user/settings/account',
    })
  },

  gotoAbout() {
    wx.navigateTo({
      url: '/pages/user/settings/about',
    })
  },

  gotoUserAgreement(){
    wx.navigateTo({
      url: '/pages/user/settings/webView?title=迎米生活用户协议&url=https://ytx.com/ym/help/views/agreement/buyer.php',
    })
  },

  gotoPrivacy(){
    wx.navigateTo({
      url: '/pages/user/settings/webView?title=法律声明及隐私政策&url=https://ytx.com/ym/help/views/agreement/privacy.php',
    })
  },

  onBtnLogoutTap() {
    const _this = this;
    wx.showActionSheet({
      alertText: '提示',
      itemList: ['退出登录'],
      success(res) {
        _this.logout();
      }
    });
  },

  logout() {
    app.globalData.rememberMe = null;
    app.globalData.openId = null;
    app.globalData.account = null;
    app.globalData.userInfo = null;
    app.globalData.cartData = {};
    wx.removeStorageSync('rememberMe');
    wx.removeStorageSync('lastLogin');
    wx.removeStorageSync('openId');
    wx.removeStorageSync('account');
    wx.removeStorageSync('userInfo');
    wx.switchTab({
      url: '/pages/index/index/index',
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
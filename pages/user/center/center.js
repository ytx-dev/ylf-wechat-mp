// pages/user/center/center.js
const app = getApp();
const util = require('../../../utils/util.js');
const request = require('../../../utils/request.js');
const UiUtils = require("../../../lib/qrcode/UiUtils.js");
const cart = require('../../../utils/cart.js');
var unload = true;

Page({

  /**
   * Page initial data
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    imageDomain: app.globalData.imageDomain,
    cartNumData: app.globalData.cartData,
    headImg: '',
    nickName: '',
    order_status1: 0,
    order_status2: 0,
    order_status3: 0,
    order_status4: 0,
    redBagCount: 0,
    cardCount: 0,
    cardBalance: 0,
    hiddenmodalput: true,
    showNums: 0,
    customerService: '',
    serviceNumber: '',
    animationX: null,
    animationY: null,
    ballX: 0,
    ballY: 0,
    showBall: false,
    isLoading: false,
    guessList: [],
    orderBy: '',
    pageNumber: 1,
    hasNextPage: true,
    scrollTop: 0,
    isSpread: 0
  },

  gotoCouponCenter() {
    wx.navigateTo({
      url: '/pages/user/redbag/center',
    })
  },

  gotoProfile() {
    wx.navigateTo({
      url: '/pages/user/settings/profile/index',
    })
  },

  onBtnScanTap() {
    let _this = this;
    wx.scanCode({
      success(res) {
        console.log('gotoInvite', res);
        _this.gotoInviteBind(res.result);
      }
    })
  },

  async gotoInviteBind(url) {
    const invite = require('../../../utils/invite.js');
    let value = await invite.getSpreadNumberFromUrl(url);
    console.log(value)
    wx.navigateTo({
      url: '/pages/index/invite/index?spreadNumber=' + value,
    })
  },

  initCartData() {
    cart.initCartData(() => {
      this.setData({
        cartNumData: app.globalData.cartData
      });
      app.getCartNumber();
    });
  },

  setCartDataItemAdd(itemId) {
    cart.setCartDataItemAdd(itemId);
    this.setData({
      cartNumData: app.globalData.cartData
    })
  },

  gotoItem(e) {
    let item = e.currentTarget.dataset['item'];
    if (item) {
      wx.navigateTo({
        url: '/pages/item/detail/detail?itemId=' + item.itemId
      })
    }
  },

  async onItemAddCartTap(e) {
    if (this.data.isLoading) {
      return;
    }
    app.login(() => {
      let itemId = e.currentTarget.dataset['itemId'];
      this.setCartDataItemAdd(itemId);
      this.addCart(itemId, e);
    });
  },

  async addCart(itemId, e) {
    this.setData({
      isLoading: true
    });
    const res = await request.post('ylf/trade/cart/' + app.globalData.sellerShop.accountId + '/' + itemId);
    if (res.succeed) {
      let ballX = e.touches[0].clientX;
      let ballY = e.touches[0].clientY;
      this.createAnimation(ballX, ballY);
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      });
      this.setData({
        isLoading: false
      });
    }
  },

  setDelayTime(sec) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, sec)
    });
  },

  // 创建动画
  createAnimation(ballX, ballY) {
    var sysinfo = wx.getSystemInfoSync();
    let _this = this;
    let bottomX = UiUtils.rpx2px(480);
    let bottomY = sysinfo.windowHeight;
    let animationX = _this.flyX(bottomX, ballX); // 创建小球水平动画
    let animationY = _this.flyY(bottomY, ballY); // 创建小球垂直动画
    _this.setData({
      ballX: ballX,
      ballY: ballY,
      showBall: true
    });
    _this.setDelayTime(100).then(() => {
      // 100ms延时,  确保小球已经显示
      _this.setData({
        animationX: animationX.export(),
        animationY: animationY.export()
      })
      // 400ms延时, 即小球的抛物线时长
      return _this.setDelayTime(400);
    }).then(() => {
      _this.setData({
        animationX: _this.flyX(0, 0, 0).export(),
        animationY: _this.flyY(0, 0, 0).export(),
        showBall: false,
        isLoading: false
      });
      _this.initCartData();
    })
  },

  // 水平动画
  flyX(bottomX, ballX, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'linear',
    })
    animation.translateX(bottomX - ballX).step();
    return animation;
  },
  // 垂直动画
  flyY(bottomY, ballY, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'ease-in',
    })
    animation.translateY(bottomY - ballY).step();
    return animation;
  },

  initGuessList() {
    this.setData({
      guessList: [],
      pageNumber: 1,
      orderBy: '',
      hasNextPage: true
    }, () => {
      this.getGuessList();
    });
  },

  async getGuessList() {
    let params = {
      pageNum: this.data.pageNumber,
      sellerAccountId: app.globalData.sellerShop.accountId
    }
    if (!app.globalData.isInRange) {
      params.type = 2;
    }
    if (this.data.orderBy) {
      params.sort = this.data.orderBy;
    }
    let res = await request.get('ylf/search/guess', params);
    if (res && res.data) {
      let page = res.data.page;
      this.setData({
        guessList: this.data.guessList.concat(page.list),
        hasNextPage: page.pageNum < page.pages,
        orderBy: page.orderBy
      });
    }
  },

  gotoSettings() {
    wx.navigateTo({
      url: '/pages/user/settings/index',
    })
  },

  gotoAddress() {
    wx.navigateTo({
      url: '/pages/user/address/mgtList/index',
    })
  },

  gotoInvoice() {
    wx.navigateTo({
      url: '/pages/user/invoice/list',
    })
  },

  gotoInvite() {
    wx.navigateTo({
      url: '/pages/user/invite/index',
    })
  },

  callPhone() {
    if (this.data.serviceNumber) {
      wx.makePhoneCall({
        phoneNumber: this.data.serviceNumber
      })
    }
  },

  gotoFeedback() {
    wx.navigateTo({
      url: '/pages/user/feedback/index',
    })
  },

  getServiceNumber() {
    let phone = '';
    if (app.globalData.sellerShop) {
      if (app.globalData.sellerShop.phoneAreaNumber && app.globalData.sellerShop.phoneNumber) {
        phone = app.globalData.sellerShop.phoneAreaNumber + '-' + app.globalData.sellerShop.phoneNumber;
      } else if (app.globalData.sellerShop.mobile) {
        phone = app.globalData.sellerShop.mobile;
      }
    }
    this.setData({
      serviceNumber: phone
    })
  },

  showOrderaList: function (event) {
    var status = event.currentTarget.dataset.status;
    if (unload) {
      unload = false;
      if (status > 30) {
        util.navigation_to('/pages/user/order/refund/list', 1);
      } else {
        util.navigation_to('/pages/user/order/list/list?status=' + status, 1);
      }
    }
  },

  showRedbagList: function () {
    if (unload) {
      unload = false;
      util.navigation_to('/pages/user/redbag/redbag', 1);
    }
  },

  showCardList: function () {
    if (unload) {
      unload = false;
      util.navigation_to('/pages/user/card/card', 1);
    }
  },

  async sqlGetOrderCount() {
    var that = this;
    try {
      const res = await request.get('ylf/buyer/buyer/show');
      const data = res.data;
      if (data) {
        that.setData({
          order_status1: data.awaitingPay,
          order_status2: data.awaitingShip,
          order_status3: data.shipping,
          order_status4: data.progressDisputeJournalCount,
          redBagCount: data.redBagCount,
          isSpread: data.isSpread
        });
        that.initNameAndHead(data.name, data.headImg);
      }
      that.getCardInfo();
    } catch (err) {
      if (err.isReLogin) {
        app.login(() => {
          this.init();
        });
      }
    }
  },

  async getCardInfo() {
    let res = await request.get('ylf/gift/gift/info');
    if (res && res.data && res.data.giftCard) {
      const data = res.data.giftCard;
      this.setData({
        cardBalance: data && data.balance ? data.balance : 0
      });
    } else {
      this.setData({
        cardBalance: 0
      });
    }

  },

  initNameAndHead(name, headImg) {
    let fullHeadImg = headImg ? headImg : (app.globalData.account && app.globalData.account.headImg ? app.globalData.account.headImg : (app.globalData.userInfo && app.globalData.userInfo.avatarUrl ? app.globalData.userInfo.avatarUrl : ''));
    if (!(/http|https/.test(fullHeadImg))) {
      fullHeadImg = app.globalData.imageDomain + fullHeadImg;
    }
    this.setData({
      headImg: fullHeadImg,
      nickName: name ? name : (app.globalData.account && app.globalData.account.nickName ? app.globalData.account.nickName : '')
    });
  },

  init() {
    this.sqlGetOrderCount();
    this.getServiceNumber();
    this.initGuessList();
    this.initCartData();
  },

  onPageScroll: function (res) {
    this.setData({
      scrollTop: res.scrollTop
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    this.setData({
      showNums: this.data.showNums += 1,
      cartNumData: app.globalData.cartData
    });
    unload = true;
    app.login(() => {
      this.init();
    });
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {
    if (this.data.hasNextPage) {
      this.setData({
        pageNumber: this.data.pageNumber + 1,
      }, () => {
        this.getGuessList();
      })
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
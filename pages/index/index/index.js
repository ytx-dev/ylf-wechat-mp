// pages/index/index/index.js
const app = getApp();
const request = require('../../../utils/request.js');
const address = require('../../../utils/address.js');
const util = require('../../../utils/util.js');
const UiUtils = require("../../../lib/qrcode/UiUtils.js");
const cart = require('../../../utils/cart.js');

Page({

  /**
   * Page initial data
   */
  data: {
    currLocationName: app.globalData.aoi ? app.globalData.aoi.name : '',
    imgServer: app.globalData.imageDomain,
    cartData: app.globalData.cartData,
    widgetDatas: [],
    isReachBottom: false,
    currShop: {
      sellerAccountName: '',
      sellerAccountId: -1
    },
    currAddress: {},
    isReloadData: false,
    isAlertAddr: false,
    cacheAddress: {},
    paddingHeight: 0,
    showRedBagDialog: false,
    isReceiveRedbag: false,
    redBagList: null,
    scrollTop: 0,
    animationX: null,
    animationY: null,
    ballX: 0,
    ballY: 0,
    showBall: false,
    isLoading: false,
    guessList: [],
    orderBy: '',
    pageNumber: 1,
    hasNextPage: true,
    notifyList: [],
    isShowedAlert: false,
    currAlert: null,
    seckillData: {},
    seckillTime: 0,
    seckillTimer: null
  },

  onBtnScanTap() {
    let _this = this;
    app.login(() => {
      wx.scanCode({
        success(res) {
          _this.gotoInvite(res.result);
        }
      });
    });
  },

  async gotoInvite(url) {
    const invite = require('../../../utils/invite.js');
    let value = await invite.getSpreadNumberFromUrl(url);
    wx.navigateTo({
      url: '/pages/index/invite/index?spreadNumber=' + value,
    })
  },

  onNotifyItemTap(e) {
    const item = e.currentTarget.dataset['item'];
    wx.navigateTo({
      url: '/pages/index/notify/detail?id=' + item.id,
    })
  },

  async initNotifyData() {
    if (app.globalData.sellerShop && app.globalData.sellerShop.accountId > 0) {
      let params = {
        sellerAccountId: app.globalData.sellerShop.accountId
      }
      if (!app.globalData.isInRange) {
        params.type = 2;
      }
      let res = await request.get('ylf/buyer/notice/list', params);
      if (res && res.data && res.data.list) {
        this.setData({
          notifyList: res.data.list
        })
      } else {
        this.setData({
          notifyList: []
        })
      }
    }
  },

  gotoSeckill() {
    wx.navigateTo({
      url: '/pages/index/seckill/list',
    })
  },

  async initSeckillData() {
    if (app.globalData.sellerShop && app.globalData.sellerShop.accountId > 0) {
      let params = {
        sellerAccountId: app.globalData.sellerShop.accountId
      }
      if (!app.globalData.isInRange) {
        params.type = 2;
      }
      let res = await request.get('ylf/buyer/seckill/index', params);
      if (res && res.data && res.data.dataVo) {
        this.setData({
          seckillData: res.data.dataVo,
          seckillTime: (res.data.dataVo.stop - res.data.dataVo.currentTime) * 1000
        }, () => {
          if (this.data.seckillTimer) {
            clearTimeout(this.data.seckillTimer);
            this.setData({
              seckillTimer: null
            });
          }
          this.countdownSeckill();
        })
      } else {
        this.setData({
          seckillData: {},
          seckillTime: 0
        })
      }
    }
  },

  countdownSeckill() {
    let time = this.data.seckillTime - 1000;
    if (time > 0) {
      let timer = setTimeout(() => {
        this.countdownSeckill();
      }, 1000);
      let seckillData = this.data.seckillData;
      let timeStr = util.convertTimeDian3(time);
      timeStr = timeStr.split(':');
      seckillData.hh = timeStr[0];
      seckillData.mm = timeStr[1];
      seckillData.ss = timeStr[2];
      this.setData({
        seckillData: seckillData,
        seckillTime: time,
        seckillTimer: timer
      });

    } else {
      this.initSeckillData();
    }
  },

  initGuessList() {
    this.setData({
      guessList: [],
      pageNumber: 1,
      orderBy: '',
      hasNextPage: true
    }, () => {
      this.getGuessList();
    });
  },

  async getGuessList() {
    if (app.globalData.sellerShop.accountId) {
      let params = {
        pageNum: this.data.pageNumber,
        sellerAccountId: app.globalData.sellerShop.accountId
      }
      if (!app.globalData.isInRange) {
        params.type = 2;
      }
      if (this.data.orderBy) {
        params.sort = this.data.orderBy;
      }
      let res = await request.get('ylf/search/guess', params);
      if (res && res.data) {
        let page = res.data.page;
        this.setData({
          guessList: this.data.guessList.concat(page.list),
          hasNextPage: page.pageNum < page.pages,
          orderBy: page.orderBy
        });
      }
    }
  },

  onAddrAlertCloseTap() {
    this.setData({
      isAlertAddr: false
    });
    this.initAlert();
  },

  onAddrAlertChangeTap() {
    this.setData({
      currAddress: this.data.cacheAddress,
      isAlertAddr: false
    }, () => {
      wx.setStorageSync('currAddress', JSON.stringify(this.data.cacheAddress));
      this.setCheckAddrInRangeAndInitShop(true);
    });
  },

  setDelayTime(sec) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, sec)
    });
  },

  // 创建动画
  createAnimation(ballX, ballY) {
    var sysinfo = wx.getSystemInfoSync();
    let _this = this;
    let bottomX = UiUtils.rpx2px(480);
    let bottomY = sysinfo.windowHeight;
    let animationX = _this.flyX(bottomX, ballX); // 创建小球水平动画
    let animationY = _this.flyY(bottomY, ballY); // 创建小球垂直动画

    _this.setData({
      ballX: ballX,
      ballY: ballY,
      showBall: true
    });
    _this.setDelayTime(100).then(() => {
      // 100ms延时,  确保小球已经显示
      _this.setData({
        animationX: animationX.export(),
        animationY: animationY.export()
      })
      // 400ms延时, 即小球的抛物线时长
      return _this.setDelayTime(400);
    }).then(() => {
      _this.setData({
        animationX: _this.flyX(0, 0, 0).export(),
        animationY: _this.flyY(0, 0, 0).export(),
        showBall: false,
        isLoading: false
      });
      this.initCartData();
    })
  },

  // 水平动画
  flyX(bottomX, ballX, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'linear',
    })
    animation.translateX(bottomX - ballX).step();
    return animation;
  },
  // 垂直动画
  flyY(bottomY, ballY, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'ease-in',
    })
    animation.translateY(bottomY - ballY).step();
    return animation;
  },

  async receiveRedbagDialog() {
    let params = [];
    for (const i in this.data.redBagList) {
      let item = this.data.redBagList[i];
      params.push(item.batchCode);
    }
    let res = await request.post('ylf/buyer/buyer/redbag/receiveAll', params);
    let list = [];
    if (res && res.data && res.data.redBagBatches && res.data.redBagBatches.length > 0) {
      for (const i in res.data.redBagBatches) {
        const item = res.data.redBagBatches[i];
        item.startTime = util.formatTime2(new Date(item.efficientDate), '.');
        item.endTime = util.formatTime2(new Date(item.efficientEndDate), '.');
        list.push(item);
      }
    }
    this.setData({
      isReceiveRedbag: true,
      redBagList: list
    });
  },

  gotoRedbag() {
    this.closeRedbagDialog();
    wx.navigateTo({
      url: '/pages/user/redbag/redbag',
    })
  },

  gotoRedbagItemList(e) {
    let item = e.currentTarget.dataset.item;
    console.log(item);
    this.closeRedbagDialog();
    if (item.usingRange != 0) {
      wx.navigateTo({
        url: '/pages/item/redbag/list?redbag=' + JSON.stringify(item),
      })
    }
  },

  closeRedbagDialog() {
    this.setData({
      showRedBagDialog: false,
      isReceiveRedbag: false,
      redBagList: null
    });
  },

  onAddrTap() {
    util.navigation_to("/pages/index/addr/index", 1);
  },

  async updateAppPage(sellerAccountId, isNeedAlert) {
    this.setData({
      widgetDatas: []
    });
    const shopIndex = await this.getShopIndexPage(sellerAccountId);
    app.globalData.sellerShop = shopIndex.sellerShop;
    wx.setStorage({
      key: "sellerShop",
      data: JSON.stringify(shopIndex.sellerShop)
    });
    wx.setStorage({
      key: "currShop",
      data: JSON.stringify(this.data.currShop)
    });
    await this.initNotifyData();
    this.initSeckillData();
    this.initCartData();
    wx.stopPullDownRefresh();
    wx.hideLoading();
    this.setPageWidget(shopIndex.appPage);
    this.setData({
      isReloadData: false
    });
    this.initGuessList();
    if (isNeedAlert && !this.data.isAlertAddr) {
      this.initAlert();
    }
  },

  initCartData() {
    cart.initCartData(() => {
      this.setData({
        cartData: app.globalData.cartData
      });
      app.getCartNumber();
    });
  },

  setCartDataItemAdd(itemId) {
    cart.setCartDataItemAdd(itemId);
    this.setData({
      cartData: app.globalData.cartData
    })
  },

  async initAlert() {
    //检查红包
    await this.getRedbag();
    //检查弹框
    if (!this.data.showRedBagDialog && !this.data.isShowedAlert) {
      this.getAlert();
    }
  },

  async getRedbag() {
    if (app.globalData.rememberMe) {
      const res = await request.get('ylf/buyer/buyer/redbag/list');
      if (res && res.data && res.data.redBagBatches && res.data.redBagBatches.length > 0) {
        let list = [];
        for (const i in res.data.redBagBatches) {
          const item = res.data.redBagBatches[i];
          item.startTime = util.formatTime2(new Date(item.efficientDate), '.');
          item.endTime = util.formatTime2(new Date(item.efficientEndDate), '.');
          list.push(item);
        }
        this.setData({
          showRedBagDialog: true,
          isReceiveRedbag: false,
          redBagList: list
        })
      } else {
        this.setData({
          showRedBagDialog: false,
          isReceiveRedbag: false,
          redBagList: null
        });
      }
    }
  },

  async getAlert() {
    if (app.globalData.sellerShop.accountId) {
      let url = 'ylf/store/dialog/' + app.globalData.sellerShop.accountId;
      if (!app.globalData.isInRange) {
        url += '?type=2';
      }
      const res = await request.getWithOutToken(url);
      let list = res.data.dialog;
      if (list && list.length > 0) {
        for (const i in list) {
          let item = list[i];
          if (!this.getLocalAlertById(item.id)) {
            this.setData({
              currAlert: item,
              isShowedAlert: true
            }, () => {
              this.setLocalAlertById(item.id);
            });
            break;
          }
        }
      }
    }
  },

  getLocalAlertById(id) {
    try {
      let str = wx.getStorageSync('alert');
      if (str) {
        let data = JSON.parse(str);
        let nowDate = util.formatTime2(new Date(), '-');
        if (data.date != nowDate) {
          this.cleanLocalAlert();
        } else if (data.ids && data.ids[id]) {
          return 1;
        }
      }
    } catch (e) {}
    return null;
  },
  /*
  {
    date: '2022-01-01',

  }
  */
  setLocalAlertById(id) {
    try {
      let str = wx.getStorageSync('alert');
      let data = {};
      if (str) {
        data = JSON.parse(str);
        if (!data) {
          data = {};
        }
      }
      if (!data.date) {
        data.date = util.formatTime2(new Date(), '-');
      }
      if (!data.ids) {
        data.ids = {};
      }
      data.ids[id] = 1;
      wx.setStorageSync('alert', JSON.stringify(data));
    } catch (e) {}
  },

  cleanLocalAlert() {
    try {
      wx.removeStorageSync('alert');
    } catch (e) {}
  },

  onAlertTap() {
    let alert = this.data.currAlert;
    if (alert.type == 0) {
      this.gotoDetail(alert.refId);
    } else if (alert.type == 1) {
      this.gotoList(alert.refId, alert.path, '商品列表');
    } else if (alert.type == 2) {
      this.gotoTopic(alert.refId);
    }
    this.setData({
      currAlert: null
    })
  },

  onAlertCloseTap() {
    this.setData({
      currAlert: null
    })
  },

  async getShopIndexPage(id) {
    let url = 'ylf/store/index/' + id;
    if (!app.globalData.isInRange) {
      url += '?type=2';
    }
    const res = await request.getWithOutToken(url);
    return res.data;
  },

  async setPageWidget(datas) {
    const result = [];
    for (const i in datas) {
      const data = datas[i];
      data.convertParameters = JSON.parse(data.convertParameters);
      if (data.appWidgetPrototypeId === 11) {
        //do nothing
      } else if (data.appWidgetPrototypeId === 7 || data.appWidgetPrototypeId === 8) {
        data.convertParameters.item = JSON.parse(data.convertParameters.item);
      } else {
        for (const key in data.convertParameters) {
          data.convertParameters[key] = JSON.parse(data.convertParameters[key]);
        }
      }
      result.push(data);
    }
    this.setData({
      widgetDatas: result
    });
  },

  onWidgetTap(e) {
    const item = e.currentTarget.dataset['item'];
    if ((item.type + '') === '1') {
      this.gotoViewByUrl(item.url);
    } else if ((item.type + '') === '2') {
      this.gotoList(item.categoryId, item.categoryIds, item.title ? item.title : '商品列表');
    } else if ((item.type + '') === '3') {
      this.gotoDetail(item.itemId);
    } else if ((item.type + '') === '4') {
      this.gotoTopic(item.topicId);
    } else if ((item.type + '') === '5') {
      this.gotoPintuan(0);
    } else if ((item.type + '') === '6') {
      this.gotoPintuan(1);
    } else if ((item.type + '') === '7') {
      this.gotoLive();
    } else if ((item.type + '') === '8') {
      this.gotoLiveRoom();
    }
  },

  onCategoryTap(e) {
    let categoryId = e.currentTarget.dataset['categoryId'];
    let categoryIds = e.currentTarget.dataset['categoryIds'];
    let categoryName = e.currentTarget.dataset['categoryName'];
    this.gotoList(categoryId, categoryIds, categoryName);
  },

  onItemTap(e) {
    let itemId = e.currentTarget.dataset['itemId'];
    this.gotoDetail(itemId);
  },

  async onItemAddCartTap(e) {
    if (this.data.isLoading) {
      return;
    }
    app.login(() => {
      let itemId = e.currentTarget.dataset['itemId'];
      this.setCartDataItemAdd(itemId);
      this.addCart(itemId, e);
    });
  },

  async addCart(itemId, e) {
    if (app.globalData.sellerShop && app.globalData.sellerShop.accountId) {
      this.setData({
        isLoading: true
      });
      const res = await request.post('ylf/trade/cart/' + app.globalData.sellerShop.accountId + '/' + itemId);
      if (res.succeed) {
        let ballX = e.touches[0].clientX;
        let ballY = e.touches[0].clientY;

        this.createAnimation(ballX, ballY);
      } else {
        wx.showToast({
          title: res.message,
          icon: 'none'
        });
        this.setData({
          isLoading: false
        });
      }
    }
  },

  onSearchTap(e) {
    util.navigation_to("/pages/item/search/search", 1);
  },

  gotoDetail(itemId) {
    util.navigation_to('/pages/item/detail/detail?itemId=' + itemId, 1);
  },

  gotoTopic(topicId) {
    util.navigation_to('/pages/index/topic/index?topicId=' + topicId, 1);
  },

  gotoPintuan(type) {
    util.navigation_to('/pages/pintuan/index/index?type=' + type, 1);
  },

  gotoLive() {
    wx.navigateTo({
      url: '/pages/index/live/index',
    })
  },

  gotoLiveRoom() {
    wx.navigateTo({
      url: '/pages/index/liveroom/index',
    })
  },

  gotoList(categoryId, categoryIds, categoryName) {
    const array = categoryIds.split(',');
    if (array.length === 3) {
      categoryId = array[1];
      categoryIds = array[1] + ',' + array[2];
    }
    util.navigation_to('/pages/item/list/list2?categoryId=' + categoryId + '&categoryIds=' + categoryIds + '&navigationBarTitleText=' + categoryName, 1);
  },

  gotoViewByUrl(url) {},

  async initAddress(isNeedAlert) {
    try {
      const cacheAddressStr = wx.getStorageSync('currAddress');
      const res = await address.getCurrentLocation();
      if (res && res.sellerAccountId > 0 && res.latitude > 0) {
        if (isNeedAlert && cacheAddressStr) {
          const cacheAddr = JSON.parse(cacheAddressStr);
          if (cacheAddr && cacheAddr.sellerAccountId && cacheAddr.sellerName) {
            if (res.isInRange != cacheAddr.isInRange) {
              this.setData({
                isAlertAddr: true,
                cacheAddress: cacheAddr
              });
            } else {
              this.setData({
                isAlertAddr: false,
                cacheAddress: {}
              });
            }
          } else {
            this.setData({
              isAlertAddr: false,
              cacheAddress: {}
            });
          }
        }
        this.setData({
          currAddress: {
            ...res,
            isAuto: true
          }
        }, () => {
          wx.setStorageSync('currAddress', JSON.stringify(res));
          this.setCheckAddrInRangeAndInitShop(isNeedAlert);
        });
      } else {
        //读取之前的缓存地址
        if (cacheAddressStr) {
          const cacheAddr = JSON.parse(cacheAddressStr);
          if (cacheAddr && cacheAddr.sellerAccountId && cacheAddr.sellerName) {
            this.setData({
              currAddress: cacheAddr
            }, () => {
              this.setCheckAddrInRangeAndInitShop(isNeedAlert);
            });
          } else {
            this.setData({
              currAddress: {}
            }, () => {
              this.reCheckAddr(isNeedAlert);
            });
          }
        } else {
          this.setData({
            currAddress: {}
          }, () => {
            this.reCheckAddr(isNeedAlert);
          });
        }
      }
    } catch (e) {
      wx.hideLoading();
    }
  },

  async reloadAddr() {
    const currAddressStr = wx.getStorageSync('currAddress');
    if (currAddressStr) {
      const currAddr = JSON.parse(currAddressStr);
      if (!currAddr.sellerAccountId) {
        currAddr.sellerAccountId = app.globalData.sellerAccountId;
      }
      const res = await address.checkAddr('', currAddr.latitude, currAddr.longitude);
      if (res.isCreateAddress) {
        currAddr.isInRange = true;
      } else {
        currAddr.isInRange = false;
      }
      this.setData({
        currAddress: currAddr
      }, () => {
        this.setCheckAddrInRangeAndInitShop();
      });
    }
  },

  async reCheckAddr(isNeedAlert) {
    const res = await address.checkAddr('', '1', '1');
    if (res) {
      app.globalData.isInRange = res.isCreateAddress;
      app.globalData.sellerAccountId = res.sellerAccountId;
      let currShop = {
        sellerAccountId: res.sellerAccountId,
        sellerName: res.sellerName
      };
      this.setData({
        currShop: currShop
      });
      this.updateAppPage(currShop.sellerAccountId, isNeedAlert);
    }
  },

  async setCheckAddrInRangeAndInitShop(isNeedAlert) {
    app.globalData.isInRange = this.data.currAddress.isInRange;
    let currShop = {
      sellerAccountId: this.data.currAddress.sellerAccountId
    };
    this.setData({
      currShop: currShop
    });
    this.updateAppPage(currShop.sellerAccountId, isNeedAlert);
  },

  onPageScroll: function (res) {
    this.setData({
      scrollTop: res.scrollTop
    });
  },

  async initInvite() {
    let spreadNumber = wx.getStorageSync('spreadNumber');
    if (spreadNumber && app.globalData.rememberMe) {
      const invite = require('../../../utils/invite.js');
      invite.bindFormLocal();
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    if (options.spreadNumber) {
      wx.setStorageSync('spreadNumber', options.spreadNumber);
    }
    this.setData({
      paddingHeight: wx.getMenuButtonBoundingClientRect().top
    });
    const appAuthorizeSetting = wx.getAppAuthorizeSetting();
    if (appAuthorizeSetting.locationAuthorized == 'not determined') {
      this.reCheckAddr(false);
    }
    this.initAddress(true);
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    let isReloadAddr = wx.getStorageSync('isReloadAddr');
    if (isReloadAddr) {
      wx.removeStorageSync('isReloadAddr');
      this.reloadAddr();
    } else {
      if (this.data.isReloadData) {
        this.setCheckAddrInRangeAndInitShop();
      } else {
        // this.getRedbag();
        this.initCartData();
      }
    }
    this.initInvite();
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {
    if (this.data.currAddress && this.data.currAddress.isAuto) {
      this.initAddress(false);
    } else {
      if (this.data.currShop && this.data.currShop.sellerAccountId) {
        this.updateAppPage(this.data.currShop.sellerAccountId, false);
      } else {
        this.initAddress(false);
      }
    }
  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {
    this.setData({
      isReachBottom: true
    });
    if (this.data.hasNextPage) {
      this.setData({
        pageNumber: this.data.pageNumber + 1,
      }, () => {
        this.getGuessList();
      })
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
// pages/index/topic/index.js
const app = getApp();
const request = require('../../../utils/request.js');
const cart = require('../../../utils/cart.js');
const UiUtils = require("../../../lib/qrcode/UiUtils.js");

Page({

  /**
   * Page initial data
   */
  data: {
    imgServer: app.globalData.imageDomain,
    StatusBar: app.globalData.StatusBar < 21 ? 26 : app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    scrollTop: 0,
    cartNum: 0,
    cartData: app.globalData.cartData,
    animationX: null,
    animationY: null,
    ballX: 0,
    ballY: 0,
    showBall: false,
    isLoading: false,
    topicId: null,
    topicData: {},
    itemList: [],
    tagList: [],
    tagMap: {},
    currTag: ''
  },

  onTagTap(e) {
    let data = e.currentTarget.dataset['data'];
    this.setData({
      currTag: data,
      itemList: this.data.tagMap[data]
    })
  },

  setDelayTime(sec) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, sec)
    });
  },

  // 创建动画
  createAnimation(ballX, ballY) {
    let _this = this;
    let bottomX = UiUtils.rpx2px(520);
    let bottomY = UiUtils.rpx2px(80);
    let animationX = _this.flyX(bottomX, ballX); // 创建小球水平动画
    let animationY = _this.flyY(bottomY, ballY); // 创建小球垂直动画
    _this.setData({
      ballX: ballX,
      ballY: ballY,
      showBall: true
    });
    _this.setDelayTime(100).then(() => {
      // 100ms延时,  确保小球已经显示
      _this.setData({
        animationX: animationX.export(),
        animationY: animationY.export()
      })
      // 400ms延时, 即小球的抛物线时长
      return _this.setDelayTime(400);
    }).then(() => {
      _this.setData({
        animationX: _this.flyX(0, 0, 0).export(),
        animationY: _this.flyY(0, 0, 0).export(),
        showBall: false,
        isLoading: false
      })
    })
  },

  // 水平动画
  flyX(bottomX, ballX, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'linear',
    })
    animation.translateX(bottomX - ballX).step();
    return animation;
  },
  // 垂直动画
  flyY(bottomY, ballY, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'ease-in',
    })
    animation.translateY(bottomY - ballY).step();
    return animation;
  },

  onBtnBackTap() {
    const pages = getCurrentPages();
    if (pages.length > 1) {
      wx.navigateBack();
    } else {
      wx.switchTab({
        url: '/pages/index/index/index',
      })
    }
  },

  gotoCart() {
    wx.switchTab({
      url: '/pages/cart/cart',
    })
  },

  gotoItem(e) {
    let item = e.currentTarget.dataset['item'];
    if (item) {
      wx.navigateTo({
        url: '/pages/item/detail/detail?itemId=' + item.itemId
      })
    }
  },

  async onItemAddCartTap(e) {
    if (this.data.isLoading) {
      return;
    }
    app.login(() => {
      let itemId = e.currentTarget.dataset['itemId'];
      this.setCartDataItemAdd(itemId);
      this.addCart(itemId, e);
    });
  },

  async addCart(itemId, e) {
    this.setData({
      isLoading: true
    });
    const res = await request.post('ylf/trade/cart/' + app.globalData.sellerShop.accountId + '/' + itemId);
    if (res.succeed) {
      let ballX = e.touches[0].clientX;
      let ballY = e.touches[0].clientY;
      this.createAnimation(ballX, ballY);
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      });
      this.setData({
        isLoading: false
      });
    }
  },

  initCartData() {
    cart.initCartData(() => {
      let count = cart.getCartSize();
      this.setData({
        cartData: app.globalData.cartData,
        cartNum: count
      })
    });
  },

  setCartDataItemAdd(itemId) {
    cart.setCartDataItemAdd(itemId);
    let count = cart.getCartSize();
    this.setData({
      cartData: app.globalData.cartData,
      cartNum: count
    })
  },

  async init() {
    let res = await request.get('ylf/item/topic/' + app.globalData.sellerShop.accountId + '/' + this.data.topicId);
    let tagList = [];
    let tagMap = {};
    if (res && res.data) {
      for (const i in res.data.items) {
        let item = res.data.items[i];
        if (item.tag) {
          let tag_arr = tagMap[item.tag];
          if (!tag_arr) {
            tag_arr = [];
          }
          tag_arr.push(item);
          tagMap[item.tag] = tag_arr;
        }
      }
    }
    for (const key in tagMap) {
      tagList.push(key);
    }
    this.setData({
      topicData: res.data.topic,
      itemList: tagList.length > 0 ? tagMap[tagList[0]] : res.data.items,
      tagList: tagList,
      tagMap: tagMap,
      currTag: tagList.length > 0 ? tagList[0] : ''
    });
    this.initCartData();
  },

  async getShopIndexPage(id) {
    const res = await request.get('ylf/store/index/' + id);
    return res.data;
  },

  async checkShopData(shopId) {
    if (app.globalData.sellerShop && app.globalData.sellerShop.accountId) {
      this.init();
    } else if (shopId) {
      const shopIndex = await this.getShopIndexPage(shopId);
      app.globalData.sellerShop = shopIndex.sellerShop;
      wx.setStorage({
        key: "sellerShop",
        data: JSON.stringify(shopIndex.sellerShop)
      });
      this.init();
    } else {
      wx.showModal({
        title: '提示',
        content: '请选择地址',
        showCancel: false,
        success(res) {
          wx.switchTab({
            url: '/pages/index/index/index',
          });
        }
      });
    }
  },

  /**
   * Lifecycle function--Called when page scroll
   */
  onPageScroll: function (res) {
    this.setData({
      scrollTop: res.scrollTop
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    if (!options.topicId) {
      wx.navigateBack();
    } else {
      this.setData({
        topicId: options.topicId
      }, () => {
        this.checkShopData(options.shopId);
      });
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    let query = wx.createSelectorQuery();
    query.select('.page-topic-navbar').boundingClientRect(rect => {
      let v = UiUtils.px2rpx(rect.height);
      let height = UiUtils.rpx2px(v + 220);
      this.setData({
        navbarHeightpx: rect.height,
        navbarHeightrpx: v ? v : 0,
        scrollHeight: height
      })
    }).exec();
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return {
      title: this.data.topicData.title,
      path: '/pages/index/topic/index?topicId=' + this.data.topicData.id + '&shopId=' + app.globalData.sellerShop.accountId,
      imageUrl: this.data.imgServer + this.data.topicData.image
    };
  }
})
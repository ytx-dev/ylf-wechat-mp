// pages/index/live/index.js
const app = getApp();
const request = require('../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    dataList: [],
    total: 0,
    pageNumber: 1
  },

  async getList() {
    let res = await request.get('ylf/buyer/liveStream/room/list', {
      pageNumber: this.data.pageNumber
    });
    if (res && res.data) {
      let page = res.data.page;
      let array = page.list;
      let currDate = new Date();
      let todayStr = currDate.Format('MM月dd日');
      currDate.setDate(currDate.getDate() + 1);
      let tmrStr = currDate.Format('MM月dd日');
      currDate.setDate(currDate.getDate() + 1);
      let atmrStr = currDate.Format('MM月dd日');
      for (const i in array) {
        let item = array[i];
        if (item.liveStatus == 101 || item.liveStatus == 105 || item.liveStatus == 106) {
          item.timeText = '直播中';
        } else if (item.liveStatus == 103 || item.liveStatus == 107) {
          item.timeText = '已结束';
        } else {
          let date = new Date(item.startTime);
          let dateStr = date.Format('MM月dd日 hh:mm');
          if (dateStr.indexOf(todayStr) > -1) {
            dateStr = dateStr.replace(todayStr + ' ', '');
          } else if (dateStr.indexOf(tmrStr) > -1) {
            dateStr = dateStr.replace(tmrStr, '明天');
          } else if (dateStr.indexOf(atmrStr) > -1) {
            dateStr = dateStr.replace(atmrStr, '后天');
          }
          item.timeText = dateStr + '开始';
        }
      }
      this.setData({
        dataList: this.data.dataList.concat(array),
        total: page.total
      })
    }
  },

  init() {
    this.setData({
      dataList: [],
      total: 0,
      pageNumber: 1
    }, () => {
      this.getList();
    })
  },

  onItemTap(e) {
    const item = e.currentTarget.dataset.item;
    this.gotoLivePage(item.roomId);
  },

  gotoLivePage(roomId) {
    wx.navigateTo({
      url: `plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=${roomId}`
    })
  },

  async checkShopData(shopId) {
    if (app.globalData.sellerShop && app.globalData.sellerShop.accountId) {} else if (shopId) {
      const shopIndex = await this.getShopIndexPage(shopId);
      app.globalData.sellerShop = shopIndex.sellerShop;
      wx.setStorage({
        key: "sellerShop",
        data: JSON.stringify(shopIndex.sellerShop)
      });
    } else {
      wx.showModal({
        title: '提示',
        content: '请选择地址',
        showCancel: false,
        success(res) {
          wx.switchTab({
            url: '/pages/index/index/index',
          });
        }
      });
    }
  },

  async getShopIndexPage(id) {
    const res = await request.get('ylf/store/index/' + id);
    return res.data;
  },

  async initAppData(shopId, roomId) {
    await this.checkShopData(shopId);
    this.gotoLivePage(roomId);
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    if (options.shopId && options.roomId) {
      this.initAppData(options.shopId, options.roomId);
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {
    this.init();
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {
    if (this.data.dataList.length < this.data.total) {
      this.setData({
        pageNumber: ++this.data.pageNumber
      }, () => {
        this.getList();
      });
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
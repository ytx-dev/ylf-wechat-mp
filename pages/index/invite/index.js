// pages/index/invite/index.js
const app = getApp();
const request = require('../../../utils/request.js');
const invite = require('../../../utils/invite.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imgServer: app.globalData.imageDomain,
    spreadNumber: null,
    spreadData: {},
    errMsg: '',
    isShowDialog: false
  },

  emptyHandler() {

  },

  onDialogClose() {
    this.setData({
      errMsg: '',
      isShowDialog: false
    })
  },

  async onBtnOkTap() {
    let res = await invite.bind(this.data.spreadNumber);
    if (res.succeed) {
      wx.redirectTo({
        url: '/pages/user/invite/bindResult',
      })
    } else {
      this.setData({
        errMsg: res.message,
        isShowDialog: true
      })
    }
  },

  onBtnCancelTap() {
    wx.navigateBack();
  },

  async getData() {
    let res = await request.get('ylf/buyer/spread/getInviter', {
      spreadNumber: this.data.spreadNumber
    });
    this.setData({
      spreadData: res.data
    })
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    this.setData({
      spreadNumber: options.spreadNumber
    }, () => {
      this.getData();
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {

  }
})
// pages/index/liveroom/index.js
const app = getApp();
const request = require('../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imgServer: app.globalData.imageDomain,
    isLoaded: false,
    roomId: undefined,
    dataList: [],
    total: 0,
    pageNumber: 1
  },

  async getList() {
    let res = await request.get('ylf/liveroom/mobile/activity/queryList', {
      sellerAccountId: app.globalData.sellerShop.accountId,
      pageNumber: this.data.pageNumber
    });
    if (res && res.data) {
      let page = res.data;
      let array = page.list;
      let currDate = new Date();
      let todayStr = currDate.Format('MM月dd日');
      currDate.setDate(currDate.getDate() + 1);
      let tmrStr = currDate.Format('MM月dd日');
      currDate.setDate(currDate.getDate() + 1);
      let atmrStr = currDate.Format('MM月dd日');
      let currLive = {};
      for (const i in array) {
        let item = array[i];
        if (item.itemId && item.itemId == this.data.roomId) {
          currLive = item;
        }
        if (item.status == 1) {
          item.timeText = '直播中';
        } else if (item.liveStatus == 2) {
          item.timeText = '已结束';
        } else if (item.liveStatus == 3) {
          item.timeText = '已过期';
        } else {
          let date = new Date(item.startTime);
          let dateStr = date.Format('MM月dd日 hh:mm');
          if (dateStr.indexOf(todayStr) > -1) {
            dateStr = dateStr.replace(todayStr + ' ', '');
          } else if (dateStr.indexOf(tmrStr) > -1) {
            dateStr = dateStr.replace(tmrStr, '明天');
          } else if (dateStr.indexOf(atmrStr) > -1) {
            dateStr = dateStr.replace(atmrStr, '后天');
          }
          item.timeText = dateStr + '开始';
        }
      }
      this.setData({
        dataList: this.data.dataList.concat(array),
        isLoaded: true
        // total: page.total
      }, () => {
        if (currLive && currLive.roomId) {
          this.gotoLive(currLive);
        }
      })
    }
  },

  init() {
    this.setData({
      dataList: [],
      total: 0,
      pageNumber: 1
    }, () => {
      this.getList();
    })
  },

  onItemTap(e) {
    const item = e.currentTarget.dataset.item;
    if (item && item.status == 1) {
      this.gotoLive(item);
    } else {
      wx.showToast({
        title: '暂未开播',
        icon: 'none'
      })
    }
  },

  gotoLive(item) {
    if (item && item.status == 1) {
      wx.navigateTo({
        url: '/pages/index/liveroom/live?roomId=' + item.roomId + '&id=' + item.id + '&ownerId=' + item.ownerId + '&activityName=' + item.name + '&activityImage=' + this.data.imgServer + item.cover
      })
    }
  },

  async checkShopData(shopId) {
    if (app.globalData.sellerShop && app.globalData.sellerShop.accountId) {} else if (shopId) {
      const shopIndex = await this.getShopIndexPage(shopId);
      app.globalData.sellerShop = shopIndex.sellerShop;
      wx.setStorage({
        key: "sellerShop",
        data: JSON.stringify(shopIndex.sellerShop)
      });
    } else {
      wx.showModal({
        title: '提示',
        content: '请选择地址',
        showCancel: false,
        success(res) {
          wx.switchTab({
            url: '/pages/index/index/index',
          });
        }
      });
    }
  },

  async getShopIndexPage(id) {
    const res = await request.get('ylf/store/index/' + id);
    return res.data;
  },

  async initAppData(shopId) {
    await this.checkShopData(shopId);
    this.init();
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    if (options.shopId && options.roomId) {
      this.setData({
        isLoaded: false,
        roomId: options.roomId
      }, () => {
        this.initAppData(options.shopId);
      })
    } else {
      this.setData({
        isLoaded: false,
        roomId: undefined
      }, () => {
        this.init();
      })
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {
    if (this.data.isLoaded) {
      this.init();
    }
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {
    if (this.data.dataList.length < this.data.total) {
      this.setData({
        pageNumber: ++this.data.pageNumber
      }, () => {
        this.getList();
      });
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
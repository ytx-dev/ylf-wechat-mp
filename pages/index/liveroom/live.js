// pages/index/liveroom/live.js
const app = getApp();
const request = require('../../../utils/request')
const requestLive = require('../../../utils/request_live')
const md5 = require('../../../utils/md5');
import TIM from '../../../utils/tim-wx';
import TLS from '../../../utils/tls.min.js'

Page({

  /**
   * Page initial data
   */
  data: {
    sellerAccountId: 0,
    activityId: 0,
    activityName: '',
    activityImage: '',
    ownerId: '',
    userData: {},
    currRoom: {},
    ownerInfo: {},

    userInfo: {},
    groupInfo: {
      groupID: ''
    },

    messageQueen: [],
    isTimReady: false,
    animation2: null, //商品列表动画

    goods: [],
    currGood: {},
    goodPageNumber: 1,
    goodHasNextPage: true,

    roomStatus: 0, //?
    playerUrl: '',
    tls: null
  },

  onItemBuy(e) {
    let itemId = e.detail.itemId;
    this.updateTapNum(itemId);
    let itemData = {};
    for (const i in this.data.goods) {
      let item = this.data.goods[i];
      if (item.activityItem.itemId == itemId) {
        itemData = item;
        break;
      }
    }
    if (itemData.activityItem && itemData.activityItem.itemId) {
      let selfMention = itemData.sellerItemVo && itemData.sellerItemVo.selfMention == 1 ? 1 : 0;
      wx.navigateTo({
        url: '/pages/order/confirm/confirm?isLive=1&itemId=' + itemId + '&activityId=' + itemData.activityItem.activityId + '&selfMention=' + selfMention,
      });
    }
  },

  onItemTap(e) {
    let itemId = e.detail.itemId;
    this.updateTapNum(itemId);
  },

  updateTapNum(itemId) {
    request.post('ylf/liveroom/mobile/item/updateHitNumber?activityId=' + this.data.activityId + '&itemId=' + itemId, {});
  },

  onQuitGroup() {
    wx.navigateBack();
  },

  onSendMessage(e) {
    this.data.tls.sendMessage(e.detail).then((res) => {
      let messageQueen = this.data.messageQueen;
      let account = app.globalData.account;
      let avatar = this.getUserAvatar();
      messageQueen.push({
        name: this._formatNick(this.data.userInfo.userID, account.nickName),
        avatar: avatar,
        message: res.message,
        id: `id${Date.now()}`
      })
      this.setData({
        messageQueen: messageQueen
      })
    })
  },

  _formatNick(userID, nick) {
    if (userID === this.data.userInfo.userID) {
      return '我'
    }
    return nick
  },

  onShowGoods(e) {
    const ani = wx.createAnimation({
      duration: 200
    })
    ani.bottom(0).step()
    this.setData({
      animation2: ani.export()
    })
  },

  onHideGoods() {
    const ani = wx.createAnimation({
      duration: 200
    })
    ani.bottom('-45vh').step()
    this.setData({
      animation2: ani.export()
    })
  },

  onRoomContentTap() {
    this.onHideGoods();
  },

  onPlayerStateChange(e) {
    console.log('live-player code:', e.detail.code)
    let code = e.detail.code;
    let roomStatus = 0;
    if (code == '2004') {
      roomStatus = 1;
    }
    this.setData({
      roomStatus: roomStatus
    })
  },

  onPlayerError(e) {
    console.error('live-player error:', e.detail.errMsg)
  },

  getUserAvatar() {
    let fullHeadImg = app.globalData.account && app.globalData.account.headImg ? app.globalData.account.headImg : (app.globalData.userInfo && app.globalData.userInfo.avatarUrl ? app.globalData.userInfo.avatarUrl : '');
    if (!(/http|https/.test(fullHeadImg))) {
      fullHeadImg = app.globalData.imageDomain + fullHeadImg;
    }
    return fullHeadImg;
  },

  async onSignature() {
    let account = app.globalData.account;
    let tag = 'guest';
    let salt = md5.hexMD5(account.id + '-' + account.mobile);
    let signature = md5.hexMD5(account.id + '-' + tag + '---' + salt);
    let avatar = this.getUserAvatar();
    let params = {
      username: account.id,
      tag: tag,
      signature: signature,
      sellerAccountId: this.data.sellerAccountId,
      name: account.nickName,
      avatar: avatar
    }
    let res = await requestLive.post('/base/v1/oauth/signature', params);
    if (res.errorCode > 0) {
      this.onRegister(account.id, salt);
    } else {
      console.log('onSignature:', res.data);
      this.setData({
        userData: res.data
      }, () => {
        this.onEnterRoom();
      })
    }
  },

  async onRegister(username, salt) {
    let params = {
      username: username,
      salt,
      sellerAccountId: this.data.sellerAccountId
    };
    let res = await requestLive.post('/base/v1/oauth/register', params);
    if (res.errorCode == 0 && res.data && res.data.id) {
      console.log('onRegister:', res.data);
      this.onSignature();
    }
  },

  async onEnterRoom() {
    let params = {
      appId: this.data.userData.sdkAppId,
      userId: this.data.userData.userId,
      token: this.data.userData.token,
      roomId: this.data.currRoom.roomId,
      role: 'guest'
    }
    let res = await requestLive.get('/base/v1/rooms/enter_room', params);
    console.log('onEnterRoom:', res);
    this.setData({
      currRoom: res.data
    }, () => {
      this.onInitPlayer();
      this.onInitIM();
      this.onInitGoods(false);
    })
  },

  onInitPlayer() {
    let url = 'http://live.ytx.com/live/' + this.data.ownerId + '.flv';
    console.log('playerUrl: ', url);
    this.setData({
      playerUrl: url
    })
  },

  onInitIM() {
    const tls = new TLS({
      SDKAppID: this.data.userData.sdkAppId,
      roomID: this.data.currRoom.roomId,
      userSig: this.data.userData.sdkUserSig,
      userName: this.data.userData.userId,
      TIM: TIM
    });
    this.setData({
      tls
    })
    //组件已经初始化好
    tls.on(TLS.EVENT.SDK_READY, async () => {
      console.log('------SDK_READY')
      tls.tim.setLogLevel(1);
      let account = app.globalData.account;
      let avatar = this.getUserAvatar();
      tls.tim.updateMyProfile({
        nick: account.nickName,
        avatar: avatar,
      }).then(async (res) => {
        let roomId = this.data.currRoom.roomId;
        const {
          groupInfo,
          userInfo
        } = await tls.joinRoom({
          roomID: roomId,
          getOwnerInfo: true
        })
        console.log('joinRoom:', groupInfo, userInfo)
        this.setData({
          isTimReady: true,
          userInfo: userInfo,
          groupInfo: groupInfo,
          ownerInfo: groupInfo.ownerInfo,
          noticeText: groupInfo.notification
        })
      });
    });
    //房间状态改变，如主播上（下）线，暂停等
    tls.on(TLS.EVENT.ROOM_STATUS_CHANGE, async (data) => {
      console.log('------ROOM_STATUS_CHANGE', data)
    });
    //有人进入直播间
    tls.on(TLS.EVENT.JOIN_GROUP, async (data) => {
      console.log('------JOIN_GROUP', data)
      let msgs = this.data.messageQueen;
      msgs.push({
        name: this._formatNick(data.userID, data.nick),
        avatar: data.avatar,
        message: '进入了直播间',
        id: `id${Date.now()}`
      })
      this.setData({
        messageQueen: msgs
      })
    });
    //有人离开直播间
    tls.on(TLS.EVENT.EXIT_GROUP, async (data) => {
      console.log('------EXIT_GROUP', data)
      let msgs = this.data.messageQueen;
      msgs.push({
        name: this._formatNick(data.userID, data.nick),
        avatar: data.avatar,
        message: '离开了直播间',
        id: `id${Date.now()}`
      })
      this.setData({
        messageQueen: msgs
      })
    });
    //有新的公告?
    tls.on(TLS.EVENT.NOTIFACATION, async (data) => {
      console.log('------NOTIFACATION', data)
    });
    //公告发生修改?
    tls.on(TLS.EVENT.INTRODUCTION, async ({
      name,
      introduction
    }) => {
      console.log('------INTRODUCTION', name, introduction)
    });
    //有人发送群消息
    tls.on(TLS.EVENT.MESSAGE, async (data) => {
      if (data.isSystemMessage) {
        console.log('------SYSTEM_MESSAGE', data)
        if (data.payload.operationType == 255) {
          if (data.payload.userDefinedField == 'update' || data.payload.userDefinedField == 'top') {
            this.onInitGoods(data.payload.userDefinedField == 'top');
          }
        } else if (data.payload.operationType == 5 || data.payload.operationType == 11) {
          this.liveEnd();
        }
      } else {
        console.log('------MESSAGE', data)
        let msgs = this.data.messageQueen;
        msgs.push({
          name: this._formatNick(data.userID, data.nick),
          avatar: data.avtar,
          message: data.message,
          id: `id${Date.now()}`
        })
        this.setData({
          messageQueen: msgs
        })
      }
    });
    //账号其他地方登录
    tls.on(TLS.EVENT.KICKED, async () => {
      console.log('------KICKED')
    });
    //sdk发生错误
    tls.on(TLS.EVENT.ERROR, async (data) => {
      console.log('------ERROR', data)
    });
  },

  liveEnd() {
    wx.showToast({
      title: '直播结束',
      icon: 'none'
    }, () => {
      wx.navigateBack();
    })
  },

  async onInitGoods(isAlert) {
    this.setData({
      goods: [],
      goodPageNumber: 1,
      goodHasNextPage: true
    }, () => {
      this.getGoodList(isAlert);
    });

  },

  async getGoodList(isAlert) {
    let params = {
      pageSize: 10,
      pageNumber: this.data.goodPageNumber,
      activityId: this.data.activityId
    }
    let res = await request.get('ylf/liveroom/mobile/item/queryShoppingCartItem', params);
    let page = res.data.page;
    let array = this.data.goods.concat(page.list);
    let currGood = this.data.currGood;
    if (array && array.length > 0) {
      for (const i in array) {
        let item = array[i];
        if (item.activityItem.topStatus == 0) {
          currGood = item;
        }
      }
      if (!currGood.activityItem) {
        if (array && array.length > 0) {
          currGood = array[0];
        }
      }
    } else {
      currGood = {};
    }
    if (!isAlert) {
      currGood = {};
    }
    this.setData({
      goods: array,
      goodHasNextPage: page.pageNum < page.pages,
      currGood: currGood
    })
  },

  onLower() {
    if (this.data.goodHasNextPage) {
      this.setData({
        pageNumber: this.data.pageNumber + 1
      }, () => {
        this.getGoodList();
      })
    }
  },

  init(options) {
    this.setData({
      sellerAccountId: app.globalData.sellerShop.accountId,
      activityId: options.id,
      activityName: options.activityName,
      activityImage: options.activityImage,
      ownerId: options.ownerId,
      currRoom: {
        roomId: options.roomId
      }
    }, () => {
      this.onSignature();
    });

  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    app.login(() => {
      this.init(options);
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {
    console.log('----onUnload---')
    let params = {
      userId: this.data.userData.userId,
      token: this.data.userData.token,
      roomId: this.data.currRoom.roomId
    }
    requestLive.get('/base/v1/rooms/leave_room', params).then(res => {
      console.log('leave_room: ', res)
    })
    if (this.data.tls) {
      this.data.tls.exitRoom().then(() => {
        this.data.tls.destroy();
        this.setData({
          isTimReady: false,
          tls: null
        })
      })
    }
  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return {
      title: this.data.activityName,
      path: '/pages/index/liveroom/index?roomId=' + this.data.currRoom.roomId + '&shopId=' + app.globalData.sellerShop.accountId,
      imageUrl: this.data.activityImage
    };
  }
})
// pages/index/seckill/list.js
const app = getApp();
const request = require('../../../utils/request.js');
const util = require('../../../utils/util.js');
const cart = require('../../../utils/cart.js');
const UiUtils = require("../../../lib/qrcode/UiUtils.js");

Page({

  /**
   * Page initial data
   */
  data: {
    imgServer: app.globalData.imageDomain,
    StatusBar: app.globalData.StatusBar < 21 ? 26 : app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    scrollTop: 0,
    navbarHeight: 0,
    seckillTimeList: [],
    currSeckill: {},
    seckillItemList: [],
    SECKILL_STATUS_TEXT: {
      0: '即将开始',
      1: '马上抢',
      2: '已结束'
    },
    seckillTime: 0,
    seckillTimer: null,
    animationX: null,
    animationY: null,
    ballX: 0,
    ballY: 0,
    showBall: false,
    isLoading: false,
    cartData: {},
    cartNum: 0,
    floatBtn: {
      x: 300,
      y: 750
    },
    setFloatBtnPointTimeoutId: -1,
  },

  onItemTap(e) {
    let itemId = e.currentTarget.dataset['itemId'];
    this.gotoDetail(itemId);
  },

  onBtnBackTap() {
    const pages = getCurrentPages();
    if (pages.length > 1) {
      wx.navigateBack();
    } else {
      wx.switchTab({
        url: '/pages/index/index/index',
      })
    }
  },

  onTimeItemTap(e) {
    let data = e.currentTarget.dataset['data'];
    if (data && data.seckillId != this.data.currSeckill.seckillId) {
      this.setCurrSeckill(data);
    }
  },

  gotoDetail(itemId) {
    util.navigation_to('/pages/item/detail/detail?itemId=' + itemId, 1);
  },

  gotoCart() {
    wx.switchTab({
      url: '/pages/cart/cart',
    })
  },

  async onItemAddCartTap(e) {
    let item = e.currentTarget.dataset['data'];
    if (item.seckillStatus == 1) {
      if (this.data.isLoading) {
        return;
      }
      app.login(() => {
        this.addCart(item.itemId, e);
      });
    }
  },

  async addCart(itemId, e) {
    if (app.globalData.sellerShop && app.globalData.sellerShop.accountId) {
      this.setData({
        isLoading: true
      });
      const res = await request.post('ylf/trade/cart/' + app.globalData.sellerShop.accountId + '/' + itemId);
      if (res.succeed) {
        let ballX = e.touches[0].clientX;
        let ballY = e.touches[0].clientY;

        this.createAnimation(ballX, ballY);
      } else {
        wx.showToast({
          title: res.message,
          icon: 'none'
        });
        this.setData({
          isLoading: false
        });
      }
    }
  },

  initCartData() {
    cart.initCartData(() => {
      let count = cart.getCartSize();
      this.setData({
        cartData: app.globalData.cartData,
        cartNum: count
      })
    });
  },

  setDelayTime(sec) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, sec)
    });
  },

  // 创建动画
  createAnimation(ballX, ballY) {
    let _this = this;
    let bottomX = this.data.floatBtn['x'];
    let bottomY = this.data.floatBtn['y'];
    let animationX = _this.flyX(bottomX, ballX); // 创建小球水平动画
    let animationY = _this.flyY(bottomY, ballY); // 创建小球垂直动画

    _this.setData({
      ballX: ballX,
      ballY: ballY,
      showBall: true
    });
    _this.setDelayTime(100).then(() => {
      // 100ms延时,  确保小球已经显示
      _this.setData({
        animationX: animationX.export(),
        animationY: animationY.export()
      })
      // 400ms延时, 即小球的抛物线时长
      return _this.setDelayTime(400);
    }).then(() => {
      _this.setData({
        animationX: _this.flyX(0, 0, 0).export(),
        animationY: _this.flyY(0, 0, 0).export(),
        showBall: false,
        isLoading: false
      });
      this.initCartData();
    })
  },

  // 水平动画
  flyX(bottomX, ballX, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'linear',
    })
    animation.translateX(bottomX - ballX).step();
    return animation;
  },
  // 垂直动画
  flyY(bottomY, ballY, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'ease-in',
    })
    animation.translateY(bottomY - ballY).step();
    return animation;
  },

  setCurrSeckill(data) {
    let seckillTime = 0;
    if (data.status < 2) {
      let end = data.status == 1 ? data.end : data.begin;
      seckillTime = (end * 1000) - new Date().getTime();
    }
    this.setData({
      currSeckill: data,
      seckillTime: seckillTime
    }, () => {
      if (data.seckillId) {
        this.getItemList();
        if (this.data.seckillTimer) {
          clearTimeout(this.data.seckillTimer);
          this.setData({
            seckillTimer: null
          });
        }
        if (data.status < 2) {
          this.countdownSeckill();
        }
      }
    })
  },

  countdownSeckill() {
    let time = this.data.seckillTime - 1000;
    if (time > 0) {
      let timer = setTimeout(() => {
        this.countdownSeckill();
      }, 1000);
      let currSeckill = this.data.currSeckill;
      let timeStr = util.convertTimeDian3(time);
      timeStr = timeStr.split(':');
      currSeckill.hh = timeStr[0];
      currSeckill.mm = timeStr[1];
      currSeckill.ss = timeStr[2];
      this.setData({
        currSeckill: currSeckill,
        seckillTime: time,
        seckillTimer: timer
      });
    } else {
      this.init();
    }
  },

  onMovableChange(e) {
    if (this.data.setFloatBtnPointTimeoutId > -1) {
      clearTimeout(this.data.setFloatBtnPointTimeoutId);
    }
    let id = setTimeout(this.setFloatBtnPoint, 1000, e.detail);
    this.setData({
      setFloatBtnPointTimeoutId: id
    });
  },

  setFloatBtnPoint(point) {
    let newFloatBtn = this.data.floatBtn;
    newFloatBtn.x = point.x;
    newFloatBtn.y = point.y;
    this.setData({
      floatBtn: newFloatBtn
    })
    wx.setStorage({
      key: 'floatBtnLocation',
      data: JSON.stringify(newFloatBtn)
    });
  },

  async getItemList() {
    let params = {
      sellerAccountId: app.globalData.sellerShop.accountId,
      seckillId: this.data.currSeckill.seckillId
    }
    if (!app.globalData.isInRange) {
      params.type = 2;
    }
    let res = await request.get('ylf/buyer/seckill/list', params);
    let array = res.data.dataList;
    for (const i in array) {
      let item = array[i];
      if (item.buyerNum) {
        item.isCanBuy = item.seckillStock - item.buyerNum > 0;
        item.progress = (item.buyerNum / item.seckillStock * 100).toFixed(0) + '%';
      } else {
        item.progress = 0;
        item.isCanBuy = item.seckillStock > 0;
      }
    }
    this.setData({
      seckillItemList: array
    });
  },

  async init() {
    let res = await request.get('ylf/buyer/seckill/time', {
      sellerAccountId: app.globalData.sellerShop.accountId
    });
    let array = res.data && res.data.dataList ? res.data.dataList : [];
    let current = {};
    if (array) {
      for (const i in array) {
        let item = array[i];
        let date = new Date(item.begin * 1000);
        item.startStr = date.Format('hh:mm');
        if (item.status == 1) {
          current = item;
        }
      }
      if (!current.seckillId) {
        for (const i in array) {
          let item = array[i];
          if (item.status == 0) {
            current = item;
            break;
          }
        }
      }
      if (!current.seckillId && array.length > 0) {
        current = array[0];
      }
    }
    this.setData({
      seckillTimeList: array
    }, () => {
      this.setCurrSeckill(current);
    });
    this.initCartData();
  },

  /**
   * Lifecycle function--Called when page scroll
   */
  onPageScroll: function (res) {
    this.setData({
      scrollTop: res.scrollTop
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    let floatBtn = {}
    let floatBtnStr = wx.getStorageSync('floatBtnLocation');
    if (floatBtnStr) {
      floatBtn = JSON.parse(floatBtnStr);
    } else {
      var res = wx.getSystemInfoSync();
      floatBtn = {
        x: res.windowWidth - 90,
        y: res.windowHeight - 90
      }
    }
    this.setData({
      floatBtn: floatBtn
    }, () => {
      this.init();
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {
    let query = wx.createSelectorQuery();
    query.select('.page-seckill-navbar').boundingClientRect(rect => {
      let v = UiUtils.px2rpx(rect.height);
      this.setData({
        navbarHeightpx: rect.height,
        navbarHeightrpx: v ? v : 0
      })
    }).exec();
    this.setData({
      cartData: app.globalData.cartData
    })
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
// pages/index/notify/detail.js
const app = getApp();
const request = require('../../../utils/request.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imgServer: app.globalData.imageDomain,
    notify: {},
    descList: []
  },

  async initData(id) {
    if (id) {
      let res = await request.get('ylf/buyer/notice/' + id);
      if (res.data && res.data.notice) {
        let descList = [];
        if(res.data.notice.noticeDesc && res.data.notice.noticeDesc.description) {
          let array = JSON.parse(res.data.notice.noticeDesc.description);
          if(array){
            descList = array;
          }
        }
        let date = new Date(res.data.notice.createdAt);
        res.data.notice.date = date.Format('yyyy年MM月dd日 hh:mm:ss');
        this.setData({
          notify: res.data.notice,
          descList: descList
        })
      } else {
        this.setData({
          notify: {},
          descList: []
        })
      }
    }
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.initData(options.id);
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
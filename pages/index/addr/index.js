// pages/index/addr/index.js
const app = getApp();
const request = require('../../../utils/request.js');
const address = require('../../../utils/address.js');
const util = require('../../../utils/util.js');

Page({

  /**
   * Page initial data
   */
  data: {
    isLogin: app.globalData.rememberMe ? true : false,
    homeAddress: {},
    currData: null,
    location: {},
    addrList: [],
    formData: null
  },

  onLocationTap() {
    if (this.data.location && this.data.location.sellerAccountId) {
      this.setSelect(this.data.location);
    } else {
      wx.showToast({
        icon: 'error',
        title: '当前定位不在配送范围内',
      });
    }
  },

  onSelectAddrTap() {
    util.navigation_to('/pages/user/address/edit/map', 1);
  },

  onBtnAddTap() {
    util.navigation_to('/pages/user/address/edit/index', 1);
  },

  async initData() {
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    this.setData({
      homeAddress: prevPages.data.currAddress
    });
    this.initLocation();
    this.getAddrList();
  },

  async getAddrList() {
    if (app.globalData.rememberMe) {
      const res = await request.get('ylf/buyer/buyer/address/list');
      this.setData({
        addrList: res.data.addressList
      })
    }
  },

  async initLocation() {
    const res = await address.getCurrentLocation();
    this.setData({
      location: res
    });
  },

  onAddrItemTap(e) {
    const item = e.currentTarget.dataset.item;
    this.setSelect(item);
  },

  async setSelect(item) {
    const res = await address.checkAddr(item.cityCode, item.latitude, item.longitude);
    item['sellerAccountId'] = res.sellerAccountId;
    item['sellerName'] = res.sellerName;
    item['isInRange'] = res.isCreateAddress;
    const pages = getCurrentPages();
    const prevPages = pages[pages.length - 2];
    prevPages.setData({
      currAddress: item,
      isReloadData: true
    }, () => {
      wx.setStorageSync('currAddress', JSON.stringify(item));
      wx.navigateBack();
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.initData();
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    this.setData({
      isLogin: app.globalData.rememberMe ? true : false
    })
    if (this.data.currData && this.data.currData.id) {
      this.setSelect(this.data.currData);
    } else if (this.data.formData && this.data.formData.sellerAccountId) {
      this.setSelect(this.data.formData);
    }
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
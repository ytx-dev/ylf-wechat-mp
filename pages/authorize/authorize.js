// pages/authorize/authorize.js
const util = require('../../utils/util.js');
const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        canIUseGetUserProfile: false,
        type: "", //指定h5跳转页面activity
        activityUrl: "" //指定h5授权成功后跳转下一个h5的url
    },
    getUserProfile(e) {
        // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
        // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
        wx.getUserProfile({
            desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
            success: (res) => {
                const authorize = res.errMsg === "getUserProfile:ok" ? true : false;
                if (authorize) {
                    app.globalData.userInfo = res.userInfo;
                    wx.setStorage({
                        key: 'userInfo',
                        data: JSON.stringify(app.globalData.userInfo)
                    });
                    app.globalData.iv = res.iv;
                    app.globalData.encryptedData = res.encryptedData;
                    app.ytxLogin(() => {
                        const invite = require('../../utils/invite.js');
                        invite.getSpreadNumber();
                        const cart = require('../../utils/cart.js');
                        cart.initCartData(() => {
                            wx.navigateBack();
                        });
                    });
                } else {
                    wx.navigateBack();
                }
            }
        });
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        if (wx.getUserProfile) {
            this.setData({
                canIUseGetUserProfile: true
            })
        }
        this.setData({
            type: options.type,
            activityUrl: options.activityUrl
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        return app.getShareCommonData();
    }
})
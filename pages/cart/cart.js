// pages/cart/cart.js
const app = getApp();
const request = require('../../utils/request.js');
const service = require("../../utils/util.js");
const UiUtils = require("../../lib/qrcode/UiUtils.js");
const cart = require('../../utils/cart.js');

Page({

  /**
   * Page initial data
   */
  data: {
    StatusBar: app.globalData.StatusBar < 21 ? 26 : app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    canShow: false,
    imageDomain: app.globalData.imageDomain,
    cartNumData: app.globalData.cartData,
    isDelState: false,
    cartData: {},
    selected: false,
    total: 0,
    shippingFee: 0,
    goodsCount: 0,
    selectedIds: '',
    selfMention: 0,
    triggered: false,
    animationX: null,
    animationY: null,
    ballX: 0,
    ballY: 0,
    showBall: false,
    isLoading: false,
    guessList: [],
    orderBy: '',
    pageNumber: 1,
    hasNextPage: true,
    ITEM_TYPE_TEXT: {
      1: '折',
      2: '秒'
    }
  },

  async deleteGoods(e) {
    const res = await request.del('ylf/trade/cart/' + e.currentTarget.dataset.goods.id);
    if (res) {
      this.initData();
    }
  },

  setCartDataItemAdd(itemId) {
    cart.setCartDataItemAdd(itemId);
    this.setData({
      cartNumData: app.globalData.cartData
    })
  },

  gotoItem(e) {
    let item = e.currentTarget.dataset['item'];
    if (item) {
      wx.navigateTo({
        url: '/pages/item/detail/detail?itemId=' + item.itemId
      })
    }
  },

  async onItemAddCartTap(e) {
    if (this.data.isLoading) {
      return;
    }
    app.login(() => {
      let itemId = e.currentTarget.dataset['itemId'];
      this.setCartDataItemAdd(itemId);
      this.addCart(itemId, e);
    });
  },

  async addCart(itemId, e) {
    this.setData({
      isLoading: true
    });
    const res = await request.post('ylf/trade/cart/' + app.globalData.sellerShop.accountId + '/' + itemId);
    if (res.succeed) {
      let ballX = e.touches[0].clientX;
      let ballY = e.touches[0].clientY;
      this.createAnimation(ballX, ballY);
    } else {
      wx.showToast({
        title: res.message,
        icon: 'none'
      });
      this.setData({
        isLoading: false
      });
    }
  },

  setDelayTime(sec) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve()
      }, sec)
    });
  },

  // 创建动画
  createAnimation(ballX, ballY) {
    var sysinfo = wx.getSystemInfoSync();
    let _this = this;
    let bottomX = UiUtils.rpx2px(480);
    let bottomY = sysinfo.windowHeight;
    let animationX = _this.flyX(bottomX, ballX); // 创建小球水平动画
    let animationY = _this.flyY(bottomY, ballY); // 创建小球垂直动画
    _this.setData({
      ballX: ballX,
      ballY: ballY,
      showBall: true
    });
    _this.setDelayTime(100).then(() => {
      // 100ms延时,  确保小球已经显示
      _this.setData({
        animationX: animationX.export(),
        animationY: animationY.export()
      })
      // 400ms延时, 即小球的抛物线时长
      return _this.setDelayTime(400);
    }).then(() => {
      _this.setData({
        animationX: _this.flyX(0, 0, 0).export(),
        animationY: _this.flyY(0, 0, 0).export(),
        showBall: false,
        isLoading: false
      });
      _this.initData();
    })
  },

  // 水平动画
  flyX(bottomX, ballX, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'linear',
    })
    animation.translateX(bottomX - ballX).step();
    return animation;
  },
  // 垂直动画
  flyY(bottomY, ballY, duration) {
    let animation = wx.createAnimation({
      duration: duration || 400,
      timingFunction: 'ease-in',
    })
    animation.translateY(bottomY - ballY).step();
    return animation;
  },

  initGuessList() {
    this.setData({
      guessList: [],
      pageNumber: 1,
      orderBy: '',
      hasNextPage: true
    }, () => {
      this.getGuessList();
    });
  },

  async getGuessList() {
    let params = {
      pageNum: this.data.pageNumber,
      sellerAccountId: app.globalData.sellerShop.accountId
    }
    if (!app.globalData.isInRange) {
      params.type = 2;
    }
    if (this.data.orderBy) {
      params.sort = this.data.orderBy;
    }
    let res = await request.get('ylf/search/guess', params);
    if (res && res.data) {
      let page = res.data.page;
      this.setData({
        guessList: this.data.guessList.concat(page.list),
        hasNextPage: page.pageNum < page.pages,
        orderBy: page.orderBy
      });
    }
  },

  onScrollLower() {
    if (this.data.hasNextPage) {
      this.setData({
        pageNumber: this.data.pageNumber + 1,
      }, () => {
        this.getGuessList();
      })
    }
  },

  onRefresh() {
    this.setData({
      triggered: true
    }, () => {
      this.initData();
    });
  },

  onNavberActionTap() {
    this.setData({
      isDelState: !this.data.isDelState,
      selectedIds: '',
      selfMention: 0,
      selected: false
    }, () => {
      this.updateSelectedStatus(false);
    });
  },

  /* 计算数量 */
  async dealCount(e) {
    const type = e.currentTarget.dataset.type;
    let index = e.currentTarget.dataset.index;
    let cartData = this.data.cartData;
    let item = cartData.valid[index];
    let number = item.number;
    if (type === '0') {
      //减
      if (number <= 1) {
        wx.showToast({
          title: '不能再少啦~',
          icon: 'none'
        });
        return false;
      }
      number--;
      let result = await this.updateCartItemNumber(item.id, number);
      if (result) {
        item.number = number;
        cartData.valid[index] = item;
        this.setData({
          cartData: cartData
        }, () => {
          this.calculationTotal();
          cart.setCartData(cartData);
          app.getCartNumber();
          this.setData({
            cartNumData: app.globalData.cartData
          })
        });
      }
    } else {
      //加
      if (number >= cartData.valid[index].stockNum || (cartData.valid[index].saleMaxNum > 0 && number >= cartData.valid[index].saleMaxNum)) {
        wx.showToast({
          title: '不能再多啦~',
          icon: 'none'
        });
        return false;
      }
      number++;
      let result = await this.updateCartItemNumberByAdd(item, number);
      if (result) {
        item.number = number;
        cartData.valid[index] = item;
        this.setData({
          cartData: cartData
        }, () => {
          this.calculationTotal();
          cart.setCartData(cartData);
          app.getCartNumber();
          this.setData({
            cartNumData: app.globalData.cartData
          })
        });
      }
    }

  },

  async updateCartItemNumber(id, number) {
    const res = await request.put('ylf/trade/cart/' + id + '/' + number);
    return res.succeed;
  },

  async updateCartItemNumberByAdd(item, number) {
    const res = await request.post('ylf/trade/cart?seller_item_sku_id=' + item.sellerItemSkuId + '&number=1');
    if (!res.succeed) {
      wx.showToast({
        title: res.message,
        icon: 'none'
      })
    }
    return res.succeed;
  },

  async calculationTotal() {
    let total = 0;
    let count = 0;
    let ids = '';
    let selfMention = 0;
    for (const i in this.data.cartData.valid) {
      const item = this.data.cartData.valid[i];
      if (item.isCheck) {
        if (ids) {
          ids += ',';
        }
        ids += item.id;
        total = Number((total + item.salePrice * item.number).toFixed(2));
        count++;
      }
      if (item.selfMention) {
        selfMention = item.selfMention;
      }
    }
    const fee = await this.getShippingFee(total);
    this.setData({
      total: total,
      goodsCount: count,
      selectedIds: ids,
      selfMention: selfMention,
      shippingFee: fee
    })
  },

  async getShippingFee(total) {
    const res = await request.get('ylf/trade/cart/shippingfee', {
      amount: total
    });
    return res.data.shippingFee;
  },

  async clickGoods(e) {
    const index = e.currentTarget.dataset.index;
    let cartData = this.data.cartData;
    let selectCount = 0;
    let params = {};
    for (const i in cartData.valid) {
      let item = cartData.valid[i];
      if (i === index + '') {
        item.isCheck = item.isCheck == 0 ? 1 : 0;
        params = {
          ids: item.id,
          isCheck: item.isCheck
        };
      }
      if (item.isCheck) {
        selectCount++;
      }
    }
    let res = await this.updateCheck(params);
    this.setData({
      cartData: cartData,
      selected: selectCount === cartData.valid.length
    }, () => {
      this.calculationTotal();
    });
  },

  async updateCheck({
    ids,
    isCheck
  }) {
    let res = await request.post('ylf/trade/cart/updateIsCheck?ids=' + ids + '&isCheck=' + isCheck, {});
    return res;
  },

  onBatchDelTap: function (e) {
    if (this.data.selectedIds) {
      const _this = this;
      wx.showModal({
        title: '提示',
        content: '确认删除选中商品吗？',
        success(res) {
          if (res.confirm) {
            _this.deleteGood();
          }
        }
      });
    } else {
      wx.showToast({
        icon: 'none',
        duration: 2000,
        title: '请选择商品',
      });
    }
  },

  async deleteGood() {
    const res = await request.del('ylf/trade/cart/' + this.data.selectedIds);
    if (res) {
      this.initData();
    }
  },

  async clearGood() {
    let ids = '';
    if (this.data.cartData.disabled) {
      for (const i in this.data.cartData.disabled) {
        let item = this.data.cartData.disabled[i];
        if (ids) {
          ids += ',';
        }
        ids += item.id;
      }
    }
    if (ids) {
      const res = await request.del('ylf/trade/cart/' + ids);
      if (res) {
        let cartData = this.data.cartData;
        cartData.disabled = [];
        this.setData({
          cartData: cartData
        }, () => {
          this.calculationTotal();
          cart.setCartData(cartData);
          app.getCartNumber();
        });
      }
    }

  },

  /* 点击全选 */
  clickTotal: function () {
    let selected = !this.data.selected;
    this.updateSelectedStatus(selected);
  },

  async updateSelectedStatus(selected) {
    let list = [];
    let ids = '';
    for (const i in this.data.cartData.valid) {
      let item = this.data.cartData.valid[i];
      item.isCheck = selected ? 1 : 0;
      list.push(item);
      if (ids) {
        ids += ',';
      }
      ids += item.id;
    }
    let cartData = this.data.cartData;
    cartData.valid = list;
    let params = {
      ids: ids,
      isCheck: selected ? 1 : 0
    };
    let res = await this.updateCheck(params);
    this.setData({
      selected: selected,
      cartData: cartData
    }, () => {
      this.calculationTotal();
    })
  },

  clearDisabled() {
    const _this = this;
    wx.showModal({
      title: '提示',
      content: '确认清空失效商品？',
      success(res) {
        if (res.confirm) {
          _this.clearGood();
        }
      }
    });
  },

  clickImage: function (e) {
    service.navigation_to('../item/detail/detail?itemId=' + e.currentTarget.dataset.itemid, 1);
  },

  async initData() {
    wx.showLoading({
      title: '载入中',
      mask: true
    });
    const data = await this.getCartList();
    if (data) {
      let selectedIds = '';
      let count = 0;
      let selfMention = 0;
      for (const i in data.valid) {
        let item = data.valid[i];
        if (item.isCheck) {
          if (selectedIds) {
            selectedIds += ',';
          }
          selectedIds += item.id;
          count++;
        }
        if (item.selfMention) {
          selfMention = item.selfMention;
        }
      }
      this.setData({
        cartData: data,
        canShow: true,
        total: 0,
        shippingFee: 0,
        selectedIds: selectedIds,
        selfMention: selfMention,
        selected: count == data.valid.length
      }, () => {
        this.calculationTotal();
      })
    }
    cart.setCartData(data);
    app.getCartNumber();
    wx.hideLoading();
    this.setData({
      cartNumData: app.globalData.cartData,
      isDelState: false,
      triggered: false
    });
  },

  async getCartList() {
    try {
      const res = await request.get('ylf/trade/cart', {
        shop_id: app.globalData.sellerShop.accountId
      });
      if (res && res.data) {
        if (!res.data.valid) {
          res.data.valid = [];
        }
        if (!res.data.disabled) {
          res.data.disabled = [];
        }
        return res.data;
      }
      return {
        valid: [],
        disabled: []
      };
    } catch (err) {
      if (err.isReLogin) {
        app.login(() => {
          this.initData();
        });
      }
    }
  },

  goIndex: function () {
    wx.switchTab({
      url: '/pages/index/index/index',
    });
  },

  clickSettlement: function () {
    if (this.data.selectedIds) {
      service.navigation_to('../order/confirm/confirm?cartLineIds=' + this.data.selectedIds + '&selfMention=' + this.data.selfMention, 1);
    } else {
      wx.showToast({
        icon: 'none',
        duration: 2000,
        title: '请选择商品',
      });
    }
  },

  doNothing() {
    return false;
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {

  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {
    if (!app.globalData.sellerShop.accountId) {
      wx.showModal({
        title: '提示',
        content: '请选择地址',
        showCancel: false,
        success(res) {
          wx.switchTab({
            url: '/pages/index/index/index',
          });
        }
      });
    } else {
      app.login(() => {
        this.initData();
      });
    }
    this.initGuessList();
  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  }
})
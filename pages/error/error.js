// pages/error/error.js
// const errorCode = {
//   '101':'网络连接失败，请检查是否联网',
//   '102':'加载失败，请点击重新加载',
//   '-101':'网络异常，请检查后重试',
//   '-102':'请求失败，请稍后再试'
// }
const app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return app.getShareCommonData();
  },
  /**
   * 公共 error 弹出方法
   */
  showError:function(){
    var currUrl = getCurrentPages(getCurrentPages().length - 1);
  }
})
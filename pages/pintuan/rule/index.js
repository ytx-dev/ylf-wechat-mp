// pages/pintuan/rule/index.js
const app = getApp();

Page({

  /**
   * Page initial data
   */
  data: {
    type: 0, // 0:普通 1:邀新
    textRuleList: [],
    textRuleType0: [
      '1. 发团：拼团活动期间，用户可发起拼团。',
      '2. 参团：参加朋友发起的拼团。',
      '3. 限量：拼团活动进行期间，每位用户每件商品仅可开团一次，若拼团失败后可重新开团或参团。',
      '4. 拼团人数：拼团人数可大于拼团要求人数。即拼团成功后，新参与者可继续参与拼团，直到倒计时结束。',
      '5. 成功：规定时间内团购人数满足拼团要求人数（包括团长），视为拼团成功。',
      '6. 失败：逾期人数未满足要求即为拼团失败，系统将发起退款，退款将于1-5个工作日原路返回。',
      '7. 拼团过程中，不支持发起退款操作。',
    ],
    textRuleType1: [
      '1. 发团：拼团活动期间，用户可发起拼团。',
      '2. 参团：仅新用户可参加朋友发起的拼团。',
      '3. 限量：拼团活动进行期间，每位用户每件商品仅可开团一次，若拼团失败后可重新开团或参团。',
      '4. 拼团人数：拼团人数可大于拼团要求人数。即拼团成功后，新参与者可继续参与拼团，直到倒计时结束。',
      '5. 成功：规定时间内团购人数满足拼团要求人数（包括团长），视为拼团成功。',
      '6. 失败：逾期人数未满足要求即为拼团失败，系统将发起退款，退款将于1-5个工作日原路返回。',
      '7. 拼团过程中，不支持发起退款操作。',
    ]
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    this.setData({
      textRuleList: options.type ? this.data.textRuleType1 : this.data.textRuleType0
    })
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    return app.getShareCommonData();
  }
})
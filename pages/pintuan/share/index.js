// pages/pintuan/share/index.js
const app = getApp();
const request = require('../../../utils/request.js');
const util = require('../../../utils/util.js');

Page({

  /**
   * Page initial data
   */
  data: {
    imageDomain: app.globalData.imageDomain,
    groupId: '',
    orderCombinationVo: {},
    itemVo: {},
    pintuanData: {},
    pintuanTime: 0,
    pintuanTimer: null,
  },

  gotoKaituan() {
    wx.navigateTo({
      url: '/pages/order/confirm/confirm?isPintuan=1&itemId=' + this.data.itemVo.itemId + '&combinationId=' + this.data.orderCombinationVo.combinationId,
    });
  },

  gotoPintuan() {
    if (this.data.orderCombinationVo.peopleNumber == this.data.orderCombinationVo.detailList.length) {
      this.gotoItemTap();
    } else {
      app.login(() => {
        this.gotoConfirm();
      });
    }
  },

  gotoConfirm() {
    wx.navigateTo({
      url: '/pages/order/confirm/confirm?isPintuan=1&itemId=' + this.data.itemVo.itemId + '&combinationId=' + this.data.orderCombinationVo.combinationId + '&groupId=' + this.data.groupId,
    });
  },

  gotoItemTap() {
    wx.navigateTo({
      url: '/pages/item/detail/detail?itemId=' + this.data.itemVo.itemId
    })
  },

  async initItemData() {
    let res = await request.get('ylf/trade/shareCombination', {
      groupId: this.data.groupId
    });
    let orderCombinationVo = res.data.orderCombinationVo;
    let pintuanTime = 0;
    if (orderCombinationVo) {
      await this.sqlGetOrderCount();
      pintuanTime = orderCombinationVo.stopTime - orderCombinationVo.nowTime;
      if (orderCombinationVo.detailList) {
        for (const i in orderCombinationVo.detailList) {
          let item = orderCombinationVo.detailList[i];
          if (item.headerImg) {
            item.headerImg = this.data.imageDomain + item.headerImg;
          } else if (item.nikeName == this.data.nickName) {
            item.headerImg = this.data.headImg
          }
        }
      }

    }
    if (this.data.pintuanTimer) {
      clearTimeout(this.data.pintuanTimer);
    }
    this.setData({
      orderCombinationVo: orderCombinationVo,
      itemVo: res.data.itemVo,
      pintuanData: {},
      pintuanTime: pintuanTime,
      pintuanTimer: null
    }, () => {
      this.setCountdown();
    })
  },

  setCountdown() {
    let time = this.data.pintuanTime - 1000;
    if (time > 0) {
      let timer = setTimeout(() => {
        this.setCountdown();
      }, 1000);
      let pintuanData = this.data.pintuanData;
      let timeStr = util.convertTimeDian3(time);
      timeStr = timeStr.split(':');
      pintuanData.hh = timeStr[0];
      pintuanData.mm = timeStr[1];
      pintuanData.ss = timeStr[2];
      this.setData({
        pintuanData: pintuanData,
        pintuanTime: time,
        pintuanTimer: timer
      });
    } else {
      this.setData({
        pintuanData: {
          hh: '00',
          mm: '00',
          ss: '00'
        },
        pintuanTime: null,
        pintuanTimer: null
      });
    }
  },

  initNameAndHead(name, headImg) {
    let fullHeadImg = headImg ? headImg : (app.globalData.account && app.globalData.account.headImg ? app.globalData.account.headImg : (app.globalData.userInfo && app.globalData.userInfo.avatarUrl ? app.globalData.userInfo.avatarUrl : ''));
    if (!(/http|https/.test(fullHeadImg))) {
      fullHeadImg = app.globalData.imageDomain + fullHeadImg;
    }
    this.setData({
      headImg: fullHeadImg,
      nickName: name ? name : (app.globalData.account && app.globalData.account.nickName ? app.globalData.account.nickName : '')
    });
  },

  async sqlGetOrderCount() {
    if (app.globalData.rememberMe) {
      const res = await request.get('ylf/buyer/buyer/show');
      const data = res.data;
      if (data) {
        this.initNameAndHead(data.name, data.headImg);
      }
    }
  },

  async checkShopData(shopId) {
    if (shopId && !(app.globalData.sellerShop && app.globalData.sellerShop.accountId)) {
      const shopIndex = await this.getShopIndexPage(shopId);
      app.globalData.sellerShop = shopIndex.sellerShop;
      wx.setStorage({
        key: "sellerShop",
        data: JSON.stringify(shopIndex.sellerShop)
      });
    }
    this.initItemData();
  },

  async getShopIndexPage(id) {
    const res = await request.get('ylf/store/index/' + id);
    return res.data;
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    if (options.spreadNumber) {
      wx.setStorageSync('spreadNumber', options.spreadNumber);
      const invite = require('../../../utils/invite.js');
      invite.bindFormLocal();
    }
    if (!options.groupId) {
      wx.navigateBack();
    } else {
      this.setData({
        groupId: options.groupId
      }, () => {
        this.checkShopData(options.shopId);
      });
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {

  }
})
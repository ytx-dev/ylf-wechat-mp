// pages/pintuan/index/index.js
const app = getApp();
const request = require('../../../utils/request.js');
const util = require('../../../utils/util.js');
const address = require('../../../utils/address.js');

Page({

  /**
   * Page initial data
   */
  data: {
    isLoading: true,
    imgServer: app.globalData.imageDomain,
    StatusBar: app.globalData.StatusBar < 21 ? 26 : app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    scrollTop: 0,
    pintuanType: 0,
    pageNum: 1,
    dataList: [],
    total: 0,
    pintuanData: {},
    pintuanTime: 0,
    pintuanTimer: null,
    shareItem: {}
  },

  onBtnBackTap() {
    const pages = getCurrentPages();
    if (pages.length > 1) {
      wx.navigateBack();
    } else {
      wx.switchTab({
        url: '/pages/index/index/index',
      })
    }
  },

  gotoRule() {
    wx.navigateTo({
      url: '/pages/pintuan/rule/index?type=' + this.data.pintuanType,
    })
  },

  onItemTap(e) {
    let item = e.currentTarget.dataset['data'];
    this.gotoDetail(item.itemId);
  },

  onShareTap(e) {
    let item = e.currentTarget.dataset['data'];
    this.setData({
      shareItem: item
    })
  },

  gotoDetail(e) {
    let item = e.currentTarget.dataset['data'];
    wx.navigateTo({
      url: '/pages/item/detail/detail?itemId=' + item.itemId,
    })
  },

  gotoEnd() {
    wx.redirectTo({
      url: '/pages/pintuan/index/end',
    })
  },

  setCountdown() {
    let time = this.data.pintuanTime - 1000;
    if (time > 0) {
      let timer = setTimeout(() => {
        this.setCountdown();
      }, 1000);
      let pintuanData = this.data.pintuanData;
      let timeStr = util.convertTimeDian3(time);
      timeStr = timeStr.split(':');
      pintuanData.hh = timeStr[0];
      pintuanData.mm = timeStr[1];
      pintuanData.ss = timeStr[2];
      this.setData({
        pintuanData: pintuanData,
        pintuanTime: time,
        pintuanTimer: timer
      });
    } else {
      this.gotoEnd();
    }
  },

  async getList() {
    if (app.globalData.sellerShop && app.globalData.sellerShop.accountId) {
      let res = await request.get('ylf/buyer/combination/index', {
        sellerAccountId: app.globalData.sellerShop.accountId,
        type: this.data.pintuanType,
        pageNum: this.data.pageNum
      });
      if (!res.data.combinationVo.stopTime) {
        this.gotoEnd();
      } else {
        if (this.data.pintuanTimer) {
          clearTimeout(this.data.pintuanTimer);
        }
        this.setData({
          dataList: this.data.dataList.concat(res.data.page.list),
          total: res.data.page.total,
          pintuanData: res.data.combinationVo,
          pintuanTime: res.data.combinationVo.stopTime - res.data.combinationVo.currentTime,
          pintuanTimer: null
        }, () => {
          if (this.data.isLoading) {
            this.setData({
              isLoading: false
            });
          }
          this.setCountdown();
        })
      }
    }
  },

  init(type) {
    this.setData({
      pintuanType: type,
      pageNum: 1,
      dataList: []
    }, () => {
      this.getList();
    })
  },

  async checkShopData(shopId) {
    if (app.globalData.sellerShop && app.globalData.sellerShop.accountId) {
      this.init(this.data.pintuanType);
    } else if (shopId) {
      const shopIndex = await this.getShopIndexPage(shopId);
      app.globalData.sellerShop = shopIndex.sellerShop;
      wx.setStorage({
        key: "sellerShop",
        data: JSON.stringify(shopIndex.sellerShop)
      });
      this.init(this.data.pintuanType);
    } else {
      const res = await address.getCurrentLocation();
      if (res && res.sellerAccountId) {
        this.checkShopData(res.sellerAccountId);
      }
    }
  },

  /**
   * Lifecycle function--Called when page scroll
   */
  onPageScroll: function (res) {
    this.setData({
      scrollTop: res.scrollTop
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad(options) {
    if (options.spreadNumber) {
      wx.setStorageSync('spreadNumber', options.spreadNumber);
      const invite = require('../../../utils/invite.js');
      invite.bindFormLocal();
    }
    this.setData({
      isLoading: true,
      pintuanType: options.type,
      pageNum: 1,
      dataList: []
    }, () => {
      this.checkShopData(options.shopId);
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady() {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow() {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide() {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload() {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh() {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom() {
    if (this.data.dataList.length < this.data.total) {
      this.setData({
        pageNum: this.data.pageNum + 1,
      }, () => {
        this.getList();
      })
    }
  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage() {
    if (this.data.shareItem && this.data.shareItem.groupId) {
      let groupId = this.data.shareItem.groupId;
      const promise = new Promise(resolve => {
        request.get('ylf/trade/imageCombiner/combination', {
          groupId: groupId
        }).then((res) => {
          if (res.data && res.data.shareUrl) {
            let path = '/pages/pintuan/share/index?groupId=' + groupId + '&shopId=' + app.globalData.sellerShop.accountId;
            if (app.globalData.spreadNumber) {
              path += '&spreadNumber=' + app.globalData.spreadNumber;
            }
            resolve({
              title: res.data.shareTitle,
              path: path,
              imageUrl: res.data.shareUrl
            });
          }
          this.setData({
            shareItem: {}
          })
        });
      });
      let dp = '/pages/pintuan/share/index?groupId=' + groupId + '&shopId=' + app.globalData.sellerShop.accountId;
      if (app.globalData.spreadNumber) {
        dp += '&spreadNumber=' + app.globalData.spreadNumber;
      }
      return {
        title: this.data.shareItem.title,
        path: dp,
        imageUrl: this.data.shareItem.img,
        promise
      }
    } else {
      let dp = '/pages/pintuan/index/index?type=' + this.data.pintuanType + '&shopId=' + app.globalData.sellerShop.accountId;
      if (app.globalData.spreadNumber) {
        dp += '&spreadNumber=' + app.globalData.spreadNumber;
      }
      return {
        title: this.data.combinationVo.mark,
        path: dp,
        imageUrl: this.data.combinationVo.img
      }
    }
  }
})